//! The central point of communication between the GUI and the Falcon server.
//!
//! The essential function of the Proxy is to forward messages from Falcon to
//! the GUI and to forward messages from the GUI to Falcon. This requires a
//! translation step because the GUI speaks JSON and Falcon speaks a custom
//! format over ZMQ. There is also the details of handling the socket connection
//! with Falcon that much be controlled by the GUI.

use std::collections::HashSet;
use std::sync;

use futures::StreamExt;
use gethostname::gethostname;
use git_version::git_version;
use log::{debug, error};
use tokio::sync::mpsc;
use tokio::sync::{broadcast, RwLock};
use tokio::task;
use tokio::time::Duration;
use tokio_stream::wrappers::{ReceiverStream, UnboundedReceiverStream};
use uuid::Uuid;

use crate::error::Result;
use crate::falcon;
use crate::model;

const GIT_VERSION: &str = git_version!(args = ["--tags", "--long", "--dirty=-modified"]);
const CHANNEL_SIZE: usize = 1000;

/// A `Proxy` stores the source of a broadcast channel, the set of subscribed clients, and
/// the connection to the Falcon server. Messages from Falcon are broadcast to all
/// subscribed clients or (optionally) to a specific client addressed by their UUID.
/// When sending to a specific, all clients receive the message but they filter for their
/// own UUID. When sending to all (N) clients, N messages are put into the broadcast
/// channel, each with a UUID of a subscribed client.
pub struct Proxy {
    bcast: broadcast::Sender<model::OutputParcel>,
    clients: RwLock<HashSet<Uuid>>,
    falcon_client: sync::Arc<sync::RwLock<falcon::FalconClient>>,
}

impl Proxy {
    /// Create a new `Proxy`.
    pub fn new() -> Self {
        let (bcast, _) = broadcast::channel::<model::OutputParcel>(CHANNEL_SIZE);

        let falcon_client: sync::Arc<sync::RwLock<falcon::FalconClient>> =
            sync::Arc::new(sync::RwLock::new(falcon::FalconClient::new()));

        Proxy {
            bcast,
            clients: Default::default(),
            falcon_client,
        }
    }

    /// Subscribe to broadcast messages from the `Proxy`.
    ///
    /// This is used by clients to receive messages from Falcon via the `Proxy`.
    pub async fn subscribe(&self, id: Uuid) -> broadcast::Receiver<model::OutputParcel> {
        self.clients.write().await.insert(id);
        self.bcast.subscribe()
    }

    /// Broadcast a message to all subscribed clients.
    pub async fn send(&self, message: model::ProxyMessage) {
        if self.bcast.receiver_count() == 0 {
            return;
        }
        self.clients.read().await.iter().for_each(|user_id| {
            self.bcast
                .send(model::OutputParcel::new(*user_id, message.clone()))
                .unwrap();
        });
    }

    /// Broadcast a message to a single client.
    pub async fn send_targeted(&self, client_id: Uuid, message: model::ProxyMessage) {
        if (self.bcast.receiver_count() > 0) && (self.clients.read().await.contains(&client_id)) {
            self.bcast
                .send(model::OutputParcel::new(client_id, message))
                .unwrap();
        }
    }

    /// Removes a client from the set of subscribers.
    ///
    /// Should be called when the client disconnects.
    pub async fn on_disconnect(&self, client_id: Uuid) {
        self.clients.write().await.remove(&client_id);
    }

    /// Run the Proxy.
    ///
    /// Spawns threads to handle communication with Falcon and connects
    /// forwards messages between clients and Falcon.
    ///
    /// # Examples
    /// ```
    /// let proxy = Arc::new(Proxy::new());
    ///
    /// // Wait 1 second then terminate the Proxy.
    /// tokio::spawn(async move {
    ///   time::sleep(time::Duration::from_secs(1)).await;
    ///   proxy.terminate().await;
    /// });
    ///
    /// proxy.run().await;
    /// ```
    pub async fn run(&self, receiver: mpsc::UnboundedReceiver<model::InputParcel>) {
        let (falcon_response_tx, proxy_response_rx) =
            mpsc::channel::<Result<falcon::FalconResponse>>(CHANNEL_SIZE);
        let (proxy_request_tx, falcon_request_rx) = mpsc::channel::<falcon::Request>(CHANNEL_SIZE);
        let (event_tx, event_rx) = mpsc::channel::<model::ProxyMessage>(CHANNEL_SIZE);

        let falcon_client_requests = self.falcon_client.clone();

        let falcon_client_events = self.falcon_client.clone();

        task::spawn_blocking(move || {
            falcon::requests(
                falcon_client_requests,
                falcon_response_tx,
                falcon_request_rx,
            );
        });

        task::spawn_blocking(move || {
            falcon::subscribe(falcon_client_events, event_tx);
        });

        let process_requests = UnboundedReceiverStream::new(receiver)
            .for_each(|request| self.process(request, &proxy_request_tx));

        let fwd_responses = ReceiverStream::new(proxy_response_rx)
            .map(|response| match response {
                Ok(message) => model::ProxyMessage::Response(message.data),
                Err(err) => model::ProxyMessage::Error(format!("{}", err)),
            })
            .for_each(|parcel| async {
                self.send(parcel).await;
            });

        let fwd_events = ReceiverStream::new(event_rx).for_each(|evt| self.send(evt));

        tokio::select! {
            _ = process_requests => {},
            _ = fwd_responses => {},
            _ = fwd_events => {},
        }

        debug!("Proxy finishing");
    }

    /// Terminate the Proxy
    ///
    /// Stops the Falcon connection threads.
    pub async fn terminate(&self) {
        debug!("Trying to terminate Falcon connections");
        if let Ok(mut client) = self.falcon_client.write() {
            client.terminate();
            debug!("Terminated Falcon connections");
        } else {
            error!("Could not acquire lock on client");
        }
    }

    async fn process(
        &self,
        request: model::InputParcel,
        proxy_request_tx: &mpsc::Sender<falcon::Request>,
    ) {
        match request.input {
            model::ClientRequest::Ping => {
                self.send_targeted(request.client_id, model::ProxyMessage::Pong)
                    .await
            }
            model::ClientRequest::Control(ctrl) => {
                self.process_control(&ctrl, &proxy_request_tx).await
            }
            model::ClientRequest::Falcon(req) => {
                self.request(
                    falcon::FalconRequest {
                        from: request.client_id,
                        data: req,
                    },
                    proxy_request_tx,
                )
                .await
            }
        }
    }

    async fn process_control(
        &self,
        control: &model::ProxyControl,
        proxy_request_tx: &mpsc::Sender<falcon::Request>,
    ) {
        let result = match control {
            model::ProxyControl::Connect(addr) => {
                self.connect(addr.to_string(), proxy_request_tx).await
            }
            model::ProxyControl::Disconnect(addr) => {
                self.disconnect(addr.to_string(), proxy_request_tx).await
            }
            model::ProxyControl::Terminate => {
                self.falcon_client.write().unwrap().terminate();
                model::ProxyMessage::Ack(vec![String::from("terminated")])
            }
            model::ProxyControl::Version => model::ProxyMessage::Ack(vec![
                String::from("version"),
                format!(
                    "{} ({})",
                    GIT_VERSION,
                    std::env!("CARGO_PKG_VERSION", "????")
                ),
                gethostname()
                    .into_string()
                    .unwrap_or(String::from("localhost")),
            ]),
        };

        self.send(result).await;
    }

    async fn connect(
        &self,
        addr: String,
        request_tx: &mpsc::Sender<falcon::Request>,
    ) -> model::ProxyMessage {
        self.falcon_client.write().unwrap().connect(&addr);
        if let Err(err) = request_tx
            .send_timeout(
                falcon::Request::Connect(addr.clone()),
                Duration::from_millis(100),
            )
            .await
        {
            let error = format!("Count not forward connect request from Proxy: {}", err);
            error!("{}", error);
            model::ProxyMessage::Error(error)
        } else {
            model::ProxyMessage::Ack(vec![String::from("connected"), addr])
        }
    }

    async fn disconnect(
        &self,
        addr: String,
        request_tx: &mpsc::Sender<falcon::Request>,
    ) -> model::ProxyMessage {
        self.falcon_client.write().unwrap().disconnect();
        if let Err(err) = request_tx
            .send_timeout(
                falcon::Request::Disconnect(addr.clone()),
                Duration::from_millis(100),
            )
            .await
        {
            let error = format!("Count not forward disconnect request from Proxy: {}", err);
            error!("{}", error);
            model::ProxyMessage::Error(error)
        } else {
            model::ProxyMessage::Ack(vec![String::from("disconnected"), addr])
        }
    }

    async fn request(
        &self,
        req: falcon::FalconRequest,
        request_tx: &mpsc::Sender<falcon::Request>,
    ) {
        if let Err(err) = request_tx
            .send_timeout(falcon::Request::Falcon(req), Duration::from_millis(100))
            .await
        {
            let error = format!("Count not forward request: {}", err);
            error!("{}", error);
        }
    }
}

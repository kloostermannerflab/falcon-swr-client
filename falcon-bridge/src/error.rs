//! Custom application error type
//!
//! This module defines a custom error wrapper for the observable errors
//! that this application can encounter.

use std::{error, fmt, io, result};

use tokio::sync::mpsc::error as tokio;

use crate::model;

/// Custom error wrapper
#[derive(Debug)]
pub enum Error {
    System(String),
    Io(io::Error),
    MessageDecode(serde_json::Error),
    MessageRecv(tokio::SendError<model::InputParcel>),
    MessageSend(tokio::SendError<result::Result<warp::ws::Message, warp::Error>>),
    Zmq(zmq::Error),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::System(err) => write!(f, "System error: {}", err),
            Error::Io(ref err) => write!(f, "IO error: {}", err),
            Error::MessageDecode(ref err) => write!(f, "Invalid message: {}", err),
            Error::MessageRecv(ref err) => write!(f, "Error receiving message: {}", err),
            Error::MessageSend(ref err) => write!(f, "Error sending message: {}", err),
            Error::Zmq(ref err) => write!(f, "ZeroMQ error: {}", err),
        }
    }
}

impl error::Error for Error {}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        Error::Io(err)
    }
}

impl From<serde_json::Error> for Error {
    fn from(err: serde_json::Error) -> Self {
        Error::MessageDecode(err)
    }
}

impl From<tokio::SendError<model::InputParcel>> for Error {
    fn from(err: tokio::SendError<model::InputParcel>) -> Self {
        Error::MessageRecv(err)
    }
}

impl From<tokio::SendError<result::Result<warp::ws::Message, warp::Error>>> for Error {
    fn from(err: tokio::SendError<result::Result<warp::ws::Message, warp::Error>>) -> Self {
        Error::MessageSend(err)
    }
}

impl From<zmq::Error> for Error {
    fn from(err: zmq::Error) -> Self {
        Error::Zmq(err)
    }
}

/// Custom Result type that uses the custom Error type as it's Error branch.
pub type Result<T> = result::Result<T, Error>;

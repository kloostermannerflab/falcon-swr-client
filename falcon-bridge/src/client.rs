//! An interface for dealing with clients.
//!
//! This simplifies dealing with clients by treating them
//! as input and output `Stream`s. Each client is uniquely
//! Identified by a UUID. The `Proxy` is responsible for
//! keeping track of connected clients.

use std::{error, result};

use futures::stream::SplitStream;
use futures::{future, Stream, StreamExt, TryStream, TryStreamExt};
use uuid::Uuid;
use warp::filters::ws::WebSocket;

use crate::error::{Error, Result};
use crate::model::{InputParcel, OutputParcel};

/// A `Client` is uniquely identified by a UUID
#[derive(Clone, Copy, Default)]
pub struct Client {
    pub id: Uuid,
}

impl Client {
    /// Create a new `Client`
    pub fn new() -> Self {
        Client { id: Uuid::new_v4() }
    }

    /// Read data from a `WebSocket` as a continuous `Stream`
    pub fn read_input(
        &self,
        stream: SplitStream<WebSocket>,
    ) -> impl Stream<Item = Result<InputParcel>> {
        let client_id = self.id;
        stream
            .take_while(|message| {
                future::ready(if let Ok(message) = message {
                    message.is_text()
                } else {
                    false
                })
            })
            .map(move |message| match message {
                Err(err) => Err(Error::System(err.to_string())),
                Ok(message) => {
                    let input = serde_json::from_str(message.to_str().unwrap())?;
                    Ok(InputParcel::new(client_id, input))
                }
            })
    }

    /// Write data to a `WebSocket` as a continuous `Stream`
    ///
    /// Only put a message in the stream if it matches this clients UUID.
    pub fn write_output<S, E>(&self, stream: S) -> impl Stream<Item = Result<warp::ws::Message>>
    where
        S: TryStream<Ok = OutputParcel, Error = E> + Stream<Item = result::Result<OutputParcel, E>>,
        E: error::Error,
    {
        let client_id = self.id;
        stream
            .try_filter(move |parcel| future::ready(parcel.client_id == client_id))
            .map_ok(|message| {
                let data = serde_json::to_string(&message.output).unwrap();
                warp::ws::Message::text(data)
            })
            .map_err(|err| Error::System(err.to_string()))
    }
}

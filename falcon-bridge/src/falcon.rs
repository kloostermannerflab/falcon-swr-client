#![warn(missing_docs)]
//! Deal with the specifis of talking to Falcon
//!
//! Falcon accepts connections on 3 zmq tcp ports:
//! * Requests (A zmq REQ port),
//! * Logging (A zmq SUB port), and
//! * Events (A zmq SUB port).
//!
//! Requests _must_ be synchronised such that, after sending a request,
//! a reply from Falcon must be sent. The Logging and Events ports
//! always receive data so are always listening. Therefore this module
//! separates Requests handling from Logging and Events handling, running
//! them in seperate threads.
//!
//! This module has no knowledge of the Proxy (which uses this module to
//! handle the details of communication with Falcon). The interface only
//! consists of the `FalconClient` and a set of channels.

use std::borrow::BorrowMut;
use std::convert::TryFrom;
use std::default::Default;
use std::sync::{Arc, RwLock};
use std::thread;
use std::time::Duration;

use log::{debug, error, info, trace};
use tokio::sync::mpsc;
use tokio::task::JoinHandle;
use uuid::Uuid;

use crate::error::{Error, Result};
use crate::model::{FalconEvents, FalconLog, ProxyMessage};

const REQ_POLL_TIMEOUT: i64 = 5000; // ms
const SUB_POLL_TIMEOUT: i64 = 150; // ms
const SLEEP_TIMEOUT: u64 = 150; // ms

const REQUEST_PORT: i32 = 5555;
const LOG_PORT: i32 = 5556;
const EVENT_PORT: i32 = 7777;

/// A message describing a request to Falcon.
#[derive(Debug)]
pub struct FalconRequest {
    /// The UUID of the connected client.
    pub from: Uuid,

    /// The Falcon command to send.
    pub data: Vec<String>,
}

#[derive(Debug)]
pub enum Request {
    Connect(String),
    Disconnect(String),
    Falcon(FalconRequest),
}

/// A message describing a response to a request.
#[derive(Debug, Clone)]
pub struct FalconResponse {
    /// The UUID of the connected client that this should be sent to.
    pub to: Uuid,

    /// The response from Falcon prefixed with the request.
    ///
    /// The structure looks like this:
    /// `[[request, data], [response, data]]`
    pub data: Vec<Vec<String>>,
}

/// A structure to track the connection with Falcon
pub struct FalconClient {
    context: zmq::Context,
    server_address: Option<String>, // None when not connected
    handles: Vec<JoinHandle<()>>,
    running: bool,
}

impl FalconClient {
    /// Create a new falcon client.
    pub fn new() -> Self {
        // INFO: The failure modes for socket creation are
        //       terminal so I'm fine with panicing if
        //       they happen. See: http://api.zeromq.org/master:zmq-socket#toc26

        FalconClient {
            context: zmq::Context::new(),
            server_address: None,
            handles: Default::default(),
            running: true,
        }
    }

    /// Connect to Falcon.
    ///
    /// This is actually done asynchronously. This function just sets
    /// the address of the Falcon server. Each seperate thread checks
    /// if the address is set and connects if it is.
    ///
    /// # Examples
    /// ```
    /// let falcon = Arc::new(RwLock::new(FalconClient::new()));
    /// falcon.write().unwrap().connect("localhost");
    /// assert!(falcon.read().unwrap().server_address == Some("localhost"));
    /// ```
    pub fn connect(&mut self, address: &str) {
        self.server_address = match self.server_address.clone() {
            Some(addr) => Some(addr),
            None => Some(address.to_string()),
        }
    }

    /// Disconnect from Falcon.
    ///
    /// The opposite of `connect()` above. Just unsets the address.
    ///
    /// # Examples.
    /// ```
    /// let falcon = Arc::new(RwLock::new(FalconClient::new()));
    /// falcon.write().unwrap().disconnect();
    /// assert!(falcon.read().unwrap().server_address.is_none());
    /// ```
    pub fn disconnect(&mut self) {
        self.server_address = None;
    }

    /// Stop the Falcon connection threads.
    pub fn terminate(&mut self) {
        self.running = false;
        for handle in &self.handles {
            handle.abort();
        }
    }
}

/// Used to indicate intentionally quiting the thread
/// or that the request socket should be recovered.
#[derive(PartialEq)]
enum ExitCondition {
    Recover,
    Quit,
}

/// Handle the synchronisation of requests to Falcon and replies from Falcon
///
/// This function should be run in its own thread. Requests come from the Proxy
/// and responses are forwarded to the Proxy. The Proxy is responsible for forwarding
/// these responses to the correct client.
///
/// This function implments the [Lazy Pirate](https://zguide.zeromq.org/docs/chapter4/#Client-Side-Reliability-Lazy-Pirate-Pattern)
/// Client-side reliability pattern. To achieve this a helper function is used to
/// create and drop a socket when an error condition is met. If the requests
/// thread should continue to operate then it simply calls the helper function
/// ([`run_requests`]) again which creates a new socket.
pub fn requests(
    client: Arc<RwLock<FalconClient>>,
    tx: mpsc::Sender<Result<FalconResponse>>,
    mut rx: mpsc::Receiver<Request>,
) {
    while client.read().unwrap().running {
        if ExitCondition::Quit == run_requests(client.clone(), &tx, rx.borrow_mut()) {
            break;
        }
    }

    info!(r#"Falcon requests thread exiting"#);
}

fn run_requests(
    client: Arc<RwLock<FalconClient>>,
    tx: &mpsc::Sender<Result<FalconResponse>>,
    rx: &mut mpsc::Receiver<Request>,
) -> ExitCondition {
    let mut exit = ExitCondition::Recover;
    let socket = client.read().unwrap().context.socket(zmq::REQ).unwrap();
    if let Err(err) = socket.set_linger(0) {
        error!("Could not change zmq socket linger value: {}", err);
    }
    if let Err(err) = socket.set_sndtimeo(500) {
        error!("Could not change zmq socket send timeout: {}", err);
    }

    while client.read().unwrap().running {
        match rx.blocking_recv() {
            Some(req) => {
                match req {
                    Request::Connect(addr) => {
                        if let Ok(()) =
                            socket.connect(format!("tcp://{}:{}", addr, REQUEST_PORT).as_str())
                        {
                            debug!("Connected to tcp://{}:{}", addr, REQUEST_PORT);
                        } else {
                            error!("Could not connect to tcp://{}:{}", addr, REQUEST_PORT);
                        }
                    }
                    Request::Disconnect(addr) => {
                        if let Err(err) =
                            socket.disconnect(format!("tcp://{}:{}", addr, REQUEST_PORT).as_str())
                        {
                            error!(
                                "Could not disconnect from tcp://{}:{} --> {:?}",
                                addr, REQUEST_PORT, err
                            );
                        } else {
                            debug!("Disconnected from tcp://{}:{}", addr, REQUEST_PORT);
                        }
                        break; // Force dropping the socket.
                    }
                    Request::Falcon(request) => {
                        loop {
                            // Send to socket, retrying on timeout
                            debug!("Sending request: {:?}", request);
                            match socket.send_multipart(request.data.iter(), 0) {
                                Ok(_) => match socket.poll(zmq::POLLIN, REQ_POLL_TIMEOUT) {
                                    Ok(1) => {
                                        let response = FalconResponse {
                                            to: request.from,
                                            data: vec![
                                                request.data,
                                                socket
                                                    .recv_multipart(0)
                                                    .unwrap()
                                                    .into_iter()
                                                    .map(|part| String::from_utf8(part).unwrap())
                                                    .collect(),
                                            ],
                                        };
                                        trace!("Forwarding response: {:?}", response);

                                        if let Err(err) = tx.blocking_send(Ok(response)) {
                                            error!("Could not send Falcon response: {}", err);
                                        }
                                        break;
                                    }

                                    otherwise => error!("No response received: {:?}", otherwise),
                                },
                                Err(msg) => {
                                    error!("Failed to send: {}", msg);
                                    if let Err(err) = tx.blocking_send(Err(Error::Zmq(msg))) {
                                        error!("Could not notify Proxy of Error: {}", err);
                                    }
                                    break;
                                }
                            };
                        }
                    }
                }
            }

            None => {
                info!(r#"Failed to receive a message, quitting"#);
                rx.close();
                exit = ExitCondition::Quit;
                break;
            }
        };
    }

    exit
}

/// Forward Event and Log messages from Falcon to the Proxy
///
/// This function should be run in its own thread. The Proxy is
/// responsible for sending these messages to the right clients.
pub fn subscribe(client: Arc<RwLock<FalconClient>>, tx: mpsc::Sender<ProxyMessage>) {
    let mut connected = false;
    let mut address = String::from("");
    let logging = client.read().unwrap().context.socket(zmq::SUB).unwrap();
    let ev = client.read().unwrap().context.socket(zmq::SUB).unwrap();

    let mut poll_items = [
        logging.as_poll_item(zmq::POLLIN),
        ev.as_poll_item(zmq::POLLIN),
    ];

    let mut msg = zmq::Message::new();

    while client.read().unwrap().running {
        match &client.read().unwrap().server_address {
            None => {
                if connected {
                    if let Err(err) =
                        logging.disconnect(format!("tcp://{}:{}", address, LOG_PORT).as_str())
                    {
                        error!(
                            "Could not disconnect from logging subscription tcp://{}:{} --> {:?}",
                            address, LOG_PORT, err
                        );
                    } else if let Err(err) =
                        ev.disconnect(format!("tcp://{}:{}", address, EVENT_PORT).as_str())
                    {
                        error!(
                            "Could not disconnect from event subscription tcp://{}:{} --> {:?}",
                            address, LOG_PORT, err
                        );
                    } else {
                        connected = false;
                        debug!("Disonnected from tcp://{}:{}", address, EVENT_PORT);
                    }
                }
                thread::sleep(Duration::from_millis(SLEEP_TIMEOUT));
            }
            Some(addr) => {
                if !connected {
                    if let Err(err) =
                        logging.connect(format!("tcp://{}:{}", addr, LOG_PORT).as_str())
                    {
                        error!(
                            "Could not connect to logging subscription tcp://{}:{} --> {:?}",
                            addr, LOG_PORT, err
                        );
                    } else if let Err(err) =
                        ev.connect(format!("tcp://{}:{}", addr, EVENT_PORT).as_str())
                    {
                        error!(
                            "Could not connect to event subscription tcp://{}:{} --> {:?}",
                            addr, EVENT_PORT, err
                        );
                    } else {
                        logging.set_subscribe(b"").unwrap();
                        ev.set_subscribe(b"").unwrap();
                        debug!("Connected to tcp://{}:{}", addr, LOG_PORT);
                        debug!("Connected to tcp://{}:{}", addr, EVENT_PORT);
                        connected = true;
                        address = addr.clone();
                    }
                }

                match zmq::poll(&mut poll_items, SUB_POLL_TIMEOUT) {
                    Ok(_) => {
                        if poll_items[0].is_readable() {
                            let data: Vec<String> = logging
                                .recv_multipart(0)
                                .unwrap()
                                .into_iter()
                                .map(|part| String::from_utf8(part).unwrap())
                                .collect();
                            debug!("Received log data: {:?}", data);
                            let res = FalconLog::try_from(&data)
                                .map(|message| tx.blocking_send(ProxyMessage::Log(message)));
                            if let Err(e) = res {
                                error!("Failed to send logs: #{:?}", e);
                                break;
                            }
                        }

                        if poll_items[1].is_readable() {
                            ev.recv(&mut msg, 0).unwrap();
                            match FalconEvents::try_from(&*msg) {
                                Ok(events) => {
                                    if let Err(_) = tx.blocking_send(ProxyMessage::Events(events)) {
                                        error!("Failed to send events");
                                        break;
                                    }
                                }
                                Err(error) => error!("{}", error),
                            }
                        }
                    }

                    Err(err) => error!("Error polling zmq: {}", err),
                }
            }
        }
    }

    debug!("Falcon subscription thread exiting");
}

#![warn(missing_docs)]
//! The main entry point for the application. Launches a webserver and a browser.

mod client;
mod error;
mod falcon;
mod model;
mod proxy;
mod server;

use log::error as log_error;
use tokio::time;

const SERVER_PORT: u16 = 51966;

#[tokio::main]
async fn main() {
    env_logger::init();
    let server = server::Server::new(SERVER_PORT);

    tokio::spawn(async {
        time::sleep(time::Duration::from_secs(1)).await;
        if let Err(err) = webbrowser::open(&format!("http://localhost:{}/", SERVER_PORT)) {
            log_error!("Unable to launch browser: {}", err);
        }
    });

    server.run().await;
}

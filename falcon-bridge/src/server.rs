//! Webserver for serving the GUI over HTTP and connecting the WebSocket.
//!
//! This module spawns a webserver which responds with the client GUI
//! on all paths except `/bridge` which is the [WebSocket] path.
//! `Server` is parameterised by the port number on which to listen
//! for connections.
//!
//! # Examples
//! ```
//! let server = server::Server::new(7778);
//! server.run().await;
//! ```
//!
//! [WebSocket]: https://en.wikipedia.org/wiki/WebSocket

use std::result;
use std::sync::Arc;

use async_stream::try_stream;
use futures::{StreamExt, TryStreamExt};
use futures_core::stream::Stream;
use log::{debug, error};
use tokio::sync::broadcast;
use tokio::sync::mpsc;
use tokio::sync::mpsc::UnboundedSender;
use tokio_stream::wrappers::UnboundedReceiverStream;
use warp::ws::WebSocket;
use warp::Filter;

use crate::client::Client;
use crate::error;
use crate::model;
use crate::proxy::Proxy;

/// A `Server` listens on a TCP port `port` and forwards messages to a `Proxy`
pub struct Server {
    port: u16,
    proxy: Arc<Proxy>,
}

impl Server {
    /// Create a new server.
    pub fn new(port: u16) -> Self {
        Server {
            port,
            proxy: Arc::new(Proxy::new()),
        }
    }

    /// Run the server.
    pub async fn run(&self) {
        // proxy_receiver is given to the Proxy. All clients send messages on this
        // channel to the Proxy.
        let (proxy_sender, proxy_receiver) = mpsc::unbounded_channel::<model::InputParcel>();
        let proxy = self.proxy.clone();

        debug!("Running the server @ 127.0.0.1:{}", self.port);

        let bridge = warp::path("bridge")
            .and(warp::ws())
            .and(warp::any().map(move || proxy_sender.clone()))
            .and(warp::any().map(move || proxy.clone()))
            .map(
                move |ws: warp::ws::Ws,
                      proxy_sender: UnboundedSender<model::InputParcel>,
                      proxy: Arc<Proxy>| {
                    ws.on_upgrade(move |web_socket| async move {
                        tokio::spawn(Self::process_client(proxy, web_socket, proxy_sender));
                    })
                },
            );

        let shutdown = async {
            tokio::signal::ctrl_c()
                .await
                .expect("failed to install CTRL+C signal handler");
        };

        let cors = warp::cors()
            .allow_origin("http://localhost:1234")
            .allow_origin("http://localhost:8000")
            .allow_origin("http://localhost:7778")
            .allow_methods(vec!["GET"]);

        let path = warp::path::end()
            .map(|| warp::reply::html(std::include_str!("index.html")))
            .with(cors)
            .or(bridge);

        let (_, serving) =
            warp::serve(path).bind_with_graceful_shutdown(([127, 0, 0, 1], self.port), shutdown);

        let running_proxy = self.proxy.run(proxy_receiver);

        tokio::select! {
            _ = serving => {},
            _ = running_proxy => {},
        }

        debug!("Server terminated");
        self.proxy.terminate().await;
    }

    /// Accept a websocket connection and create a new `Client`
    async fn process_client(
        proxy: Arc<Proxy>,
        web_socket: WebSocket,
        proxy_sender: UnboundedSender<model::InputParcel>,
    ) {
        let client = Client::new();

        // TODO: This is temporary while Streams are being stabilised in std
        let client_receiver = stream_subscription(proxy.subscribe(client.id).await);
        let (ws_sink, ws_source) = web_socket.split();

        debug!("Client {} connected", client.id);

        let reading = client.read_input(ws_source).try_for_each(|message| async {
            proxy_sender.send(message).map_err(error::Error::from)
        });

        let (tx, rx) = mpsc::unbounded_channel();
        tokio::spawn(UnboundedReceiverStream::new(rx).forward(ws_sink));

        let writing = client
            .write_output(client_receiver)
            .try_for_each(|message| async { tx.send(Ok(message)).map_err(error::Error::from) });

        if let Err(err) = tokio::select! {
            result = reading => result,
            result = writing => result,
        } {
            error!("Client connection error: {}", err);
        }

        proxy.on_disconnect(client.id).await;
        debug!("Client {} disconnected", client.id);
    }
}

/// Treat the `Proxy` subscription as an infinite stream of messages
fn stream_subscription(
    mut subscription: broadcast::Receiver<model::OutputParcel>,
) -> impl Stream<Item = result::Result<model::OutputParcel, broadcast::error::RecvError>> {
    try_stream! {
        loop {
            let item = subscription.recv().await?;
            yield item;
        }
    }
}

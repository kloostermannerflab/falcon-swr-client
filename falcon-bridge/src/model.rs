#![warn(missing_docs)]
//! This module defines shared data structures.

use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};
use std::convert::TryFrom;
use uuid::Uuid;

extern crate flatbuffers;
#[allow(dead_code, unused_imports)]
#[path = "datatype_generated.rs"]
mod datatype_generated;
pub use datatype_generated::RootMsg;

extern crate flexbuffers;
use flexbuffers::Reader;

/// Equivalent of the multichanneldata type in Falcon
#[derive(Debug, Serialize, Clone, Default)]
pub struct FalconSeries {
    // Equivalent of the hardware timestamp in Falcon
    timestamp: u64,

    // Can read any multichanneldata type but will be used
    // for this specific configuration:
    // Stats + threshold samples from the ripple detector in Falcon
    signal: Vec<f64>,
}

/// Equivalent of the event type in Falcon
#[derive(Debug, Serialize, Clone, Default)]
pub struct FalconTTLEvent {
    // Equivalent of the hardware timestamp in Falcon
    timestamp: u64,

    // Name of the event
    name: String,
}

#[derive(Debug, Serialize, Clone)]
#[serde(tag = "category", content = "signal")]
pub enum FalconSignal {
    #[serde(rename = "events")]
    Event(FalconTTLEvent),
    #[serde(rename = "series")]
    Series(FalconSeries),
}
/// A Falcon log message.
#[derive(Debug, Clone, Serialize)]
pub struct FalconLog {
    /// The log level.
    level: String,

    /// A unix microsecond timestamp.
    timestamp: i64,

    /// The log message.
    message: String,
}

impl TryFrom<&Vec<String>> for FalconLog {
    type Error = String;

    fn try_from(data: &Vec<String>) -> Result<Self, Self::Error> {
        match NaiveDateTime::parse_from_str(data[1].as_str(), "%Y/%m/%d %H:%M:%S %f") {
            Ok(datetime) => Ok(FalconLog {
                level: data[0].clone(),
                timestamp: datetime.timestamp() * 1000,
                // Fatal level messages don't have a message field.
                message: data.get(2).unwrap_or(&String::from("")).to_string(),
            }),

            Err(err) => Err(format!("Cannot parse timestamp {}: {}", data[1], err)),
        }
    }
}

/// Represents a collected series of events from the
/// Falcon server.
#[derive(Debug, Clone, Serialize)]
pub struct FalconEvents {
    /// The client supplied name for where these events
    /// should be directed.
    pub target: String,

    /// The collection of events.
    pub events: Vec<FalconSignal>,
}

impl TryFrom<&[u8]> for FalconEvents {
    type Error = String;

    fn try_from(bytes: &[u8]) -> Result<Self, Self::Error> {
        // Insert here flatbuffers deserialization
        let data =  datatype_generated::root_as_root_msg(bytes).expect("Deserialization error: Unable to read the buffer.
                                                                               Are you sure the ZMQserializer format in
                                                                               falcon is set to flatbuffer?");

        let read_root = Reader::get_root(data.data().unwrap()).unwrap().as_map();

        let events = match read_root.idx("type").as_str() {
            "event" => {
                let ev: Vec<FalconSignal> = vec![FalconSignal::Event(FalconTTLEvent {
                    timestamp: read_root.idx("hardware_ts").as_u64(),
                    name: read_root.idx("event").as_str().to_string(),
                })];
                Ok(ev)
            }

            "multichannel" => {
                let signal = read_root
                    .idx("data")
                    .as_vector()
                    .iter()
                    .filter_map(|reader| reader.get_f64().ok())
                    .collect::<Vec<f64>>();
                let ts = read_root
                    .idx("timestamps")
                    .as_vector()
                    .iter()
                    .filter_map(|reader| reader.get_u64().ok())
                    .collect::<Vec<u64>>();
                let nchannels = read_root.idx("nchannels").as_u64();

                let ev = signal
                    .chunks(usize::try_from(nchannels).unwrap())
                    .zip(ts.iter()) // Now we're iterating over elements of type (&[f64], &u64)
                    .map(|(series, timestamp)| {
                        FalconSignal::Series(FalconSeries {
                            timestamp: *timestamp,
                            signal: series.to_vec(),
                        })
                    })
                    .collect::<Vec<FalconSignal>>();
                Ok(ev)
            }

            _ => Err(format!(
                "Falcon datatype {} not yet implemented",
                read_root.idx("type").as_str()
            )),
        };

        let processor_name = data.source().unwrap().processor().unwrap();

        match events {
            Ok(events) => Ok(FalconEvents {
                target: processor_name.to_string(),
                events: events,
            }),

            Err(error) => Err(error),
        }
    }
}

/// Messages that are not intended to be forwarded to Falcon but
/// are used to control the behaviour of this proxy application.
#[derive(Debug, Clone, Deserialize)]
#[serde(tag = "method", content = "args")]
pub enum ProxyControl {
    /// Connect to a falcon server with this address.
    #[serde(rename = "connect")]
    Connect(String),

    /// Disconnect from a Falcon server.
    #[serde(rename = "disconnect")]
    Disconnect(String),

    /// Kill the proxy process.
    #[serde(rename = "terminate")]
    Terminate,

    /// Request the Bridge version string
    #[serde(rename = "version")]
    Version,
}

/// Represents the structure of a message from a connected Client. All
/// client messages muct follow this structure.
#[derive(Debug, Clone, Deserialize)]
#[serde(tag = "category", content = "args")]
pub enum ClientRequest {
    #[serde(rename = "control")]
    Control(ProxyControl),

    #[serde(rename = "falcon")]
    Falcon(Vec<String>),

    #[serde(rename = "ping")]
    Ping,
}

/// Respresents the structure of messages sent to a connected Client.
#[derive(Debug, Clone, Serialize)]
#[serde(tag = "category", content = "data")]
pub enum ProxyMessage {
    /// Response to a ping message
    #[serde(rename = "pong")]
    Pong,

    /// Log messages from Falcon
    #[serde(rename = "log")]
    Log(FalconLog),

    /// Event messages from Falcon
    #[serde(rename = "events")]
    Events(FalconEvents),

    /// Response from Falcon to a client request
    #[serde(rename = "response")]
    Response(Vec<Vec<String>>),

    /// Bridge acknowledgement of a client request
    #[serde(rename = "ack")]
    Ack(Vec<String>),

    /// Bridge error
    #[serde(rename = "error")]
    Error(String),
}

/// Wraps a message from the Server to the Proxy.
#[derive(Debug, Clone)]
pub struct InputParcel {
    pub client_id: Uuid,
    pub input: ClientRequest,
}

impl InputParcel {
    pub fn new(client_id: Uuid, input: ClientRequest) -> Self {
        InputParcel { client_id, input }
    }
}

/// Wraps a message from the Proxy to the Server.
#[derive(Debug, Clone)]
pub struct OutputParcel {
    pub client_id: Uuid,
    pub output: ProxyMessage,
}

impl OutputParcel {
    pub fn new(client_id: Uuid, output: ProxyMessage) -> Self {
        OutputParcel { client_id, output }
    }
}

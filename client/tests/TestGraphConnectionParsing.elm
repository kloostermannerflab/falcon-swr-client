module TestGraphConnectionParsing exposing (suite)

import Expect
import FalconProcessor exposing (FalconConnector, parseConnector, saveConnector)
import Test exposing (Test, describe, test)


suite : Test
suite =
    describe "standard"
        [ test "fully specified"
            (\_ ->
                let
                    connection =
                        { processor = "processor"
                        , prt =
                            Just
                                { name = "port"
                                , slot = Just "slot"
                                }
                        }
                in
                Expect.equal
                    [ connection
                    ]
                    (parseConnector "processor.port.slot")
            )
        , test "fully specified roundtrip" <|
            \_ ->
                let
                    connection =
                        { processor = "processor"
                        , prt =
                            Just
                                { name = "port"
                                , slot = Just "slot"
                                }
                        }
                in
                Expect.equal [ connection ] <| parseConnector <| saveConnector connection
        , test "processor name has underscore" <|
            \_ ->
                Expect.equal
                    [ { processor = "proc_essor"
                      , prt = Nothing
                      }
                    ]
                    (parseConnector "proc_essor")
        , test "no slot"
            (\_ ->
                Expect.equal
                    [ { processor = "processor"
                      , prt =
                            Just
                                { name = "port"
                                , slot = Nothing
                                }
                      }
                    ]
                    (parseConnector "processor.port")
            )
        , test "no slot roundtrip" <|
            \_ ->
                let
                    connection =
                        { processor = "processor"
                        , prt =
                            Just
                                { name = "port"
                                , slot = Nothing
                                }
                        }
                in
                Expect.equal [ connection ] <| parseConnector <| saveConnector connection
        , test "no port"
            (\_ ->
                Expect.equal
                    [ { processor = "processor"
                      , prt = Nothing
                      }
                    ]
                    (parseConnector "processor")
            )
        , test "no port roundtrip" <|
            \_ ->
                let
                    connection =
                        { processor = "processor"
                        , prt = Nothing
                        }
                in
                Expect.equal [ connection ] <| parseConnector <| saveConnector connection
        , test "single range port" <|
            \_ ->
                Expect.equal
                    [ { processor = "processor"
                      , prt =
                            Just
                                { name = "port1"
                                , slot = Nothing
                                }
                      }
                    ]
                    (parseConnector "processor.port( 1 )")
        , test "multiple listed ports"
            (\_ ->
                Expect.equal
                    [ { processor = "processor"
                      , prt =
                            Just
                                { name = "port1"
                                , slot = Nothing
                                }
                      }
                    , { processor = "processor"
                      , prt =
                            Just
                                { name = "port3"
                                , slot = Nothing
                                }
                      }
                    ]
                    (parseConnector "processor.port(1,3)")
            )
        , test "multiple listed ports with spaces" <|
            \_ ->
                "processor.port( 0,  0 )"
                    |> parseConnector
                    |> Expect.equal
                        [ { processor = "processor"
                          , prt =
                                Just
                                    { name = "port0"
                                    , slot = Nothing
                                    }
                          }
                        ]
        , test "multiple listed slots"
            (\_ ->
                Expect.equal
                    [ { processor = "processor"
                      , prt =
                            Just
                                { name = "port"
                                , slot = Just "slot5"
                                }
                      }
                    , { processor = "processor"
                      , prt =
                            Just
                                { name = "port"
                                , slot = Just "slot10"
                                }
                      }
                    , { processor = "processor"
                      , prt =
                            Just
                                { name = "port"
                                , slot = Just "slot21"
                                }
                      }
                    ]
                    (parseConnector "processor.port.slot(5,10,21)")
            )
        , test "range of ports" <|
            \_ ->
                "processor.port(0-1)"
                    |> parseConnector
                    |> Expect.equal
                        [ FalconConnector "processor"
                            (Just
                                { name = "port0"
                                , slot = Nothing
                                }
                            )
                        , FalconConnector "processor"
                            (Just
                                { name = "port1"
                                , slot = Nothing
                                }
                            )
                        ]
        , test "range of ports with comma first" <|
            \_ ->
                "processor.port(1,4-5)"
                    |> parseConnector
                    |> Expect.equal
                        [ FalconConnector "processor"
                            (Just
                                { name = "port1"
                                , slot = Nothing
                                }
                            )
                        , FalconConnector "processor"
                            (Just
                                { name = "port4"
                                , slot = Nothing
                                }
                            )
                        , FalconConnector "processor"
                            (Just
                                { name = "port5"
                                , slot = Nothing
                                }
                            )
                        ]
        , test "range of ports with comma last" <|
            \_ ->
                "processor.port(4- 5, 10)"
                    |> parseConnector
                    |> Expect.equal
                        [ FalconConnector "processor"
                            (Just
                                { name = "port4"
                                , slot = Nothing
                                }
                            )
                        , FalconConnector "processor"
                            (Just
                                { name = "port5"
                                , slot = Nothing
                                }
                            )
                        , FalconConnector "processor"
                            (Just
                                { name = "port10"
                                , slot = Nothing
                                }
                            )
                        ]
        ]

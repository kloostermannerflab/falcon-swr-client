module TestGraphDiff exposing (suite)

import Expect
import FalconGraph exposing (FalconGraph, GraphMsg(..), dropDirty, loadGraph, unionGraphs)
import FalconProcessor exposing (OptionValue(..))
import Result.Extra as ResultE
import Test exposing (Test, describe, test)


singletonGraph : String
singletonGraph =
    """falcon:
    version: 1
graph:
    processors:
        A:
            class: proc
    connections:
options:
    A:
        options:
            test: false
    """


otherGraph : String
otherGraph =
    """falcon:
    version: 1
graph:
    processors:
        Other:
            class: proc
    connections:
options:
    Other:
        options:
            test: false
    """


plus1Graph : String
plus1Graph =
    """falcon:
    version: 1
graph:
    processors:
        A:
            class: proc
        B:
            class: proc
    connections:
options:
    A:
        options:
            test: false
    """


connectedGraph : String
connectedGraph =
    """falcon:
    version: 1
graph:
    processors:
        A:
            class: proc
        B:
            class: proc
    connections:
        - A.port=B.port
options:
    A:
        options:
            test: false
    """


connectedRGraph : String
connectedRGraph =
    """falcon:
    version: 1
graph:
    processors:
        A:
            class: proc
        B:
            class: proc
    connections:
        - B.port=A.port
options:
    A:
        options:
            test: false
    """


connected2SlotGraph : String
connected2SlotGraph =
    """falcon:
    version: 1
graph:
    processors:
        A:
            class: proc
        B:
            class: proc
    connections:
        - A.port.0=B.port.0
        - A.port.1=B.port.1
options:
    A:
        options:
            test: false
    """


connected2SlotGraph2 : String
connected2SlotGraph2 =
    """falcon:
    version: 1
graph:
    processors:
        A:
            class: proc
        B:
            class: proc
    connections:
        - A.port.0=B.port.1
        - A.port.1=B.port.0
options:
    A:
        options:
            test: false
    """


stateGraph : String
stateGraph =
    """falcon:
    version: 1
graph:
    processors:
        A:
            class: proc
            states:
                test-state:
                    permission: W
                    value: false
                    description: "This is a test"
    connections:
options:
    A:
        options:
            test: false
    """


stateGraphMod : String
stateGraphMod =
    """falcon:
    version: 1
graph:
    processors:
        A:
            class: proc
            states:
                test-state:
                    permission: W
                    value: true
                    description: "This is a test"
    connections:
options:
    A:
        options:
            test: false
    """


helpUpdateGraph : Int -> String -> OptionValue -> FalconGraph -> FalconGraph
helpUpdateGraph node optName optValue graph =
    FalconGraph.update (OptionsChange node optName optValue) graph
        |> Tuple.first


suite : Test
suite =
    describe "Diffing graphs"
        [ test "Diff an single-node graph with itself" <|
            \_ ->
                let
                    singleton : Result String FalconGraph
                    singleton =
                        loadGraph 0 0 singletonGraph
                in
                Result.andThen (\graph -> unionGraphs graph graph) singleton
                    |> Expect.equal singleton
        , test "Diff a graph with slightly modified options" <|
            \_ ->
                let
                    graph : Result String FalconGraph
                    graph =
                        loadGraph 0 0 singletonGraph

                    modified : Result String FalconGraph
                    modified =
                        Result.map
                            (dropDirty << helpUpdateGraph 0 "test" (OptionBool True))
                            graph
                in
                Result.map2 (\orig mod -> unionGraphs orig mod) graph modified
                    |> ResultE.join
                    |> Expect.equal modified
        , test "Diff a graph with a graph containing different nodes" <|
            \_ ->
                let
                    graph : Result String FalconGraph
                    graph =
                        loadGraph 0 0 singletonGraph

                    other : Result String FalconGraph
                    other =
                        loadGraph 0 0 otherGraph
                in
                Result.map2 (\orig new -> unionGraphs orig new) graph other
                    |> ResultE.join
                    |> Expect.equal (Err "These graphs are not the same")
        , test "Diff a graph with a graph containing the same PLUS an additional node" <|
            \_ ->
                let
                    graph : Result String FalconGraph
                    graph =
                        loadGraph 0 0 singletonGraph

                    other : Result String FalconGraph
                    other =
                        loadGraph 0 0 plus1Graph
                in
                Result.map2 (\orig new -> unionGraphs orig new) graph other
                    |> ResultE.join
                    |> Expect.equal (Err "These graphs are not the same")
        , test "Diff a graph with a graph containing a reversed connection" <|
            \_ ->
                let
                    graph : Result String FalconGraph
                    graph =
                        loadGraph 0 0 connectedGraph

                    other : Result String FalconGraph
                    other =
                        loadGraph 0 0 connectedRGraph
                in
                Result.map2 (\orig new -> unionGraphs orig new) graph other
                    |> ResultE.join
                    |> Expect.equal (Err "These graphs are not the same")
        , test "Diff a graph with a graph containing the same connections but to different slots" <|
            \_ ->
                let
                    graph : Result String FalconGraph
                    graph =
                        loadGraph 0 0 connected2SlotGraph

                    other : Result String FalconGraph
                    other =
                        loadGraph 0 0 connected2SlotGraph2
                in
                Result.map2 (\orig new -> unionGraphs orig new) graph other
                    |> ResultE.join
                    |> Expect.equal (Err "These graphs are not the same")
        , test "Diff a graph with modified state" <|
            \_ ->
                let
                    graph : Result String FalconGraph
                    graph =
                        loadGraph 0 0 stateGraph

                    other : Result String FalconGraph
                    other =
                        loadGraph 0 0 stateGraphMod
                in
                Result.map2 (\orig new -> unionGraphs orig new) graph other
                    |> ResultE.join
                    |> Expect.equal other
        ]

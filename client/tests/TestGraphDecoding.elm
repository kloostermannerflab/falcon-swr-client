module TestGraphDecoding exposing (suite)

import Dict exposing (Dict)
import Expect
import FalconGraph exposing (EdgeLabel, loadGraph, showEdgeLabel)
import FalconProcessor
    exposing
        ( FalconConnector
        , FalconPort
        , FalconProcessor
        , OptionValue(..)
        , State
        , StatePermission(..)
        , StateValue(..)
        )
import Graph
import List.Extra as ListE
import Test exposing (Test, describe, test)


simpleGraph : String
simpleGraph =
    """
falcon:
  version: 1
graph:
  processors:
    source:
      class: NlxReader
    sink:
      class: ZMQSerializer
  connections:
    - source.hp=sink.data
options:
  source:
    options:
     some option: 4
"""


graphFragment : String
graphFragment =
    """
falcon:
  version: 1
graph:
  processors:
    source:
      class: NlxReader
    ripple_filter (1-2):
      class: MultiChannelFilter
    HIPPOCAMPUS_detector:
      class: RippleDetector
  connections:
    - source.hp=ripple_filter1.data
    - source.cx=ripple_filter2.data
    - ripple_filter1.data=HIPPOCAMPUS_detector.data.0
    - ripple_filter2.data=HIPPOCAMPUS_detector.data.1
options:
"""


misoFragment : String
misoFragment =
    """
falcon:
  version: 1
graph:
  processors:
    upstream (1-2):
      class: UpStream
    downstream:
      class: DownStream

  connections:
    - upstream(1-2).out=p:in(1-2).f:downstream
options:
"""


fullGraphText : String
fullGraphText =
    """
falcon:
  version: 1
graph:
   processors:
        source:
            class: NlxReader
            advanced:
                threadpriority: 100
                threadcore: 0
        ripple_filter (1-2):
            class: MultiChannelFilter
            options:
               filter:
                  file: iir_ripple_low_delay/matlab_design/iir_ripple_low_delay.filter
        HIPPOCAMPUS_detector:
            class: RippleDetector
        CORTEX_detector :
            class: RippleDetector
        eventfilter:
            class: EventFilter
        networksink:
            class: ZMQSerializer
        eventsink:
            class: EventLogger
        datasink_ev:
            class: FileSerializer
        datasink_ripplestats:
            class: FileSerializer
        stimulation_trigger:
            class: EventDelayed


   connections:
        - source.hp=ripple_filter1.data
        - source.cx=ripple_filter2.data
        - ripple_filter1.data=HIPPOCAMPUS_detector.data
        - ripple_filter2.data=CORTEX_detector.data
        - HIPPOCAMPUS_detector.events=eventfilter.events
        - CORTEX_detector.events=eventfilter.blocking_events
        - HIPPOCAMPUS_detector.statistics=networksink.data
        - eventfilter.events=eventsink.events
        - eventfilter.events=datasink_ev.data
        - eventfilter.events=stimulation_trigger.events
        - HIPPOCAMPUS_detector.statistics=datasink_ripplestats.data

options:
    source:
       options:
          channelmap:
           hp: [13,20]
           cx: [9]
    HIPPOCAMPUS_detector:
        options:
            threshold dev: 14
            smooth time: 7 # in seconds
            detection lockout time: 50 #ms
            stream events: true
            stream statistics: true
            statistics buffer size: 1.0 # sec
            statistics downsample factor: 4
            use power: true
    CORTEX_detector :
        options:
            threshold dev: 12
            smooth time: 8 # in seconds
            detection lockout time: 40 #ms
            stream events: true
            stream statistics: false
            statistics buffer size: 1.0 # sec
            statistics downsample factor: 4
            use power: true
    eventfilter:
        options:
            target event: ripple
            block duration: 40
            sync time: 1.5
            block wait time: 4 # below 3.5, asynch can occur
            detection criterion: "any" # 'any', 'all' or number
            discard warnings: false
    stimulation_trigger:
        options:
            delayed mode: false
            lockout period: 250

"""


graphYamlResponse : String
graphYamlResponse =
    """
falcon:
  version: 1
graph:
  processors:
    CORTEX-detector:
      inports:
        data:
          datatype: multichannel
          nslots_min: 1
          nslots_max: 1
          cache: false
          time_out: -1
      outports:
        events:
          datatype: event
          nslots_min: 1
          nslots_max: 1
          buffer_size: 200
          wait_strategy: blocking
        statistics:
          datatype: multichannel
          nslots_min: 1
          nslots_max: 1
          buffer_size: 200
          wait_strategy: blocking
      states:
        analysis-lockout-time:
          permission: W
          value: 30.000000
          description: ""
        detection-enabled:
          permission: N
          description: ""
        deviation:
          permission: R
          value: 0.000000
          description: ""
        stream-events:
          permission: W
          value: true
          description: ""
        stream-statistics:
          permission: W
          value: false
          description: ""
      class: RippleDetector
    eventfilter:
      inports:
        blocking-events:
          datatype: event
          nslots_min: 1
          nslots_max: 256
          cache: false
          time_out: 0
        events:
          datatype: event
          nslots_min: 1
          nslots_max: 256
          cache: false
          time_out: 0
      outports:
        events:
          datatype: event
          nslots_min: 1
          nslots_max: 1
          buffer_size: 200
          wait_strategy: blocking
      class: EventFilter
      options:
        target event: ripple
        block duration: 40
        sync time: 1.5
        block wait time: 4
        detection criterion: any
        discard warnings: false
    networksink:
      inports:
        data:
          datatype: any
          nslots_min: 1
          nslots_max: 256
          cache: false
          time_out: 0
      class: ZMQSerializer
      options:
        encoding: binary
        format: compact
    ripple_filter1:
      inports:
        data:
          datatype: multichannel
          nslots_min: 0
          nslots_max: 256
          cache: false
          time_out: -1
      outports:
        data:
          datatype: multichannel
          nslots_min: 0
          nslots_max: 256
          buffer_size: 200
          wait_strategy: blocking
      class: MultiChannelFilter
      options: &1
        filter:
          file: iir_ripple_low_delay/matlab_design/iir_ripple_low_delay.filter
    ripple_filter2:
      inports:
        data:
          datatype: multichannel
          nslots_min: 0
          nslots_max: 256
          cache: false
          time_out: -1
      outports:
        data:
          datatype: multichannel
          nslots_min: 0
          nslots_max: 256
          buffer_size: 200
          wait_strategy: blocking
      class: MultiChannelFilter
      options: *1
    source:
      outports:
        cx:
          datatype: multichannel
          nslots_min: 1
          nslots_max: 1
          buffer_size: 500
          wait_strategy: blocking
        hp:
          datatype: multichannel
          nslots_min: 1
          nslots_max: 1
          buffer_size: 500
          wait_strategy: blocking
      class: NlxReader
      options:
        address: 127.0.0.1
        port: 5000
        batch size: 3
        update interval: 10
        channelmap:
          hp: [13, 20]
          cx: [9]
  connections:
    - source.hp.0=ripple-filter.data.0
    - ripple-filter1.data.1=ripple_filter2.data.0
    - ripple-filter2.data.1=CORTEX-detector.data.0
    - CORTEX-detector.events.0=eventfilter.blocking-events.0
    - eventfilter.events.0=networksink.data.0
  states: ~
"""


multiPortConnections : String
multiPortConnections =
    """falcon:
    version: 1
graph:
    processors:
        A:
            class: proc
        B:
            class: proc
    connections:
        - p:port.f:A=p:some-port.f:B
        - p:port.f:A=p:other-port.f:B
"""


suite : Test
suite =
    describe "decoding graphs"
        [ test "simple size" <|
            \_ ->
                loadGraph 0 0 simpleGraph
                    |> Result.map (Graph.size << .graph)
                    |> Expect.equal (Ok 2)
        , test "complex size" <|
            \_ ->
                loadGraph 0 0 graphFragment
                    |> Result.map (Graph.size << .graph)
                    |> Expect.equal (Ok 4)
        , test "miso size" <|
            \_ ->
                loadGraph 0 0 misoFragment
                    |> Result.map (Graph.size << .graph)
                    |> Expect.equal (Ok 3)
        , test "fullGraph size" <|
            \_ ->
                loadGraph 0 0 fullGraphText
                    |> Result.map (Graph.size << .graph)
                    |> Expect.equal (Ok 11)
        , test "graph yaml size" <|
            \_ ->
                loadGraph 0 0 graphYamlResponse
                    |> Result.map (Graph.size << .graph)
                    |> Expect.equal (Ok 6)
        , test "simple: check first node" <|
            \_ ->
                let
                    result : Result String (Maybe FalconProcessor)
                    result =
                        loadGraph 0 0 simpleGraph
                            |> Result.map
                                (Maybe.map (.processor << .value << .label << .node)
                                    << Graph.get 0
                                    << .graph
                                )

                    expected : FalconProcessor
                    expected =
                        FalconProcessor "sink" "ZMQSerializer" [] Dict.empty Dict.empty
                in
                Expect.equal result (Ok (Just expected))
        , test "simple: check second node" <|
            \_ ->
                let
                    result : Result String (Maybe FalconProcessor)
                    result =
                        loadGraph 0 0 simpleGraph
                            |> Result.map
                                (Maybe.map (.processor << .value << .label << .node)
                                    << Graph.get 1
                                    << .graph
                                )

                    expected : FalconProcessor
                    expected =
                        FalconProcessor
                            "source"
                            "NlxReader"
                            []
                            (Dict.fromList [ ( "some option", OptionNum 4 ) ])
                            Dict.empty
                in
                Expect.equal result (Ok (Just expected))
        , test "miso: check multi-in" <|
            \_ ->
                let
                    result : Result String (List String)
                    result =
                        loadGraph 0 0 misoFragment
                            |> Result.map
                                (ListE.andThen (showEdgeLabel << .label)
                                    << Graph.edges
                                    << .graph
                                )

                    expected : List String
                    expected =
                        [ "p:out.f:upstream2=p:in2.f:downstream"
                        , "p:out.f:upstream1=p:in1.f:downstream"
                        ]
                in
                Expect.equal result (Ok expected)
        , test "complex: roundtrip connections" <|
            \_ ->
                let
                    result : Result String (List String)
                    result =
                        loadGraph 0 0 graphFragment
                            |> Result.map
                                (ListE.andThen (showEdgeLabel << .label)
                                    << Graph.edges
                                    << .graph
                                )

                    expected : List String
                    expected =
                        [ "p:cx.f:source=p:data.f:ripple_filter2"
                        , "p:hp.f:source=p:data.f:ripple_filter1"
                        , "p:data.f:ripple_filter2=s:1.p:data.f:HIPPOCAMPUS_detector"
                        , "p:data.f:ripple_filter1=s:0.p:data.f:HIPPOCAMPUS_detector"
                        ]
                in
                Expect.equal result (Ok expected)
        , test "graph yaml states" <|
            \_ ->
                let
                    result : Result String (List (Dict String State))
                    result =
                        loadGraph 0 0 graphYamlResponse
                            |> Result.map
                                (List.map (\node -> node.label.value.processor.state)
                                    << List.filter
                                        (\node ->
                                            not (Dict.isEmpty node.label.value.processor.state)
                                        )
                                    << Graph.nodes
                                    << .graph
                                )

                    expected : List (Dict String State)
                    expected =
                        [ Dict.fromList
                            [ ( "analysis-lockout-time", State (StateNum 30) Write )
                            , ( "deviation", State (StateNum 0) Read )
                            , ( "stream-events", State (StateBool True) Write )
                            , ( "stream-statistics", State (StateBool False) Write )
                            ]
                        ]
                in
                Expect.equal result (Ok expected)
        , test "Decode graph with 2 connections on the same port" <|
            \_ ->
                let
                    result : Result String (List EdgeLabel)
                    result =
                        loadGraph 0 0 multiPortConnections
                            |> Result.map (List.map .label << Graph.edges << .graph)

                    expected : List EdgeLabel
                    expected =
                        [ [ { from = FalconConnector "A" (Just (FalconPort "port" Nothing))
                            , to = FalconConnector "B" (Just (FalconPort "some-port" Nothing))
                            }
                          , { from = FalconConnector "A" (Just (FalconPort "port" Nothing))
                            , to = FalconConnector "B" (Just (FalconPort "other-port" Nothing))
                            }
                          ]
                        ]
                in
                Expect.equal result (Ok expected)
        ]

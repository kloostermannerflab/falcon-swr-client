module TestGraphEncoding exposing (suite)

import Expect
import FalconGraph exposing (loadGraph, saveGraph)
import Graph
import Test exposing (Test, describe, test)


simpleGraph : String
simpleGraph =
    """
falcon:
  version: 1
graph:
  processors:
    source:
      class: NlxReader
    sink:
      class: ZMQSerializer
  connections:
    - source.hp=sink.data
options:
  source:
    options:
     some option: 4
"""


multiPortConnections : String
multiPortConnections =
    """falcon:
    version: 1
graph:
    processors:
        A:
            class: proc
        B:
            class: proc
        C:
            class: proc
    connections:
        - p:port.f:A=p:other-port.f:C
        - p:port.f:A=p:some-port.f:B
options:
    """


multiPort2 : String
multiPort2 =
    """falcon:
    version: 1
graph:
    processors:
        A:
            class: proc
        B:
            class: proc
    connections:
        - p:port.f:B=p:other-port.f:B
        - p:port.f:B=p:some-port.f:B
        - p:port.f:A=p:some-port.f:B
options:
    """


multiPort3 : String
multiPort3 =
    """falcon:
    version: 1
graph:
    processors:
        A:
            class: proc
        B:
            class: proc
        C:
            class: proc
    connections:
        - p:port1.f:A=s:1.p:some-port.f:C
        - p:port.f:A=s:0.p:some-port.f:C
        - p:port.f:A=p:other-port.f:B
options:
    """


suite : Test
suite =
    describe "Encoding graphs"
        [ describe "Round trip simple graph"
            [ test "round-tripped has 2 nodes" <|
                \_ ->
                    loadGraph 0 0 simpleGraph
                        |> Result.map (Graph.size << .graph)
                        |> Expect.equal
                            (loadGraph 0 0 simpleGraph
                                |> Result.andThen (loadGraph 0 0 << saveGraph)
                                |> Result.map (Graph.size << .graph)
                            )
            ]
        , test "Round trip a graph with 2 connections on the same source port" <|
            \_ ->
                loadGraph 0 0 multiPortConnections
                    |> Result.map saveGraph
                    |> Expect.equal (Ok multiPortConnections)
        , test "Round-trip a graph with 2 source and 2 sink connections between a pair of nodes" <|
            \_ ->
                loadGraph 0 0 multiPort2
                    |> Result.map saveGraph
                    |> Expect.equal (Ok multiPort2)
        , test "Round-trip again" <|
            \_ ->
                loadGraph 0 0 multiPort3
                    |> Result.map saveGraph
                    |> Expect.equal (Ok multiPort3)
        ]

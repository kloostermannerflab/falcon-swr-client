module Notifications exposing (Msg(..), Notification, update, view)

{-| Display notification popups when important client events happen.
-}

import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import List.Extra exposing (removeAt)


{-| The data displayed in a notification popup
-}
type alias Notification =
    String


{-| TEA message
-}
type Msg
    = New Notification
    | Close Int


{-| TEA update function
-}
update : Msg -> List Notification -> List Notification
update msg notifications =
    case ( msg, notifications ) of
        ( New "", _ ) ->
            notifications

        ( New n, n1 :: _ ) ->
            if n == n1 then
                notifications

            else
                n :: notifications

        ( New n, [] ) ->
            [ n ]

        ( Close i, _ ) ->
            removeAt i notifications


{-| TEA view function
-}
view : List Notification -> Element Msg
view notifications =
    column
        [ width (px 250)
        , height shrink
        , spacing 10
        , padding 20
        , alignBottom
        , alignRight
        ]
        (List.indexedMap viewNotification notifications)


{-| Display a single notification popup
-}
viewNotification : Int -> Notification -> Element Msg
viewNotification index notification =
    el
        [ width fill
        , height shrink
        , Background.color (rgba 0.1 0.1 0.1 0.9)
        , Border.rounded 10
        ]
        (column
            [ width fill
            , padding 5
            , Font.color (rgb 1 1 1)
            ]
            [ row
                [ width fill ]
                [ text "⚠"
                , el [ alignRight ]
                    (Input.button []
                        { onPress = Just (Close index)
                        , label = text "×"
                        }
                    )
                ]
            , paragraph [] [ text notification ]
            ]
        )

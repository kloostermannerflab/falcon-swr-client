/**
 * The main entry point for the GUI.
 *
 * @remarks
 * This file sets up and starts the Elm app, opens a WebSocket connection to the bridge,
 * and installs global event handlers.
 */

import { Elm } from './Main.elm';

import * as Client from './client';
import { FALCON_BRIDGE_URL, FALCON_BRIDGE_CONNECTION_TIMEOUT } from './constants';
import { chartdata } from './plot';

const log_message = window.location.search.includes('debug')
    ? (...xs: unknown[]) => {
          console.log(...xs);
      }
    : () => {
          return;
      };

log_message('IN DEBUG MODE');

// Start the Elm GUI application.
const app = Elm.Main.init({
    node: document.querySelector('main'),
    flags: { width: window.innerWidth, height: window.innerHeight },
});

// Handler for error reporting.
const errors: Client.ErrorHandler = (message: string, connected: boolean) => {
    app.ports.receiveMessage.send(Client.errorMessage(message, connected));
    console.error(`[ERROR]: ${message}`);
};

/**
 * Forward messages from Falcon to the GUI.
 *
 * @remarks
 * Most messages go directly to the Elm application, but events to be plotted go
 * to the custom chart.js element. This is why messages from Falcon have to be
 * partially decoded early, before being sent to the Elm app for full decoding.
 *
 * @param message - A partially decoded message from Falcon.
 */
const messages: Client.MessageHandler = (message: Client.FalconMessage): void => {
    switch (message.category) {
        case 'log' /* Falls through */:
        case 'response' /* Falls through */:
        case 'ack' /* Falls through */:
        case 'status' /* Falls through */:
        case 'error' /* Falls through */:
            log_message('Received message: ', message);
            app.ports.receiveMessage.send(message);
            break;

        case 'events':
            if (Client.isFalconData(message?.data)) {
                Client.dispatchFalconEvents(message.data);
            }
            break;
        default:
            console.log(`[ERROR] unknown message type: ${message.category}`);
            break;
    }
};

// WebSocket handling
const connection: Client.ClientConnection = Client.connectToBridge(
    FALCON_BRIDGE_URL,
    FALCON_BRIDGE_CONNECTION_TIMEOUT,
    messages,
    errors,
);

/**
 * Forwarding messages from the Elm app to Falcon or the plot.
 *
 * Structure:
 *   [ * ] for messages to Falcon, first element cannot be the string 'plot'
 *   [ 'plot', command, address ] for messages to a plot. `command` is a plot function
 *                                and `address` is the name of the plot to modify, e.g. "main".
 */
app.ports.sendMessage.subscribe((message: string[]) => {
    if (message[0] !== 'plot') {
        Client.sendMessage(connection, JSON.stringify(message));
    } else {
        switch (message[1]) {
            case 'resize':
                for (const chartname in chartdata) {
                    chartdata[chartname].resize();
                }
                break;
            case 'clear':
                chartdata[message[2]].clear();
                break;
            case 'reset':
                chartdata[message[2]].reset();
                break;
        }
    }
});

module Main exposing (main)

{-| The main GUI entry point.

This is a standard Elm TEA application. There is an `update` and a `view` function
with some helper functions. View related functions are grouped together, and update
related functions are grouped together.

-}

import Browser
import Browser.Events exposing (onKeyUp, onResize)
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Event
import Element.Font as Font
import Element.Input as Input
import FalconControl as FC
import FalconGraph as FG exposing (GraphStatus(..))
import File exposing (File)
import File.Download as Download
import File.Select as Select
import Html
import Html.Attributes as HA
import Html.Events
import Json.Decode as Json
import Loading
import LogView exposing (Msg(..))
import Maybe.Extra as MaybeE
import Notifications
import Result.Extra as ResultE
import Task
import Time


type Actor
    = Falcon
    | Bridge


{-| The types of message that can be received from outside the application
-}
type FalconMessage
    = Log LogView.LogMessage
    | Response ( List String, List String )
    | Ack (List String) -- Emitted by the bridge: successful response from the bridge
    | StatusMessage Actor Bool String
    | BridgeError String -- Emitted by the bridge when it can't handle an error


{-| Connection status for the Bridge and Falcon
-}
type Connection
    = NotConnected String
    | Connecting String
    | Connected { address : String, version : String }


{-| An internal application window
-}
type alias Window =
    { displayed : Bool
    , width : Int
    , height : Int
    }


{-| User interface window elements
-}
type UserInterface
    = GraphArea
    | LogArea


type alias ButtonLabel =
    String


type alias TooltipLabel =
    String


type Side
    = Left
    | Right


type alias Model =
    { notifications : List Notifications.Notification
    , falcon : Connection
    , bridge : Connection
    , addressPlaceholder : String
    , graph : Maybe FG.FalconGraph
    , log : LogView.Log
    , resources : List String
    , viewport : Window
    , graphArea : Window
    , plotArea : Window
    , logArea : Window
    , helpModal : Bool
    }


type Msg
    = YamlRequested
    | YamlSelected File
    | YamlLoaded String
    | Recv Json.Value
    | GraphControl FG.GraphMsg
    | LogControl LogView.Msg
    | NotificationCtl Notifications.Msg
    | ChangeServerInput String
    | Connect
    | Disconnect
    | SelectResource String
    | StartGraph
    | StopGraph
    | DestroyGraph
    | SaveGraph
    | ResetGraphView
    | RestoreDirtyGraph
    | RebuildGraph
    | ToggleDisplay UserInterface
    | ClearPlot String
    | ResetPlot String
    | WindowResize Int Int
    | HideHelpModal
    | ToggleHelpModal
    | Noop


init : { width : Int, height : Int } -> ( Model, Cmd Msg )
init viewport =
    case windowWidth viewport.width ( True, True, True ) of
        ( graphW, plotW, logW ) ->
            ( { notifications = []
              , falcon = NotConnected ""
              , bridge = NotConnected ""
              , addressPlaceholder = "localhost"
              , graph = Nothing
              , log = LogView.init Time.utc 100
              , resources = []
              , viewport = Window True viewport.width viewport.height
              , graphArea = Window True graphW (windowHeight viewport.height)
              , plotArea = Window True plotW (windowHeight viewport.height)
              , logArea = Window True logW (windowHeight viewport.height)
              , helpModal = False
              }
            , FC.bridgeVersion
            )



-- UPDATE and helpers


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        YamlRequested ->
            ( model
            , Select.file [ ".yml", ".yaml" ] YamlSelected
            )

        YamlSelected file ->
            ( model
            , Task.perform YamlLoaded <| File.toString file
            )

        YamlLoaded content ->
            ( model, FC.build content )

        Recv data ->
            nativeReceive model data

        GraphControl graphMsg ->
            case Maybe.map (FG.update graphMsg) model.graph of
                Just ( updatedGraph, cmd ) ->
                    ( { model | graph = Just updatedGraph }, cmd )

                _ ->
                    ( model, Cmd.none )

        LogControl logMsg ->
            ( { model | log = LogView.update logMsg model.log }, Cmd.none )

        NotificationCtl nMsg ->
            ( { model | notifications = Notifications.update nMsg model.notifications }, Cmd.none )

        ChangeServerInput addrs ->
            ( { model | falcon = NotConnected addrs }, Cmd.none )

        Connect ->
            case model.falcon of
                NotConnected "" ->
                    ( { model | falcon = Connecting model.addressPlaceholder }
                    , FC.connect model.addressPlaceholder
                    )

                _ ->
                    ( { model | falcon = connecting model.falcon }
                    , FC.connect (connectionAddress model.falcon)
                    )

        Disconnect ->
            ( { model | falcon = disconnect model.falcon, graph = Nothing, resources = [] }
            , FC.disconnect (connectionAddress model.falcon)
            )

        SelectResource res ->
            ( model, FC.getGraph res )

        StartGraph ->
            ( model, FC.start )

        StopGraph ->
            ( model, FC.stop )

        DestroyGraph ->
            ( model, FC.destroy )

        SaveGraph ->
            let
                data : String
                data =
                    Maybe.map FG.saveGraph model.graph
                        |> Maybe.withDefault ""
            in
            ( model, Download.string "graph.yaml" "application/yaml" data )

        ResetGraphView ->
            ( { model | graph = Maybe.map FG.reset model.graph }, Cmd.none )

        RestoreDirtyGraph ->
            ( { model | graph = Maybe.map FG.useDirty model.graph }, Cmd.none )

        RebuildGraph ->
            ( model, FC.destroy )

        ToggleDisplay window ->
            let
                ( graphWindow, plotWindow, logWindow ) =
                    updateWindowDisplay model window
            in
            ( { model
                | graphArea = graphWindow
                , plotArea = plotWindow
                , logArea = logWindow
                , graph = Maybe.map (FG.resize (toFloat graphWindow.width) (toFloat graphWindow.height)) model.graph
              }
            , FC.resizePlots
            )

        ClearPlot plot ->
            ( model, FC.clearPlot plot )

        ResetPlot plot ->
            ( model, FC.resetPlot plot )

        WindowResize width height ->
            let
                displayed : ( Bool, Bool, Bool )
                displayed =
                    ( model.graphArea.displayed, model.plotArea.displayed, model.logArea.displayed )

                wHeight : Int
                wHeight =
                    windowHeight height
            in
            case windowWidth width displayed of
                ( graphW, plotW, logW ) ->
                    ( { model
                        | viewport = Window True width height
                        , graphArea = { displayed = model.graphArea.displayed, width = graphW, height = wHeight }
                        , plotArea = { displayed = model.plotArea.displayed, width = plotW, height = wHeight }
                        , logArea = { displayed = model.logArea.displayed, width = logW, height = wHeight }
                        , graph = Maybe.map (FG.resize (toFloat plotW) (toFloat wHeight - 30)) model.graph
                      }
                    , FC.resizePlots
                    )

        HideHelpModal ->
            ( { model | helpModal = False }, Cmd.none )

        ToggleHelpModal ->
            ( { model | helpModal = not model.helpModal }, Cmd.none )

        Noop ->
            ( model, Cmd.none )


updateWindowDisplay : Model -> UserInterface -> ( Window, Window, Window )
updateWindowDisplay model window =
    let
        zipWindows : ( Bool, Bool, Bool ) -> ( Int, Int, Int ) -> ( Int, Int, Int ) -> ( Window, Window, Window )
        zipWindows ( a1, a2, a3 ) ( b1, b2, b3 ) ( c1, c2, c3 ) =
            ( Window a1 b1 c1, Window a2 b2 c2, Window a3 b3 c3 )

        displayed =
            case window of
                GraphArea ->
                    ( not model.graphArea.displayed, True, model.logArea.displayed )

                LogArea ->
                    ( model.graphArea.displayed, True, not model.logArea.displayed )

        wh =
            windowHeight model.viewport.height
    in
    zipWindows displayed (windowWidth model.viewport.width displayed) ( wh, wh, wh )


guiLoadGraph : Model -> String -> Model
guiLoadGraph model graphText =
    let
        changeGraphInModel : Result String FG.FalconGraph -> Model
        changeGraphInModel res =
            case res of
                Ok graph ->
                    { model | graph = Just graph }

                Err err ->
                    { model
                        | notifications = Notifications.update (Notifications.New err) model.notifications
                    }

        loaded : Result String FG.FalconGraph
        loaded =
            FG.loadGraph (toFloat model.graphArea.width) (toFloat model.graphArea.height) graphText
    in
    changeGraphInModel
        (case model.graph of
            Nothing ->
                loaded

            Just oldGraph ->
                loaded
                    |> Result.andThen (FG.unionGraphs oldGraph)
                    |> Result.map FG.dropDirty
                    |> ResultE.orElse loaded
        )


disconnect : Connection -> Connection
disconnect c =
    case c of
        Connected { address } ->
            NotConnected address

        Connecting address ->
            NotConnected address

        _ ->
            c


connecting : Connection -> Connection
connecting c =
    case c of
        NotConnected addr ->
            Connecting addr

        _ ->
            c


connect : Connection -> Connection
connect c =
    case c of
        NotConnected address ->
            Connected { address = address, version = "" }

        Connecting address ->
            Connected { address = address, version = "" }

        _ ->
            c


connectionAddress : Connection -> String
connectionAddress c =
    case c of
        NotConnected addr ->
            addr

        Connecting addr ->
            addr

        Connected { address } ->
            address


connectionVersion : Connection -> String
connectionVersion c =
    case c of
        NotConnected _ ->
            "Not connected"

        Connecting addr ->
            "Connecting to " ++ addr ++ "..."

        Connected { version } ->
            version


nativeReceive : Model -> Json.Value -> ( Model, Cmd Msg )
nativeReceive model data =
    case Json.decodeValue decodeMessage data of
        Ok (Log log) ->
            let
                newStatus =
                    if log.level == LogView.STATE then
                        FG.parseGraphState log.message
                            |> Result.toMaybe

                    else
                        Maybe.map .status model.graph
            in
            ( { model
                | log = LogView.update (NewLogMessage log) model.log
                , graph = MaybeE.andMap model.graph (Maybe.map FG.graphStatus newStatus)
              }
            , Cmd.none
            )

        Ok (Response response) ->
            fromResponse model response

        Ok (Ack [ "connected", _ ]) ->
            ( model
            , FC.status
            )

        Ok (Ack [ "disconnected", _ ]) ->
            ( { model
                | falcon = disconnect model.falcon
              }
            , Cmd.none
            )

        Ok (Ack [ "version", version, address ]) ->
            ( { model
                | bridge = Connected { address = address, version = version }
                , addressPlaceholder = address
              }
            , Cmd.none
            )

        Ok (Ack unknown) ->
            ( { model
                | notifications =
                    Notifications.update (Notifications.New <| "Unknown Ack: [" ++ String.join ", " unknown ++ "]")
                        model.notifications
              }
            , Cmd.none
            )

        Ok (StatusMessage actor isConnected message) ->
            let
                newModel =
                    case ( actor, isConnected ) of
                        ( Falcon, False ) ->
                            { model | falcon = disconnect model.falcon }

                        ( Bridge, False ) ->
                            { model | bridge = disconnect model.bridge }

                        ( Falcon, True ) ->
                            { model | falcon = connect model.falcon }

                        ( Bridge, True ) ->
                            { model | bridge = connect model.bridge }
            in
            ( { newModel
                | notifications = Notifications.update (Notifications.New message) model.notifications
              }
            , case ( model.bridge, newModel.bridge ) of
                ( NotConnected _, Connected _ ) ->
                    FC.bridgeVersion

                _ ->
                    Cmd.none
            )

        Ok (BridgeError error) ->
            ( { model
                | notifications = Notifications.update (Notifications.New error) model.notifications
              }
            , FC.disconnect (connectionAddress model.falcon)
            )

        Err err ->
            ( { model
                | notifications = Notifications.update (Notifications.New <| Json.errorToString err) model.notifications
              }
            , Cmd.none
            )


fromResponse : Model -> ( List String, List String ) -> ( Model, Cmd Msg )
fromResponse model response =
    case response of
        ( [ "info" ], [ info ] ) ->
            case FG.status info of
                Ok falconInfo ->
                    let
                        newModel =
                            { model
                                | falcon =
                                    Connected
                                        { address = connectionAddress model.falcon
                                        , version = falconInfo.version
                                        }
                                , graph = Maybe.map (FG.graphStatus falconInfo.status) model.graph
                            }
                    in
                    if falconInfo.status == NOGRAPH then
                        ( newModel, FC.resources )

                    else
                        ( newModel
                        , if model.graph == Nothing then
                            FC.graphDefinition

                          else
                            Cmd.none
                        )

                Err msg ->
                    ( { model | notifications = Notifications.update (Notifications.New msg) model.notifications }
                    , Cmd.none
                    )

        ( [ "graph", "state" ], [ status ] ) ->
            ( { model | notifications = Notifications.update (Notifications.New status) model.notifications }
            , Cmd.none
            )

        ( [ "graph", "build", _ ], [ status ] ) ->
            case status of
                "OK" ->
                    ( model, FC.graphDefinition )

                _ ->
                    ( { model | notifications = Notifications.update (Notifications.New status) model.notifications }
                    , Cmd.none
                    )

        ( [ "graph", "destroy" ], [ status ] ) ->
            case status of
                "OK" ->
                    case model.graph of
                        Nothing ->
                            ( model, FC.resources )

                        Just g ->
                            if g.dirty == Nothing then
                                -- User clicked on the "destroy graph" button
                                ( { model | graph = Nothing }, FC.resources )

                            else
                                ( model, FC.build (FG.saveGraph g) )

                msg ->
                    ( { model | notifications = Notifications.update (Notifications.New msg) model.notifications }
                    , Cmd.none
                    )

        ( [ "graph", "yaml" ], [ yaml ] ) ->
            ( guiLoadGraph model yaml, FC.status )

        ( [ "resources", "list", "graphs" ], resources ) ->
            ( { model | resources = resources }, Cmd.none )

        ( [ "resources", "graphs", _ ], [ graph ] ) ->
            ( model, FC.build graph )

        _ ->
            ( model, Cmd.none )


windowWidth : Int -> ( Bool, Bool, Bool ) -> ( Int, Int, Int )
windowWidth viewportWidth windows =
    let
        displayed : Int
        displayed =
            case windows of
                ( g, p, l ) ->
                    List.filter identity [ g, p, l ]
                        |> List.length

        width : Bool -> Int
        width window =
            if window then
                (viewportWidth - (60 + (3 - displayed) * 20)) // displayed

            else
                15
    in
    case windows of
        ( g, p, l ) ->
            ( width g, width p, width l )


windowHeight : Int -> Int
windowHeight viewportHeight =
    viewportHeight - 120



-- VIEW and helpers


view : Model -> Browser.Document Msg
view model =
    let
        attrs =
            if model.helpModal then
                [ width fill
                , height fill
                , inFront (map NotificationCtl <| Notifications.view model.notifications)
                , inFront helpModal
                ]

            else
                [ width fill
                , height fill
                , inFront (map NotificationCtl <| Notifications.view model.notifications)
                ]
    in
    { title = "Falcon Client"
    , body =
        [ Element.layoutWith
            { options =
                [ focusStyle
                    { borderColor = Nothing
                    , backgroundColor = Nothing
                    , shadow = Nothing
                    }
                ]
            }
            attrs
            (column
                [ width fill
                , height fill
                ]
                [ topBar model
                , mainApp model
                ]
            )
        ]
    }


topBar : Model -> Element Msg
topBar model =
    row
        [ width fill
        , alignTop
        , Background.color (rgb255 55 55 55)
        , padding 20
        , spacing 30
        ]
        [ el
            [ alignLeft
            , Font.color (rgb255 255 255 255)
            ]
            (text "Falcon: Ripple Detection")
        , el
            [ alignRight ]
            (connectionStatus model)
        ]


connectionStatusInput : Connection -> Element Msg
connectionStatusInput connection =
    case connection of
        NotConnected _ ->
            row
                []
                []

        Connecting a ->
            el
                [ Font.color (rgb255 255 255 255) ]
                (text ("Connecting to... (" ++ a ++ ")"))

        Connected { address } ->
            row
                [ Font.color (rgb255 255 255 255)
                , spacing 20
                ]
                [ text ("Connected to " ++ address)
                , Input.button
                    [ Font.color (rgb255 255 0 0) ]
                    { onPress = Just Disconnect
                    , label = text "×"
                    }
                ]


connectionStatus : Model -> Element Msg
connectionStatus model =
    let
        green =
            rgb255 50 250 50

        red =
            rgb255 250 50 50

        connectionColour : Connection -> Attr decorative msg
        connectionColour connection =
            case connection of
                Connected _ ->
                    Font.color green

                _ ->
                    Font.color red
    in
    row
        [ spacing 20 ]
        [ connectionStatusInput model.falcon
        , column
            []
            [ el
                [ connectionColour model.bridge
                , htmlAttribute <| HA.title (connectionVersion model.bridge)
                ]
                (text "Bridge")
            , el
                [ connectionColour model.falcon
                , htmlAttribute <| HA.title (connectionVersion model.falcon)
                ]
                (text "Falcon")
            ]
        ]


addressInput : String -> String -> Element Msg
addressInput placeholder addr =
    let
        label : Input.Label Msg
        label =
            Input.labelLeft
                [ Font.color (rgb255 0 0 0) ]
                (text "Falcon Connection:")
    in
    row
        [ centerX
        , centerY
        , spacing 20
        ]
        [ Input.text
            []
            { label = label
            , onChange = ChangeServerInput
            , placeholder = Just (Input.placeholder [] (text placeholder))
            , text = addr
            }
        , Input.button
            [ Input.focusedOnLoad
            , htmlAttribute <| HA.title "Connect to Falcon"
            ]
            { onPress = Just Connect
            , label = text "▶"
            }
        ]


mainApp : Model -> Element Msg
mainApp model =
    case model.falcon of
        NotConnected addr ->
            el
                [ Background.color (rgb255 200 200 200)
                , width fill
                , height fill
                ]
                (addressInput model.addressPlaceholder addr)

        Connecting _ ->
            el
                [ Background.color (rgb255 200 200 200)
                , width fill
                , height fill
                ]
                (el
                    [ centerX, centerY ]
                    (html <|
                        Loading.render Loading.Sonar Loading.defaultConfig Loading.On
                    )
                )

        _ ->
            row
                [ width (maximum model.viewport.width fill)
                , height (maximum (model.viewport.height - 80) fill)
                , alignTop
                , Background.color (rgb255 200 200 200)
                , padding 10
                , spacing 20
                ]
                [ displayResource model
                , mainPlot model
                , logArea model
                ]


mainPlot : Model -> Element Msg
mainPlot model =
    column
        [ width (px model.plotArea.width)
        , height (px model.plotArea.height)
        , Background.color (rgb255 255 255 255)
        ]
        [ row
            [ width fill
            , Background.color (rgb255 0 0 0)
            , Font.color (rgb255 255 255 255)
            , spacing 30
            , padding 5
            ]
            [ 
            text "Hippocampus ripple detector"
            , customButton "❓" "Help with plot interaction" (Just ToggleHelpModal)
            , customButton "✘" "Clear the plot" (Just <| ClearPlot "HIPPOCAMPUS_detector")   
            , customButton "⏭" "Follow live data" (Just <| ResetPlot "HIPPOCAMPUS_detector")
            ]
        , html <|
            Html.node "event-plot"
                [ HA.attribute "name" "HIPPOCAMPUS_detector"
                , HA.attribute "legends" "statistics, threshold"
                , HA.attribute "plot_event" "true"
                , HA.style "width" "100%"
                , HA.style "height" "50%"
                ]
                []
        , row
            [ width fill
            , Background.color (rgb255 0 0 0)
            , Font.color (rgb255 255 255 255)
            , spacing 30
            , padding 5
            ]
            [ 
            text "Cortex ripple detector"
            , customButton "❓" "Help with plot interaction" (Just ToggleHelpModal)
            , customButton "✘" "Clear the plot" (Just <| ClearPlot "CORTEX_detector")   
            , customButton "⏭" "Follow live data" (Just <| ResetPlot "CORTEX_detector")
            ]
        , html <|
            Html.node "event-plot"
                [ HA.attribute "name" "CORTEX_detector"
                , HA.attribute "legends" "statistics, threshold"
                , HA.attribute "plot_event" "false"
                , HA.style "width" "100%"
                , HA.style "height" "50%"
                ]
                []
        ]


logArea : Model -> Element Msg
logArea model =
    row
        [ width (px model.logArea.width)
        , height (px model.logArea.height)
        , Background.color (rgb255 255 255 255)
        ]
        (if model.logArea.displayed then
            [ viewToggle Right True LogArea
            , el
                [ alignTop
                , height fill
                , width fill
                , scrollbars
                ]
                (LogView.view model.log
                    |> map LogControl
                )
            ]

         else
            [ viewToggle Right False LogArea ]
        )


displayResource : Model -> Element Msg
displayResource model =
    let
        clickableResource : String -> Element Msg
        clickableResource res =
            Input.button
                [ Font.color (rgb255 0 0 255)
                , mouseOver [ Font.color (rgb255 90 90 100) ]
                ]
                { onPress = Just (SelectResource res)
                , label = text res
                }
    in
    row
        [ width (px model.graphArea.width)
        , height (px model.graphArea.height)
        , Background.color (rgb255 255 255 255)
        ]
        (if model.graphArea.displayed then
            [ el
                [ width (px (model.graphArea.width - 15))
                , height fill
                , if model.graph == Nothing then
                    padding 20

                  else
                    padding 0
                ]
                (case model.graph of
                    Nothing ->
                        column
                            [ height fill
                            , width fill
                            , scrollbars
                            , spacing 5
                            , Font.color (rgb255 0 0 255)
                            ]
                            ([ fileSelect ] ++ List.map clickableResource model.resources)

                    Just graph ->
                        column
                            [ height fill
                            , width fill
                            ]
                            [ graphControlToolbar graph.status (graph.dirty /= Nothing)
                            , html
                                (Html.map GraphControl <|
                                    FG.drawGraph model.graphArea.height graph
                                )
                            ]
                )
            , viewToggle Left True GraphArea
            ]

         else
            [ viewToggle Left False GraphArea ]
        )


graphControlToolbar : FG.GraphStatus -> Bool -> Element Msg
graphControlToolbar status dirty =
    let
        statusDependent : List (Element Msg)
        statusDependent =
            case status of
                FG.READY ->
                    if not dirty then
                        [ customButton "▶" "Start processing" (Just StartGraph)
                        , customButton "✘" "Destroy graph" (Just DestroyGraph)
                        ]

                    else
                        [ customButton "✓" "Accept changes" (Just RebuildGraph)
                        , customButton "✘" "Reject changes" (Just RestoreDirtyGraph)
                        ]

                FG.PROCESSING ->
                    [ customButton "⏹" "Stop processing" (Just StopGraph) ]

                _ ->
                    []
    in
    row
        [ width fill
        , case ( dirty, status ) of
            ( True, _ ) ->
                Background.color (rgb255 255 0 0)

            ( _, FG.PROCESSING ) ->
                Background.color (rgb255 50 180 50)

            _ ->
                Background.color (rgb255 0 0 0)
        , padding 5
        , spacing 30
        , Font.color (rgb255 255 255 255)
        ]
        (statusDependent
            ++ [ customButton "↺" "Reset view" (Just ResetGraphView)
               , customButton "💾" "Save graph to file" (Just SaveGraph)
               ]
        )


helpModal : Element Msg
helpModal =
    el
        [ centerX
        , centerY
        , width (minimum 300 shrink)
        , height (minimum 100 shrink)
        , Background.color (rgba 0 0 0 0.4)
        , Border.rounded 25
        , padding 20
        , Font.color (rgb 0.8 0.8 0.8)
        , Font.size 16
        , Font.bold
        ]
        (column
            [ width fill
            , height fill
            , Background.color (rgba 0.2 0.2 0.2 0.98)
            , Border.rounded 10
            , padding 15
            , spacing 15
            ]
            [ text "• PAN: hold left-mouse-button and drag"
            , text "• TRANSLATE X: scroll wheel"
            , text "• TRANSLATE Y: hold \"Alt\" key + scroll wheel"
            , text "• ZOOM X: hold \"Ctrl\" key + scroll wheel"
            , text "• ZOOM Y: hold \"Ctrl\" + \"Alt\" keys + scroll wheel"
            ]
        )



-- GENERIC UI ELEMENTS


inputButton : String -> Msg -> Element Msg
inputButton buttonLabel onPress =
    html <|
        Html.button
            [ Html.Events.onClick onPress ]
            [ Html.text buttonLabel ]


fileSelect : Element Msg
fileSelect =
    el [ centerX ] <|
        inputButton "Load graph from file" YamlRequested


customButton : ButtonLabel -> TooltipLabel -> Maybe Msg -> Element Msg
customButton label tooltip msg =
    Input.button
        [ centerX
        , centerY
        , paddingXY 3 0
        , width <| minimum 40 shrink
        , height (px 25)
        , Border.width 1
        , Border.color (rgb 1 1 1)
        , Border.rounded 8
        , Background.color (rgb 0.2 0.2 0.2)
        , Font.size 16
        , Font.center
        , htmlAttribute <| HA.title tooltip
        , mouseOver [ Border.innerGlow (rgb 0.6 0.6 0.6) 1 ]
        , mouseDown [ Border.color (rgb 0 0 0) ]
        ]
        { label = el [ centerX, centerY ] (text label), onPress = msg }


viewToggle : Side -> Bool -> UserInterface -> Element Msg
viewToggle side displayed ui =
    column
        [ width (px 15)
        , height fill
        , Background.color (rgb255 0 0 0)
        , Font.color (rgb255 255 255 255)
        , Font.size 12
        , mouseOver [ Background.color (rgb255 30 30 30) ]
        , Event.onClick (ToggleDisplay ui)
        ]
        [ el
            [ centerX
            , centerY
            , htmlAttribute (HA.style "pointer-events" "none")
            ]
            (case ( displayed, side ) of
                ( True, Left ) ->
                    text "◀"

                ( False, Left ) ->
                    text "▶"

                ( True, Right ) ->
                    text "▶"

                ( False, Right ) ->
                    text "◀"
            )
        ]



-- MESSAGE DECODING


decodeMessage : Json.Decoder FalconMessage
decodeMessage =
    let
        pickDecoder : String -> Json.Decoder FalconMessage
        pickDecoder msgType =
            case msgType of
                "log" ->
                    Json.field "data" (Json.map Log LogView.decode)

                "response" ->
                    Json.field "data" decodeResponseMessage

                "ack" ->
                    Json.map Ack (Json.field "data" (Json.list Json.string))

                "status" ->
                    decodeStatusMessage

                "error" ->
                    Json.map BridgeError (Json.field "data" Json.string)

                _ ->
                    Json.fail ("Not a valid message type: " ++ msgType)
    in
    Json.field "category" Json.string
        |> Json.andThen pickDecoder


decodeResponseMessage : Json.Decoder FalconMessage
decodeResponseMessage =
    let
        listToTuple : List (List String) -> Json.Decoder ( List String, List String )
        listToTuple l =
            case l of
                x :: y :: [] ->
                    Json.succeed ( x, y )

                _ ->
                    Json.fail "Response should contain only 2 elements"
    in
    Json.map Response
        (Json.list
            (Json.list Json.string)
            |> Json.andThen listToTuple
        )


decodeStatusMessage : Json.Decoder FalconMessage
decodeStatusMessage =
    let
        decodeActor : Json.Decoder Actor
        decodeActor =
            Json.string
                |> Json.andThen
                    (\actor ->
                        if actor == "Bridge" then
                            Json.succeed Bridge

                        else if actor == "Falcon" then
                            Json.succeed Falcon

                        else
                            Json.fail "Not a valid actor"
                    )
    in
    Json.map3 StatusMessage
        (Json.field "actor" decodeActor)
        (Json.field "connected" Json.bool)
        (Json.field "data" Json.string)


keyDecoder : Json.Decoder Msg
keyDecoder =
    Json.map
        (\key ->
            case key of
                "Escape" ->
                    HideHelpModal

                "?" ->
                    ToggleHelpModal

                _ ->
                    Noop
        )
        (Json.field "key" Json.string)



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ FC.receiveMessage Recv
        , onResize WindowResize
        , onKeyUp keyDecoder
        , Maybe.map FG.subscriptions model.graph
            |> Maybe.withDefault Sub.none
            |> Sub.map GraphControl
        ]



-- MAIN PROGRAM


main : Program { width : Int, height : Int } Model Msg
main =
    Browser.document
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }

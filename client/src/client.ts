/**
 * Handling of the WebSocket connection to the bridge and (partial) message decoding.
 *
 * @remarks
 * This module handles the details of opening a connection, retrying failed connections,
 * sending messages, retrying when messages fail to send, and (partially) decoding
 * received messages. These types are derived from those defined in the falcon-bridge and
 * are minimum required definitions (e.g. `FalconEvent`s is not passed to Elm so must
 * be fully decoded here, but `FalconLog` messages only require the `category` field
 * so that they can be passed to Elm).
 */

import { FALCON_BRIDGE_HEARTBEAT, FALCON_BRIDGE_HEARTBEAT_TIMEOUT } from './constants';
import { chartdata } from './plot';

export type FalconSignal = {
    category: string;
    signal: {
         timestamp: number;
         signal: Array<number>;
   }

};

export type FalconEvent = {
    category: string;
    signal: {
        timestamp: number;
        name: string;
    }
};

export type FalconData = {
    target: string;
    processor: string;
    events: Array<FalconSignal | FalconEvent>;
};

export const isFalconSignal = (data: unknown): data is FalconSignal => {
    return (
        (data as FalconSignal).category !== undefined &&
        (data as FalconSignal).signal !== undefined &&
         (data as FalconSignal).signal.timestamp !== undefined &&
        (data as FalconSignal).category === 'series' &&
        typeof (data as FalconSignal).signal.timestamp === 'number'
    );
};

export const isFalconEvent = (data: unknown): data is FalconEvent => {
    return (
        (data as FalconEvent).category !== undefined &&
        (data as FalconEvent).signal.timestamp !== undefined &&
        (data as FalconEvent).signal.name !== undefined &&
        (data as FalconEvent).category === 'events' &&
        typeof (data as FalconEvent).signal.timestamp === 'number' &&
        typeof (data as FalconEvent).signal.name === 'string'
    );
};

export const isFalconData = (data: unknown): data is FalconData => {
    return (
        (data as FalconData).target !== undefined &&
        (data as FalconData).events !== undefined &&
        typeof (data as FalconData).target === 'string'
    );
};

export type FalconMessageType = 'log' | 'events' | 'response' | 'pong' | 'ack' | 'status' | 'error';

export type FalconMessage = {
    category: FalconMessageType;
    data?: unknown;
};

/**
 * Used for communicating status messages to Elm. These status messages could be
 * the bridge being disconnected, a message failing to decode, or an error in the
 * WebSocket connection.
 */
export type FalconMessageStatus = FalconMessage & {
    actor: 'Bridge' | 'Falcon';
    connected: boolean;
    data: string;
};

/** Constructor for FalconStatusMessage objects. */
export const statusMessage = (actor: 'Bridge' | 'Falcon', connected: boolean, data: string): FalconMessageStatus => {
    return {
        category: 'status',
        actor,
        connected,
        data,
    };
};

/** Special purpose constructor for FalconStatusMessage objects. */
export const errorMessage = (message: string, connected: boolean): FalconMessageStatus => {
    return statusMessage('Bridge', connected, message);
};

export type MessageHandler = (message: FalconMessage) => void;

export type ErrorHandler = (error: string, connected: boolean) => void;

/**
 * Object containing all state required for handling the WebSocket connection
 * and handling events such as messages from the socket or errors.
 *
 * Every `timeout` seconds, a heartbeat message is sent to the bridge. If the
 * bridge fails to respond it is deemed disconnected and we drop the old
 * connection and try to reconnect.
 */
export type ClientConnection = {
    url: string;
    socket: WebSocket;
    timeout: number;
    pingTimeout?: number;
    heartbeatTimer?: number;
    onmessage: MessageHandler;
    onerror: ErrorHandler;
};

/**
 * Wait for a heartbeat response from the bridge. If none
 * are received, close the connection.
 */
const heartbeat = (connection: ClientConnection): void => {
    clearTimeout(connection.pingTimeout);

    connection.pingTimeout = setTimeout(() => {
        console.error('[HEARTBEAT] waited too long: closing the socket');
        errorMessage('Waited too long for heartbeat.', false);
        connection.socket.close();
    }, FALCON_BRIDGE_HEARTBEAT_TIMEOUT);
};

/**
 * Send a heartbeat message to the bridge.
 */
const ping = (connection: ClientConnection) => {
    if (connection.socket.readyState === connection.socket.OPEN) {
        sendMessage(connection, JSON.stringify({ category: 'ping' }));
        heartbeat(connection);
    }
};

/**
 * Set up the event handlers required to manage the WebSocket.
 * This function is called recursively to reconnect a disconnected
 * socket.
 */
const setupSocket = (connection: ClientConnection): ClientConnection => {
    const reconnect = () => {
        console.info('Trying to re-establish connection...');
        connection.socket = new WebSocket(connection.url);
        setupSocket(connection);
    };

    /** Initiate sending heartbeat messages to the bridge when the connection is opened. */
    const handleOpen = () => {
        console.info('Connected to the bridge');
        connection.heartbeatTimer = setInterval(ping, FALCON_BRIDGE_HEARTBEAT, connection);
        ping(connection);
    };

    /** When a message is received, decode it and call the appropriate handler. */
    const handleMessage = (event: WebSocketEventMap['message']) => {
        const data = JSON.parse(event.data);
        const message: FalconMessage | string = messageDecoder(data);

        if (typeof message === 'string') {
            connection.onerror(message, true);
        } else if (message.category === 'pong') {
            clearTimeout(connection.pingTimeout);
            connection.onmessage(statusMessage('Bridge', true, ''));
        } else {
            connection.onmessage(message);
        }
    };

    /** When a socket error occurs, close the connection and tell Elm. */
    const handleError = () => {
        connection.socket.close();
        connection.onerror('disconnected', false);
    };

    /** When the connection is closed (e.g. when a heartbeat fails), try to reconnect. */
    const handleClose = () => {
        clearInterval(connection.heartbeatTimer);
        clearTimeout(connection.pingTimeout);

        console.info('[WS CLOSED]');
        setTimeout(() => {
            reconnect();
        }, connection.timeout);
    };

    connection.socket.addEventListener('open', handleOpen);
    connection.socket.addEventListener('close', handleClose);
    connection.socket.addEventListener('error', handleError);
    connection.socket.addEventListener('message', handleMessage);

    return connection;
};

/**
 * Public interface for starting a connection with the bridge.
 */
export const connectToBridge = (
    url: string,
    timeout: number,
    onmessage: MessageHandler,
    onerror: ErrorHandler,
): ClientConnection => {
    const connection: ClientConnection = {
        url,
        socket: new WebSocket(url),
        timeout,
        pingTimeout: undefined,
        heartbeatTimer: undefined,
        onmessage,
        onerror,
    };

    return setupSocket(connection);
};

const isFalconMessage = (data: unknown): data is { category: FalconMessageType; data?: unknown } => {
    return (data as { category: FalconMessageType; data?: unknown }).category !== undefined;
};

const messageDecoder = (data: unknown): FalconMessage | string => {
    if (isFalconMessage(data)) {
        if (['log', 'events', 'response', 'ack', 'pong', 'error'].includes(data.category)) {
            return data;
        } else {
            return `Cannot decode message: no category ${data.category}`;
        }
    }

    return 'Cannot decode message: key category does not exist';
};

/**
 * A buffer for temporarily storing messages that could not be sent.
 */
const messageBuffer: Array<string> = [];

/**
 * Send a message to the bridge. If the websocket is not ready to send messages,
 * buffer them until it is ready.
 */
export const sendMessage = (client: ClientConnection, message: string): void => {
    if (client.socket.readyState === WebSocket.OPEN) {
        while (messageBuffer.length) {
            const oldMessage: string | undefined = messageBuffer.shift();
            if (oldMessage !== undefined) {
                console.log('Sending buffered message to bridge: ', oldMessage);
                client.socket.send(oldMessage);
            }
        }

        console.log(`Sending message to bridge: ${message}`);
        client.socket.send(message);
    } else {
        messageBuffer.push(message);
        console.warn(`Socket is not ready ${client.socket.readyState}. Buffering this message: ${message}`);
    }
};

/**
 * Take a message packet and looks if it is an event or a series packet and sent it on the right plot.
 *
 */
export const dispatchFalconEvents = (message: FalconData): void => {

    switch (message.events[0].category) {
        case 'series':
            if (message.target in chartdata) {
                if (message.events.every(isFalconSignal)) {
                      console.log(message.events)
                    chartdata[message.target].pushData(message.events);
                }
            } else {
                console.log(`[ERROR] unknown message target: ${message.target}`);
            }
            break;
        case 'events':
            if (message.events.every(isFalconEvent)) {
                for (const target in chartdata) {
                    chartdata[target].pushEventData(message.events);
                }
            }
            break;
    }
};

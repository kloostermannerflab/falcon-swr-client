module FalconGraph exposing
    ( FalconGraph, GraphMsg(..), GraphStatus(..)
    , loadGraph, saveGraph, showEdgeLabel
    , update, status, subscriptions
    , drawGraph, reset, resize
    , graphStatus, parseGraphState, dropDirty, useDirty, unionGraphs
    , EdgeLabel
    )

{-| This module deals with building, displaying, and modifying a Falcon graph from its YAML definition and back.

  - Decoding a YAML graph description into a `FalconGraph` data structure.
  - Writing a `FalconGraph` to a YAML string.
  - Changing values in a graph (such as states and options).
  - Displaying the graph as an SVG with interactivity.

@docs FalconGraph, GraphMsg, GraphStatus


# Decoding and encoding graphs

@docs loadGraph, saveGraph, showEdgeLabel


# Changing the graph

@docs update, status, subscriptions


# Displaying the graph

@docs drawGraph, reset, resize


# Dealing with Falcon state machine

@docs graphStatus, parseGraphState, dropDirty, useDirty, unionGraphs

-}

import Basics.Extra exposing (flip, uncurry)
import Browser.Events
import Color exposing (Color)
import Dict exposing (Dict)
import FalconControl as FC
import FalconProcessor as FP
import Force
import Graph exposing (Edge, Graph, Node, NodeContext, NodeId)
import Html exposing (Html)
import Html.Attributes
import Html.Events exposing (on, onCheck, targetValue)
import Html.Events.Extra.Mouse as Mouse
import Html.Events.Extra.Wheel as Wheel
import IntDict
import Json.Decode as Json
import Json.Decode.Extra as JsonE
import List.Extra as ListE
import Math.Vector2 as Crd exposing (Vec2, vec2)
import Monocle.Lens as Lens exposing (Lens)
import Path.LowLevel as SvgPath
import TypedSvg
    exposing
        ( defs
        , g
        , line
        , marker
        , path
        , pattern
        , polyline
        , rect
        , svg
        , text_
        )
import TypedSvg.Attributes
    exposing
        ( class
        , d
        , fontSize
        , id
        , markerEnd
        , orient
        , refX
        , refY
        , stroke
        )
import TypedSvg.Attributes.InPx
    exposing
        ( markerHeight
        , markerWidth
        , strokeWidth
        , x1
        , x2
        , y1
        , y2
        )
import TypedSvg.Core exposing (Svg, foreignObject)
import TypedSvg.Events as E
import TypedSvg.Types
    exposing
        ( CoordinateSystem(..)
        , FontWeight(..)
        , Length(..)
        , Paint(..)
        )
import Yaml.Decode as Yaml exposing (Error(..))
import Yaml.Encode as Encode


type NodeUi
    = Options
    | States


type NodeMark
    = Source
    | Sink
    | Inner


type alias ProcessorContext =
    { processor : FP.FalconProcessor
    , width : Float
    , height : Float
    , ui : Maybe NodeUi
    , mark : NodeMark
    }


{-| Used by the graph library to represent a node in the graph
-}
type alias Entity =
    Force.Entity
        Int
        { value : ProcessorContext
        }


{-| The label for an edge. Only a single edge can be connected between nodes with this graph
library so a list of connector pairs is used to label an edge. Falcon allows multiple edges
to leave a single (node, port) pair but only a single edge into a destination (node, port)
pair. This is constraint is NOT modeled by this type.
-}
type alias EdgeLabel =
    List
        { from : FP.FalconConnector
        , to : FP.FalconConnector
        }


{-| The graph type provided by the library with concrete type parameters for nodes (`Entity`)
and edges, pair of `FalconConnector`.
-}
type alias GraphImpl =
    Graph Entity EdgeLabel


type alias FalconNodeContext =
    NodeContext Entity EdgeLabel


{-| What a mouse cursor could be touching: either a node or the canvas.
-}
type ViewElement
    = ViewNode Int Vec2
    | Canvas Vec2


{-| The public graph type exposed by this module.
-}
type alias FalconGraph =
    { graph : GraphImpl -- The graph itself
    , simulation : Force.State NodeId -- After the graph is built, layout is a simulation.
    , selected : Maybe ViewElement -- Which (if any) node is currently selected
    , viewPort : Vec2 -- The size of the viewport to display the graph in
    , viewPortOffset : Vec2 -- x and y panning of the view on the graph
    , scale : Float -- graph view zoom level
    , status : GraphStatus -- current state of the falcon state machine
    , dirty : Maybe GraphImpl -- Inhabited with the "old" graph after modifying an option
    }


{-| Simulated link between nodes in the graph.

    This is used for initial graph layout.

-}
type alias Link =
    { source : Int
    , target : Int
    , distance : Float
    , strength : Maybe Float
    }


{-| TEA messages within this view
-}
type GraphMsg
    = DragStart ViewElement
    | DragAt Vec2
    | DragEnd Vec2
    | ZoomIn
    | ZoomOut
    | StateChange Int String FP.StateValue
    | OptionsChange Int String FP.OptionValue
    | Tick
    | ToggleShowOptions Int
    | ToggleShowStates Int


{-| Graph status values defined by Falcon
-}
type GraphStatus
    = NOGRAPH
    | CONSTRUCTING
    | PREPARING
    | READY
    | STARTING
    | PROCESSING
    | STOPPING


{-| Global top-level key/value mappings provided by Falcon
-}
type alias FalconInfo =
    { version : String
    , environment : String
    , resources : String
    , status : GraphStatus
    , test : Bool
    }


type alias UiLocation =
    { x : Float
    , y : Float
    , width : Float
    , height : Float
    }


{-| Default display width for a node
-}
nodeWidth : Float
nodeWidth =
    120


{-| Default display height for a node
-}
nodeHeight : Float
nodeHeight =
    55


{-| Parse a YAML graph definition into a `FalconGraph` structure
-}
loadGraph : Float -> Float -> String -> Result String FalconGraph
loadGraph width height yaml =
    let
        errorMessage : Yaml.Error -> String
        errorMessage err =
            case err of
                Yaml.Parsing msg ->
                    "Parsing graph: " ++ msg

                Yaml.Decoding msg ->
                    "Decoding graph: " ++ msg

        sourceNodeForce : GraphImpl -> ( Force.Force NodeId, Force.Force NodeId )
        sourceNodeForce g =
            Graph.nodes g
                |> List.filter (\node -> node.label.value.mark == Source)
                |> List.map
                    (\node ->
                        ( { node = node.id
                          , strength = 10
                          , target = width / 2
                          }
                        , { node = node.id
                          , strength = 1
                          , target = height / 10
                          }
                        )
                    )
                |> List.unzip
                |> Tuple.mapBoth Force.towardsX Force.towardsY

        link : Edge EdgeLabel -> Link
        link { from, to } =
            { source = from
            , target = to
            , distance = 50
            , strength = Just 1
            }

        simulation : GraphImpl -> Force.State NodeId
        simulation graph =
            let
                ( toX, toY ) =
                    sourceNodeForce graph
            in
            Force.simulation
                [ Force.customLinks 4 <| List.map link <| Graph.edges graph
                , Force.center (width / 2) (height / 2)
                , toX
                , toY
                , Force.collision 100 <| List.map .id <| Graph.nodes graph
                ]
                |> Force.iterations 30
    in
    Yaml.fromString decodeFalconGraph yaml
        |> Result.mapError errorMessage
        |> Result.map
            (\graph ->
                { graph = graph
                , simulation = simulation graph
                , selected = Nothing
                , viewPort = vec2 width height
                , viewPortOffset = vec2 0 0
                , scale = 1.0
                , status = NOGRAPH
                , dirty = Nothing
                }
            )


{-| Encode a `FalconGraph` structure into valid YAML text that can be parsed by Falcon
-}
saveGraph : FalconGraph -> String
saveGraph graph =
    Encode.toString 4 (encodeFalconGraph graph.graph)


{-| Convert a `FalconGraph` structure into SVG
-}
drawGraph : Int -> FalconGraph -> Html GraphMsg
drawGraph viewHeight graph =
    let
        selected : Maybe ( Int, Node Entity )
        selected =
            Maybe.andThen
                (\el ->
                    case el of
                        ViewNode id _ ->
                            Graph.get id graph.graph
                                |> Maybe.map (Tuple.pair id << .node)

                        _ ->
                            Nothing
                )
                graph.selected

        -- Draw the "selected" node last (top)
        nodes : List (Node Entity)
        nodes =
            case selected of
                Nothing ->
                    Graph.nodes graph.graph

                Just ( id, node ) ->
                    List.filter (\n -> n.id /= id) (Graph.nodes graph.graph) ++ [ node ]

        -- The "pointy" part of node connectors
        arrowHead : List SvgPath.SubPath
        arrowHead =
            [ { moveto = SvgPath.MoveTo SvgPath.Relative ( 0, 0 )
              , drawtos =
                    [ SvgPath.LineTo SvgPath.Relative [ ( 0, 4 ), ( 4, -2 ) ]
                    , SvgPath.ClosePath
                    ]
              }
            ]

        -- The background grid for the graph display
        grid : List SvgPath.SubPath
        grid =
            [ { moveto = SvgPath.MoveTo SvgPath.Absolute ( 0, 0 )
              , drawtos =
                    [ SvgPath.LineTo SvgPath.Absolute [ ( 60, 0 ), ( 60, 60 ), ( 0, 60 ) ]
                    , SvgPath.ClosePath
                    ]
              }
            ]

        definitions : Svg msg
        definitions =
            defs []
                [ marker
                    [ id "arrow"
                    , markerWidth 10
                    , markerHeight 10
                    , refX "3"
                    , refY "2"
                    , orient "auto"
                    ]
                    [ path
                        [ d (SvgPath.toString arrowHead)
                        , TypedSvg.Attributes.fill <| Paint Color.blue
                        ]
                        []
                    ]
                , pattern
                    [ id "grid"
                    , TypedSvg.Attributes.InPx.width 60
                    , TypedSvg.Attributes.InPx.height 60
                    , TypedSvg.Attributes.patternUnits CoordinateSystemUserSpaceOnUse
                    ]
                    [ path
                        [ d (SvgPath.toString grid)
                        , TypedSvg.Attributes.stroke <| Paint Color.grey
                        , TypedSvg.Attributes.InPx.strokeWidth 1
                        , TypedSvg.Attributes.fill <| Paint Color.white
                        ]
                        []
                    ]
                ]

        dragStartHandler : Mouse.Event -> GraphMsg
        dragStartHandler evt =
            DragStart (Canvas << uncurry vec2 <| evt.offsetPos)

        dragHandler : Mouse.Event -> GraphMsg
        dragHandler evt =
            DragAt (uncurry vec2 evt.offsetPos)

        dragEndHandler : Mouse.Event -> GraphMsg
        dragEndHandler evt =
            DragEnd (uncurry vec2 evt.offsetPos)

        wheelHandler : Wheel.Event -> GraphMsg
        wheelHandler evt =
            if evt.deltaY < 0 then
                ZoomOut

            else
                ZoomIn
    in
    svg
        [ Html.Attributes.style "display" "block"
        , Html.Attributes.style "height" "100%"
        , Html.Attributes.style "width" "100%"
        , Html.Attributes.id "graph-view"
        , TypedSvg.Attributes.viewBox
            (Crd.getX graph.viewPortOffset)
            (Crd.getY graph.viewPortOffset)
            (Crd.getX graph.viewPort * graph.scale)
            ((toFloat viewHeight - 40) * graph.scale)
        ]
        [ definitions

        -- Background
        , rect
            [ TypedSvg.Attributes.InPx.width 20000
            , TypedSvg.Attributes.InPx.height 20000
            , TypedSvg.Attributes.InPx.x -10000
            , TypedSvg.Attributes.InPx.y -10000
            , TypedSvg.Attributes.fill <| Reference "grid"
            , TypedSvg.Attributes.pointerEvents "all"
            , Mouse.onDown dragStartHandler
            , Mouse.onMove dragHandler
            , Mouse.onUp dragEndHandler
            , Wheel.onWheel wheelHandler
            ]
            []

        -- Connections
        , g [ class [ "links" ] ] <|
            List.map (drawLinkElement graph.graph) <|
                Graph.edges graph.graph

        -- Processors
        , g [ class [ "nodes" ] ] <|
            List.map (drawNodeElement graph.status) nodes
        ]


{-| Change the viewport size
-}
resize : Float -> Float -> FalconGraph -> FalconGraph
resize width height graph =
    let
        resized : Vec2
        resized =
            vec2 width height
    in
    { graph | viewPort = resized }


{-| Reset back to origin pan position and no zoom
-}
reset : FalconGraph -> FalconGraph
reset graph =
    { graph | viewPortOffset = vec2 0 0, scale = 1.0 }


{-| TEA handling of graph messages
-}
update : GraphMsg -> FalconGraph -> ( FalconGraph, Cmd msg )
update msg graph =
    case msg of
        DragStart viewElement ->
            if graph.selected == Nothing then
                ( { graph | selected = Just <| mouseOffset graph viewElement }, Cmd.none )

            else
                ( graph, Cmd.none )

        DragAt position ->
            case graph.selected of
                Just (ViewNode nodeId coord) ->
                    ( { graph
                        | graph =
                            moveNode
                                nodeId
                                (Crd.sub position coord
                                    |> Crd.add
                                        (Crd.scale (1 / graph.scale) graph.viewPortOffset)
                                    |> Crd.scale graph.scale
                                )
                                graph.graph
                      }
                    , Cmd.none
                    )

                Just (Canvas coord) ->
                    ( { graph
                        | viewPortOffset = coordinateMove graph.scale graph.viewPortOffset coord position
                        , selected = Just (Canvas position)
                      }
                    , Cmd.none
                    )

                _ ->
                    ( graph, Cmd.none )

        DragEnd position ->
            case graph.selected of
                Just (ViewNode nodeId coord) ->
                    ( { graph
                        | graph =
                            moveNode
                                nodeId
                                (Crd.sub position coord
                                    |> Crd.add (Crd.scale (1 / graph.scale) graph.viewPortOffset)
                                    |> Crd.scale graph.scale
                                )
                                graph.graph
                        , selected = Nothing
                      }
                    , Cmd.none
                    )

                Just (Canvas coord) ->
                    ( { graph
                        | viewPortOffset = coordinateMove graph.scale graph.viewPortOffset coord position
                        , selected = Nothing
                      }
                    , Cmd.none
                    )

                _ ->
                    ( graph, Cmd.none )

        ZoomIn ->
            ( { graph | scale = graph.scale + 0.1 }, Cmd.none )

        ZoomOut ->
            ( { graph | scale = max 0.1 (graph.scale - 0.1) }, Cmd.none )

        StateChange nodeId stateName value ->
            let
                updatedGraph =
                    changeState nodeId stateName value graph.graph
            in
            ( { graph | graph = updatedGraph }
            , cmdUpdateState updatedGraph nodeId stateName
            )

        OptionsChange nodeId optionName value ->
            let
                updatedGraph =
                    changeOptions nodeId optionName value graph.graph

                dirty =
                    if graph.dirty == Nothing then
                        Just graph.graph

                    else
                        graph.dirty
            in
            ( { graph | graph = updatedGraph, dirty = dirty }, Cmd.none )

        Tick ->
            let
                ( newState, entities ) =
                    Force.tick graph.simulation <| List.map .label <| Graph.nodes graph.graph
            in
            ( { graph
                | graph = List.foldr changeEntity graph.graph entities
                , simulation = newState
              }
            , Cmd.none
            )

        ToggleShowOptions nodeId ->
            ( { graph | graph = Graph.update nodeId (Maybe.map (toggleShowUi Options)) graph.graph }, Cmd.none )

        ToggleShowStates nodeId ->
            ( { graph | graph = Graph.update nodeId (Maybe.map (toggleShowUi States)) graph.graph }, Cmd.none )


{-| Update the state of the Falcon state machine
-}
graphStatus : GraphStatus -> FalconGraph -> FalconGraph
graphStatus graphstatus graph =
    { graph | status = graphstatus }


{-| Parse a string into a `FalconInfo`
-}
status : String -> Result String FalconInfo
status info =
    let
        showYamlError : Yaml.Error -> String
        showYamlError err =
            case err of
                Parsing description ->
                    description

                Decoding description ->
                    description
    in
    Yaml.fromString decodeInfo info
        |> Result.mapError showYamlError


{-| Convert an EdgeLabel (which could represent multiple edges) into a list (multiple edges)
of String
-}
showEdgeLabel : EdgeLabel -> List String
showEdgeLabel label =
    List.map
        (\connector -> FP.saveConnector connector.from ++ "=" ++ FP.saveConnector connector.to)
        label


{-| Update the "Options" and "State" in the "old" graph with the "Options"
and "State" in the "new" graph.

This preserves the processor context such as location on the screen.

-}
unionGraphs : FalconGraph -> FalconGraph -> Result String FalconGraph
unionGraphs oldGraph newGraph =
    let
        nameAndClass : FP.FalconProcessor -> String
        nameAndClass p =
            p.name ++ " " ++ p.class

        processors : List (Node Entity) -> List String
        processors nodes =
            nodes
                |> List.map (.label >> .value >> .processor >> nameAndClass)
                |> List.sort

        edgeLabels : List (Edge EdgeLabel) -> List String
        edgeLabels edges =
            edges
                |> ListE.andThen (showEdgeLabel << .label)
                |> List.sort

        updateProc : Entity -> Entity -> Entity
        updateProc old new =
            Lens.modify processorLens
                (\proc ->
                    { proc
                        | options = Dict.union new.value.processor.options proc.options
                        , state = Dict.union new.value.processor.state proc.state
                    }
                )
                old
    in
    -- If the processor names and classes are the same and the edge labels are the same then this is the same graph
    if
        List.all
            (uncurry (==))
            [ ( processors (Graph.nodes newGraph.graph), processors (Graph.nodes oldGraph.graph) )
            , ( edgeLabels (Graph.edges newGraph.graph), edgeLabels (Graph.edges oldGraph.graph) )
            ]
    then
        -- Update options and states for each processor
        Ok
            { oldGraph
                | graph =
                    Graph.mapNodes
                        (\node ->
                            Maybe.map (updateProc node) (Maybe.map (.node >> .label) (Graph.get node.id newGraph.graph))
                                |> Maybe.withDefault node
                        )
                        oldGraph.graph
            }

    else
        Err "These graphs are not the same"


{-| Make a dirty graph clean. Delete the dirty graph without
changing the active graph.
-}
dropDirty : FalconGraph -> FalconGraph
dropDirty graph =
    { graph | dirty = Nothing }


{-| Replace the active graph with the dirty graph if
it exists. And remove the dirty graph.
-}
useDirty : FalconGraph -> FalconGraph
useDirty graph =
    { graph
        | graph = Maybe.withDefault graph.graph graph.dirty
        , dirty = Nothing
    }


{-| Parse a Falcon state machine state string into a `GraphStatus` type.
-}
parseGraphState : String -> Result String GraphStatus
parseGraphState state =
    case state of
        "NOGRAPH" ->
            Ok NOGRAPH

        "CONSTRUCTING" ->
            Ok CONSTRUCTING

        "PREPARING" ->
            Ok PREPARING

        "READY" ->
            Ok READY

        "STARTING" ->
            Ok STARTING

        "PROCESSING" ->
            Ok PROCESSING

        "STOPPING" ->
            Ok STOPPING

        _ ->
            Err ("Unknown graph state: " ++ state)


subscriptions : FalconGraph -> Sub GraphMsg
subscriptions graph =
    if Force.isCompleted graph.simulation then
        Sub.none

    else
        Browser.Events.onAnimationFrame (always Tick)



-- HELPERS


processorContext : FP.FalconProcessor -> ProcessorContext
processorContext proc =
    { processor = proc
    , width = nodeWidth
    , height = nodeHeight
    , ui = Nothing
    , mark = Inner
    }


cmdUpdateState : GraphImpl -> Int -> String -> Cmd msg
cmdUpdateState graph nodeId stateName =
    case Graph.get nodeId graph of
        Just ctx ->
            FP.saveState ctx.node.label.value.processor stateName
                |> FC.update

        _ ->
            Cmd.none


nodeLens : Lens FalconNodeContext (Node Entity)
nodeLens =
    let
        get : FalconNodeContext -> Node Entity
        get ctx =
            ctx.node

        set : Node Entity -> FalconNodeContext -> FalconNodeContext
        set new ctx =
            { ctx | node = new }
    in
    Lens get set


labelLens : Lens (Node Entity) Entity
labelLens =
    let
        get : Node Entity -> Entity
        get node =
            node.label

        set : Entity -> Node Entity -> Node Entity
        set new node =
            { node | label = new }
    in
    Lens get set


processorLens : Lens Entity FP.FalconProcessor
processorLens =
    let
        get : ProcessorContext -> FP.FalconProcessor
        get ctx =
            ctx.processor

        set : FP.FalconProcessor -> ProcessorContext -> ProcessorContext
        set new ctx =
            { ctx | processor = new }
    in
    Lens.compose processorContextLens (Lens get set)


uiLens : Lens Entity (Maybe NodeUi)
uiLens =
    let
        get : ProcessorContext -> Maybe NodeUi
        get entity =
            entity.ui

        set : Maybe NodeUi -> ProcessorContext -> ProcessorContext
        set new entity =
            { entity | ui = new }
    in
    Lens.compose processorContextLens (Lens get set)


processorContextLens : Lens Entity ProcessorContext
processorContextLens =
    let
        get entity =
            entity.value

        set new entity =
            { entity | value = new }
    in
    Lens get set


moveNode : Int -> Vec2 -> GraphImpl -> GraphImpl
moveNode nodeId position graph =
    let
        mover : FalconNodeContext -> FalconNodeContext
        mover ctx =
            Lens.modify
                (Lens.compose nodeLens labelLens)
                (\crd -> { crd | x = Crd.getX position, y = Crd.getY position })
                ctx
    in
    Graph.update nodeId (Maybe.map mover) graph


changeState : Int -> String -> FP.StateValue -> GraphImpl -> GraphImpl
changeState nodeId name value graph =
    let
        changer : FalconNodeContext -> FalconNodeContext
        changer ctx =
            Lens.modify
                (Lens.compose nodeLens (Lens.compose labelLens processorLens))
                (FP.updateState name value)
                ctx
    in
    Graph.update nodeId (Maybe.map changer) graph


changeOptions : Int -> String -> FP.OptionValue -> GraphImpl -> GraphImpl
changeOptions nodeId name value graph =
    let
        changer : FalconNodeContext -> FalconNodeContext
        changer ctx =
            Lens.modify
                (Lens.compose nodeLens (Lens.compose labelLens processorLens))
                (FP.updateOptions name value)
                ctx
    in
    Graph.update nodeId (Maybe.map changer) graph


changeEntity : Entity -> GraphImpl -> GraphImpl
changeEntity entity graph =
    let
        changer : FalconNodeContext -> FalconNodeContext
        changer =
            Lens.modify nodeLens (\node -> { node | label = entity })
    in
    Graph.update entity.id (Maybe.map changer) graph


toggleShowUi : NodeUi -> FalconNodeContext -> FalconNodeContext
toggleShowUi ui ctx =
    Lens.modify
        (Lens.compose nodeLens (Lens.compose labelLens uiLens))
        (\mui ->
            if mui == Just ui then
                Nothing

            else
                Just ui
        )
        ctx


makeGraph : List FP.FalconProcessor -> List EdgeLabel -> GraphImpl
makeGraph processors connectors =
    let
        checkConnected : ( FP.FalconProcessor, FP.FalconProcessor, EdgeLabel ) -> Bool
        checkConnected ( p1, p2, label ) =
            let
                froms : List String
                froms =
                    List.map (.processor << .from) label

                tos : List String
                tos =
                    List.map (.processor << .to) label
            in
            List.member p1.name froms
                && List.member p2.name tos

        whereIs : FP.FalconProcessor -> List FP.FalconProcessor -> Maybe Int
        whereIs p ps =
            List.indexedMap Tuple.pair ps
                |> ListE.find (\( _, proc ) -> proc.name == p.name)
                |> Maybe.map Tuple.first

        edges : List (Edge EdgeLabel)
        edges =
            ListE.lift3 (\p1 p2 conn -> ( p1, p2, conn ))
                processors
                processors
                connectors
                |> List.filter checkConnected
                |> List.map
                    (\( p1, p2, cs ) ->
                        ( whereIs p1 processors, whereIs p2 processors, cs )
                    )
                |> List.filterMap
                    (\ps ->
                        case ps of
                            ( Just a, Just b, cs ) ->
                                Just ( a, b, cs )

                            _ ->
                                Nothing
                    )
                |> List.map (\( p1, p2, cs ) -> Edge p1 p2 cs)

        markNodeContext : FalconNodeContext -> FalconNodeContext
        markNodeContext ctx =
            case ( IntDict.isEmpty ctx.incoming, IntDict.isEmpty ctx.outgoing ) of
                ( True, _ ) ->
                    Lens.modify
                        (Lens.compose (Lens.compose nodeLens labelLens) processorContextLens)
                        (\proc -> { proc | mark = Source })
                        ctx

                ( _, True ) ->
                    Lens.modify
                        (Lens.compose (Lens.compose nodeLens labelLens) processorContextLens)
                        (\proc -> { proc | mark = Sink })
                        ctx

                _ ->
                    ctx

        nodes : List (Node Entity)
        nodes =
            processors
                |> List.map processorContext
                |> List.indexedMap Force.entity
                |> List.map (\entity -> Node entity.id entity)
                |> flip Graph.fromNodesAndEdges edges
                |> Graph.mapContexts markNodeContext
                |> Graph.nodes
    in
    Graph.fromNodesAndEdges nodes edges


decodeFalconGraph : Yaml.Decoder GraphImpl
decodeFalconGraph =
    let
        attachOptions :
            Dict String (Dict String FP.OptionValue)
            -> FP.FalconProcessor
            -> FP.FalconProcessor
        attachOptions options proc =
            FP.FalconProcessor
                proc.name
                proc.class
                proc.ports
                (Dict.union
                    proc.options
                    (Maybe.withDefault Dict.empty (Dict.get proc.name options))
                )
                proc.state
    in
    Yaml.map4 (\procs conns opts _ -> makeGraph (List.map (attachOptions opts) procs) conns)
        (Yaml.at [ "graph", "processors" ] decodeProcessors)
        (Yaml.at [ "graph", "connections" ] decodeConnections)
        (Yaml.maybe
            (Yaml.field "options"
                (Yaml.dict (Yaml.field "options" decodeOptions))
            )
            |> Yaml.map (Maybe.withDefault Dict.empty)
        )
        (Yaml.at [ "falcon", "version" ]
            (Yaml.oneOf
                [ Yaml.string
                , Yaml.int |> Yaml.map String.fromInt
                ]
            )
        )


decodeConnections : Yaml.Decoder (List EdgeLabel)
decodeConnections =
    let
        createLabels : String -> List (List FP.FalconConnector) -> Yaml.Decoder (List EdgeLabel)
        createLabels s connectors =
            case connectors of
                [ froms, tos ] ->
                    ListE.zip froms tos
                        |> List.map (\( from, to ) -> [ { from = from, to = to } ])
                        |> Yaml.succeed

                _ ->
                    Yaml.fail ("Cannot determine connections from '" ++ s ++ "'")

        parse : String -> Yaml.Decoder (List EdgeLabel)
        parse s =
            String.split "=" s
                |> List.map FP.parseConnector
                |> createLabels s

        equalEnds : EdgeLabel -> EdgeLabel -> Bool
        equalEnds a b =
            (List.map (.processor << .from) a == List.map (.processor << .from) b)
                && (List.map (.processor << .to) a == List.map (.processor << .to) b)

        gatherImpl : EdgeLabel -> List EdgeLabel -> EdgeLabel
        gatherImpl first rest =
            first ++ List.concat rest

        gather : List ( EdgeLabel, List EdgeLabel ) -> List EdgeLabel
        gather =
            List.map (uncurry gatherImpl)
    in
    Yaml.list (Yaml.string |> Yaml.andThen parse)
        |> Yaml.map (gather << ListE.gatherWith equalEnds << List.concat)


decodeProcessors : Yaml.Decoder (List FP.FalconProcessor)
decodeProcessors =
    let
        createProcessor : ( String, ( String, Maybe (Dict.Dict String FP.OptionValue), Maybe (Dict.Dict String FP.State) ) ) -> List FP.FalconProcessor
        createProcessor ( name, ( class, options, state ) ) =
            String.trim name
                |> FP.parseProcessor
                |> List.map
                    (\n ->
                        FP.FalconProcessor n
                            class
                            []
                            (Maybe.withDefault Dict.empty options)
                            (Maybe.withDefault Dict.empty state)
                    )
    in
    Yaml.dict
        (Yaml.map3 (\cls opt state -> ( cls, opt, state ))
            (Yaml.field "class" Yaml.string)
            (Yaml.maybe (Yaml.field "options" decodeOptions))
            (Yaml.maybe (Yaml.field "states" (Yaml.dict FP.decodeState)))
        )
        |> Yaml.andThen
            (Yaml.succeed
                << ListE.andThen identity
                << List.map createProcessor
                << Dict.toList
            )


decodeOptions : Yaml.Decoder (Dict.Dict String FP.OptionValue)
decodeOptions =
    Yaml.maybe (Yaml.dict FP.decodeOption)
        |> Yaml.andThen (\a -> Yaml.succeed (Maybe.withDefault Dict.empty a))


encodeFalconGraph : GraphImpl -> Encode.Encoder
encodeFalconGraph graph =
    Encode.record
        [ ( "falcon", Encode.record [ ( "version", Encode.float 1.0 ) ] )
        , ( "graph"
          , Encode.record
                [ ( "processors"
                  , encodeProcessors <|
                        List.map (.processor << .value << .label) <|
                            Graph.nodes graph
                  )
                , ( "connections"
                  , encodeConnections (List.map .label (Graph.edges graph))
                  )
                ]
          )
        , ( "options"
          , encodeOptions <|
                Dict.fromList <|
                    List.map
                        (\node ->
                            ( node.label.value.processor.name
                            , node.label.value.processor.options
                            )
                        )
                    <|
                        Graph.nodes graph
          )
        ]


encodeProcessors : List FP.FalconProcessor -> Encode.Encoder
encodeProcessors processors =
    let
        encodeProcessor : FP.FalconProcessor -> Encode.Encoder
        encodeProcessor proc =
            Encode.record
                [ ( "class", Encode.string proc.class )
                ]
    in
    List.map (\proc -> ( proc.name, proc )) processors
        |> Dict.fromList
        |> Encode.dict identity encodeProcessor


encodeConnections : List EdgeLabel -> Encode.Encoder
encodeConnections labels =
    Encode.list Encode.string (ListE.andThen showEdgeLabel labels)


encodeOptions : Dict.Dict String (Dict.Dict String FP.OptionValue) -> Encode.Encoder
encodeOptions opts =
    let
        encodeOptionDict : Dict.Dict String FP.OptionValue -> Encode.Encoder
        encodeOptionDict options =
            Encode.record
                [ ( "options", Encode.dict identity FP.encodeOption options )
                ]
    in
    Dict.filter (\_ v -> Dict.size v > 0) opts
        |> Encode.dict identity encodeOptionDict


drawLinkElement : GraphImpl -> Edge EdgeLabel -> Svg msg
drawLinkElement graph edge =
    let
        coordinate : { a | x : Float, y : Float } -> Vec2
        coordinate e =
            vec2 e.x e.y

        source : Vec2
        source =
            Graph.get edge.from graph
                |> Maybe.map (coordinate << .label << .node)
                |> Maybe.withDefault (vec2 0.0 0.0)
                |> Crd.add (vec2 0.0 (nodeHeight / 2))

        target : Vec2
        target =
            let
                tgtNode : UiLocation
                tgtNode =
                    Graph.get edge.to graph
                        |> Maybe.map ((\e -> UiLocation e.x e.y e.value.width e.value.height) << .label << .node)
                        |> Maybe.withDefault (UiLocation 0 0 0 0)

                target_ =
                    coordinate tgtNode
                        |> Crd.add (vec2 0.0 (tgtNode.height / 2))

                nodeDimensions : ( Float, Float )
                nodeDimensions =
                    ( tgtNode.height / 2, tgtNode.width / 2 )

                diff : Vec2
                diff =
                    Crd.sub target_ source
            in
            case
                ( abs (Crd.getY diff) < abs (Crd.getX diff)
                , Crd.getY source > Crd.getY target_
                )
            of
                ( False, True ) ->
                    vec2
                        (Crd.getX target_)
                        (Crd.getY target_ + Tuple.first nodeDimensions)

                ( False, False ) ->
                    vec2
                        (Crd.getX target_)
                        (Crd.getY target_ - Tuple.first nodeDimensions)

                ( True, False ) ->
                    vec2
                        (if Crd.getX target_ > Crd.getX source then
                            Crd.getX target_ - Tuple.second nodeDimensions

                         else
                            Crd.getX target_ + Tuple.second nodeDimensions
                        )
                        (Crd.getY target_ - Tuple.first nodeDimensions)

                ( True, True ) ->
                    vec2
                        (if Crd.getX target_ > Crd.getX source then
                            Crd.getX target_ - Tuple.second nodeDimensions

                         else
                            Crd.getX target_ + Tuple.second nodeDimensions
                        )
                        (Crd.getY target_ + Tuple.first nodeDimensions)
    in
    line
        [ strokeWidth 3
        , stroke <| Paint Color.blue
        , x1 <| Crd.getX source
        , y1 <| Crd.getY source
        , x2 <| Crd.getX target
        , y2 <| Crd.getY target
        , markerEnd "url(#arrow)"
        ]
        []


handleOptionChange : (FP.OptionValue -> GraphMsg) -> Json.Decoder GraphMsg
handleOptionChange messageCtor =
    Json.map messageCtor
        (Json.at [ "target", "value" ] Json.string
            |> Json.andThen
                (\val ->
                    case Yaml.fromString FP.decodeOption val of
                        Ok opt ->
                            Json.succeed opt

                        Err err ->
                            Json.fail (Yaml.errorToString err)
                )
        )


drawNodeElement : GraphStatus -> Node Entity -> Svg GraphMsg
drawNodeElement graphstatus node =
    let
        top : Float
        top =
            node.label.y

        left : Float
        left =
            node.label.x - (node.label.value.width / 2)

        width : Float
        width =
            node.label.value.width

        nodeName : String
        nodeName =
            node.label.value.processor.name

        stateHeight : FP.StateValue -> Float
        stateHeight s =
            case s of
                FP.StateBool _ ->
                    25

                FP.StateNum _ ->
                    50

        optionHeight : FP.OptionValue -> Float
        optionHeight s =
            case s of
                FP.OptionBool _ ->
                    25

                FP.OptionNum _ ->
                    50

                FP.OptionStr _ ->
                    50

                FP.OptionList _ ->
                    50

                FP.OptionMap _ ->
                    50

        height : Float
        height =
            case node.label.value.ui of
                Just States ->
                    nodeHeight
                        + (List.map
                            (stateHeight << .value)
                            (Dict.values node.label.value.processor.state)
                            |> List.sum
                          )
                        + 5

                Just Options ->
                    nodeHeight
                        + (List.map
                            optionHeight
                            (Dict.values node.label.value.processor.options)
                            |> List.sum
                          )
                        + 5

                Nothing ->
                    nodeHeight

        uiloc : UiLocation
        uiloc =
            { x = left + 2
            , y = top + 55
            , width = width - 4
            , height = height - 36
            }

        viewOption : ( String, FP.OptionValue ) -> Html GraphMsg
        viewOption ( name, value ) =
            case value of
                FP.OptionBool b ->
                    Html.div
                        [ Html.Attributes.style "width" (String.fromFloat width ++ "px")
                        ]
                        [ Html.input
                            [ Html.Attributes.type_ "checkbox"
                            , Html.Attributes.checked b
                            , Html.Attributes.id (nodeName ++ name)
                            , Html.Attributes.disabled (graphstatus /= READY)
                            , onCheck (\checked -> OptionsChange node.id name (FP.OptionBool checked))
                            ]
                            []
                        , Html.label
                            [ Html.Attributes.for (nodeName ++ name)
                            , Html.Attributes.style "font-size" "10px"
                            , Html.Attributes.title name
                            ]
                            [ Html.text name ]
                        ]

                FP.OptionNum n ->
                    Html.div
                        [ Html.Attributes.style "width"
                            (String.fromFloat width ++ "px")
                        ]
                        [ Html.label
                            [ Html.Attributes.for (nodeName ++ name)
                            , Html.Attributes.style "font-size" "10px"
                            , Html.Attributes.style "display" "block"
                            , Html.Attributes.title name
                            ]
                            [ Html.text name ]
                        , Html.input
                            [ Html.Attributes.type_ "number"
                            , Html.Attributes.step "any"
                            , Html.Attributes.id (nodeName ++ name)
                            , Html.Attributes.disabled (graphstatus /= READY)
                            , Html.Attributes.style "height" "32px"
                            , Html.Attributes.style "width" (String.fromFloat (width - 20) ++ "px")
                            , Html.Attributes.style "display" "block"
                            , Html.Attributes.value (String.fromFloat n)
                            , on "change" (handleOptionChange (OptionsChange node.id name))
                            ]
                            []
                        ]

                _ ->
                    Html.div
                        [ Html.Attributes.style "width"
                            (String.fromFloat width ++ "px")
                        ]
                        [ Html.label
                            [ Html.Attributes.for (nodeName ++ name)
                            , Html.Attributes.style "font-size" "10px"
                            , Html.Attributes.style "display" "block"
                            , Html.Attributes.title name
                            ]
                            [ Html.text name ]
                        , Html.input
                            [ Html.Attributes.id (nodeName ++ name)
                            , Html.Attributes.style "height" "32px"
                            , Html.Attributes.style "width" (String.fromFloat (width - 20) ++ "px")
                            , Html.Attributes.style "display" "block"
                            , Html.Attributes.value (Encode.toString 0 (FP.encodeOption value))
                            , Html.Attributes.disabled (graphstatus /= READY)
                            , on "input" (handleOptionChange (OptionsChange node.id name))
                            ]
                            []
                        ]

        viewState : ( String, FP.State ) -> Html GraphMsg
        viewState ( name, state ) =
            case state.value of
                FP.StateBool b ->
                    Html.div
                        [ Html.Attributes.style "width" (String.fromFloat (width - 2) ++ "px")
                        ]
                        [ Html.input
                            [ Html.Attributes.type_ "checkbox"
                            , Html.Attributes.checked b
                            , Html.Attributes.id (nodeName ++ name)
                            , Html.Attributes.disabled (state.permission /= FP.Write)
                            , onCheck (\checked -> StateChange node.id name (FP.StateBool checked))
                            ]
                            []
                        , Html.label
                            [ Html.Attributes.for (nodeName ++ name)
                            , Html.Attributes.style "font-size" "10px"
                            , Html.Attributes.title name
                            ]
                            [ Html.text name ]
                        ]

                FP.StateNum n ->
                    Html.div
                        [ Html.Attributes.style "width"
                            (String.fromFloat (width - 2) ++ "px")
                        ]
                        [ Html.label
                            [ Html.Attributes.for (nodeName ++ name)
                            , Html.Attributes.style "font-size" "10px"
                            , Html.Attributes.style "display" "block"
                            , Html.Attributes.title name
                            ]
                            [ Html.text name ]
                        , Html.input
                            [ Html.Attributes.type_ "number"
                            , Html.Attributes.step "any"
                            , Html.Attributes.id (nodeName ++ name)
                            , Html.Attributes.style "height" "32px"
                            , Html.Attributes.style "width" (String.fromFloat (width - 20) ++ "px")
                            , Html.Attributes.style "display" "block"
                            , Html.Attributes.value (String.fromFloat n)
                            , Html.Attributes.disabled (state.permission /= FP.Write)
                            , on "change"
                                (Json.andThen
                                    (JsonE.fromMaybe "Could not decode a number"
                                        << Maybe.map (StateChange node.id name << FP.StateNum)
                                        << String.toFloat
                                    )
                                    targetValue
                                )
                            ]
                            []
                        ]
    in
    g []
        ([ rect
            -- Main node body
            [ TypedSvg.Attributes.InPx.width width
            , TypedSvg.Attributes.InPx.height height
            , TypedSvg.Attributes.InPx.rx 3
            , TypedSvg.Attributes.InPx.x left
            , TypedSvg.Attributes.InPx.y top
            , case node.label.value.mark of
                Source ->
                    TypedSvg.Attributes.fill <| Paint <| Color.rgb255 100 200 150

                Sink ->
                    TypedSvg.Attributes.fill <| Paint <| Color.rgb255 100 100 200

                Inner ->
                    TypedSvg.Attributes.fill <| Paint <| Color.rgb255 100 150 250
            , TypedSvg.Attributes.pointerEvents "none"
            , stroke <| Paint <| Color.black
            , strokeWidth 1
            ]
            []
         , rect
            -- Mouse click area
            [ TypedSvg.Attributes.InPx.width (width - 20)
            , TypedSvg.Attributes.InPx.height 10
            , TypedSvg.Attributes.InPx.x (left + 10)
            , TypedSvg.Attributes.InPx.y (top + 0.5)
            , TypedSvg.Attributes.fill <| Paint Color.grey
            , TypedSvg.Attributes.pointerEvents "visible"
            , Mouse.onDown
                (\evt ->
                    DragStart
                        (ViewNode node.id <|
                            uncurry vec2 evt.offsetPos
                        )
                )
            , Mouse.onMove (\evt -> DragAt (uncurry vec2 evt.offsetPos))
            , Mouse.onUp (\evt -> DragEnd (uncurry vec2 evt.offsetPos))
            ]
            []
         , line
            [ x1 left
            , y1 (top + 11)
            , x2 (left + width)
            , y2 (top + 11)
            , stroke <| Paint Color.black
            ]
            []
         , text_
            [ TypedSvg.Attributes.InPx.x (left + 3)
            , TypedSvg.Attributes.InPx.y (top + 22)
            , fontSize (Px 10)
            , TypedSvg.Attributes.pointerEvents "none"
            ]
            [ TypedSvg.Core.text node.label.value.processor.name ]
         , text_
            [ TypedSvg.Attributes.InPx.x (left + 3)
            , TypedSvg.Attributes.InPx.y (top + 34)
            , fontSize (Px 10)
            , TypedSvg.Attributes.fontWeight FontWeightBold
            , TypedSvg.Attributes.pointerEvents "none"
            ]
            [ TypedSvg.Core.text node.label.value.processor.class ]
         , line
            [ x1 (left + 3)
            , y1 (top + 54.5)
            , x2 (left + 27)
            , y2 (top + 54.5)
            , stroke <| Paint Color.black
            ]
            []
         , line
            [ x1 (left + 92.5)
            , y1 (top + 54.5)
            , x2 (left + width - 6)
            , y2 (top + 54.5)
            , stroke <| Paint Color.black
            ]
            []
         , svgTab
            (UiLocation (left + 27) (top + 41) 32 14)
            (UiLocation (left + 29.5) (top + 50) 0 0)
            "Options"
            (case node.label.value.ui of
                Nothing ->
                    ( Color.black, Color.white )

                Just States ->
                    ( Color.black, Color.white )

                Just Options ->
                    ( Color.rgba 0.0 0.0 0.0 0.0, Color.black )
            )
            (ToggleShowOptions node.id)
         , svgTab
            (UiLocation (left + 60.5) (top + 41) 32 14)
            (UiLocation (left + 65) (top + 50) 0 0)
            "States"
            (case node.label.value.ui of
                Nothing ->
                    ( Color.black, Color.white )

                Just Options ->
                    ( Color.black, Color.white )

                Just States ->
                    ( Color.rgba 0.0 0.0 0.0 0.0, Color.black )
            )
            (ToggleShowStates node.id)
         ]
            ++ (Maybe.andThen
                    (\ui ->
                        case ui of
                            Options ->
                                showOptionsUi uiloc viewOption (Dict.toList node.label.value.processor.options)

                            States ->
                                showStatesUi uiloc viewState (Dict.toList node.label.value.processor.state)
                    )
                    node.label.value.ui
                    |> List.singleton
                    |> List.filterMap identity
               )
        )


svgTab : UiLocation -> UiLocation -> String -> ( Color, Color ) -> GraphMsg -> Svg GraphMsg
svgTab tabpos namepos name ( tabcolor, namecolor ) msg =
    g
        []
        [ rect
            [ TypedSvg.Attributes.InPx.x tabpos.x
            , TypedSvg.Attributes.InPx.y tabpos.y
            , TypedSvg.Attributes.InPx.width tabpos.width
            , TypedSvg.Attributes.InPx.height tabpos.height
            , TypedSvg.Attributes.fill <| Paint tabcolor
            , E.onClick msg
            ]
            []
        , polyline
            [ TypedSvg.Attributes.fill PaintNone
            , stroke <| Paint Color.black
            , TypedSvg.Attributes.points
                [ ( tabpos.x, tabpos.y + tabpos.height )
                , ( tabpos.x, tabpos.y )
                , ( tabpos.x + tabpos.width, tabpos.y )
                , ( tabpos.x + tabpos.width, tabpos.y + tabpos.height )
                ]
            ]
            []
        , text_
            [ TypedSvg.Attributes.InPx.x namepos.x
            , TypedSvg.Attributes.InPx.y namepos.y
            , fontSize (Px 8)
            , TypedSvg.Attributes.fill <| Paint namecolor
            , TypedSvg.Attributes.pointerEvents "none"
            ]
            [ TypedSvg.Core.text name ]
        ]


showOptionsUi :
    UiLocation
    -> (( String, FP.OptionValue ) -> Html GraphMsg)
    -> List ( String, FP.OptionValue )
    -> Maybe (Svg GraphMsg)
showOptionsUi { x, y, width, height } viewOption options =
    if List.length options > 0 then
        Just
            (foreignObject
                [ TypedSvg.Attributes.InPx.width width
                , TypedSvg.Attributes.InPx.height height
                , TypedSvg.Attributes.InPx.x x
                , TypedSvg.Attributes.InPx.y y
                , Html.Attributes.style "display" "flex"
                , Html.Attributes.style "flex-direction" "column"
                , Html.Attributes.style "padding-top" "5px"
                ]
                (List.map viewOption options)
            )

    else
        Nothing


showStatesUi :
    UiLocation
    -> (( String, FP.State ) -> Html GraphMsg)
    -> List ( String, FP.State )
    -> Maybe (Svg GraphMsg)
showStatesUi { x, y, width, height } viewState states =
    if List.length states > 0 then
        Just
            (foreignObject
                [ TypedSvg.Attributes.InPx.width width
                , TypedSvg.Attributes.InPx.height height
                , TypedSvg.Attributes.InPx.x x
                , TypedSvg.Attributes.InPx.y y
                , Html.Attributes.style "display" "flex"
                , Html.Attributes.style "flex-direction" "column"
                , Html.Attributes.style "padding-top" "5px"
                ]
                (List.map viewState states)
            )

    else
        Nothing


coordinateMove : Float -> Vec2 -> Vec2 -> Vec2 -> Vec2
coordinateMove scale toMove start end =
    Crd.sub start end
        |> Crd.scale scale
        |> Crd.add toMove


mouseToCanvas : Float -> Vec2 -> Vec2 -> Vec2
mouseToCanvas scale offset mouse =
    Crd.add mouse offset
        |> Crd.scale scale


mouseOffset : FalconGraph -> ViewElement -> ViewElement
mouseOffset graph ve =
    let
        offsetNode : Vec2 -> FalconNodeContext -> Vec2
        offsetNode mouse ctx =
            Crd.sub
                (mouseToCanvas
                    graph.scale
                    (Crd.scale (1 / graph.scale) graph.viewPortOffset)
                    mouse
                )
                (vec2 ctx.node.label.x ctx.node.label.y)
                |> Crd.scale (1 / graph.scale)
    in
    case ve of
        Canvas c ->
            Canvas c

        ViewNode id mouse ->
            let
                offset =
                    Maybe.map (offsetNode mouse) (Graph.get id graph.graph)
                        |> Maybe.withDefault (vec2 0.0 0.0)
            in
            ViewNode id offset


decodeInfo : Yaml.Decoder FalconInfo
decodeInfo =
    Yaml.map5 FalconInfo
        (Yaml.field "Falcon version" Yaml.string)
        (Yaml.field "run_environment_root" Yaml.string)
        (Yaml.field "resource_root" Yaml.string)
        (Yaml.field "graph_state" Yaml.string |> Yaml.andThen (parseGraphState >> Yaml.fromResult))
        (Yaml.field "default_test_flag" Yaml.bool)

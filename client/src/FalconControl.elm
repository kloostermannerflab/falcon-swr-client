port module FalconControl exposing
    ( build, destroy, getGraph, graphDefinition, resources, start, status, stop, update
    , connect, disconnect
    , clearPlot, resizePlots, resetPlot
    , bridgeVersion, receiveMessage
    )

{-| A central point for communicating with other (non-Elm) components of the system (especially Falcon).

There are 3 external components that this module provides access to:

  - The plot(s): `clearPlot`, `resizePlots`, and `resetPlot`
  - The bridge: `connect`, `disconnect`
  - Falcon: all others.

References:

  - <https://falcon-core.readthedocs.io/en/latest/ui/interaction.html>
  - <https://bitbucket.org/kloostermannerflab/falcon-client/src/master/falcon_clients/falcon_server.py>


# Falcon commands

@docs build, destroy, getGraph, graphDefinition, resources, start, status, stop, update


# Bridge commands

@docs connect, disconnect


# Plot commands

@docs clearPlot, resizePlots, resetPlot

-}

import Json.Decode as Decode
import Json.Encode as Encode


{-| Message types to be sent over the network to either the bridge of Falcon

`Control` messages are delivered to the bridge. `Request` messages are delivered to Falcon.

-}
type Message
    = Control ControlMethod
    | Request (List String)


{-| The bridge has 3 control functions.

  - `Connect` to Falcon with an address,
  - `Disconnect` from Falcon, or
  - `Terminate` the bridge (this is implemented in the bridge, just not here).

-}
type ControlMethod
    = Connect String
    | Disconnect String
    | Version


{-| Convert a `Message` into a string
-}
showCategory : Message -> String
showCategory m =
    case m of
        Control _ ->
            "control"

        Request _ ->
            "falcon"


{-| Encode a `Message` ready for placing into a port.

Note that the encoded structure matches how the bridge decodes. Bridge decoding
can be found in `falcon-bridge/src/model.rs`.

    message (Request [ "resources", "list" ]) -- Encodes the `resources list` command for Falcon

    message (Control (Connect "localhost")) -- Encodes a request to the bridge to connect to Falcon on localhost

-}
message : Message -> Encode.Value
message m =
    let
        encodeControl : ControlMethod -> Encode.Value
        encodeControl method =
            case method of
                Connect addr ->
                    Encode.object
                        [ ( "method", Encode.string "connect" )
                        , ( "args", Encode.string addr )
                        ]

                Disconnect addr ->
                    Encode.object
                        [ ( "method", Encode.string "disconnect" )
                        , ( "args", Encode.string addr )
                        ]

                Version ->
                    Encode.object
                        [ ( "method", Encode.string "version" ) ]

        argEncoder : Encode.Value
        argEncoder =
            case m of
                Control args ->
                    encodeControl args

                Request args ->
                    Encode.list Encode.string args
    in
    Encode.object
        [ ( "category", Encode.string (showCategory m) )
        , ( "args", argEncoder )
        ]



-- Falcon commands


{-| Get a list of all server-side graph resources.

See: <https://falcon-core.readthedocs.io/en/latest/ui/interaction.html?highlight=resources%20list>

-}
resources : Cmd msg
resources =
    sendMessage <|
        message (Request [ "resources", "list", "graphs" ])


{-| Load a server-side graph description.

See: <https://falcon-core.readthedocs.io/en/latest/ui/interaction.html?highlight=resources%20graphs>

-}
getGraph : String -> Cmd msg
getGraph uri =
    sendMessage <|
        message (Request [ "resources", "graphs", uri ])


{-| Ask Falcon for its map of key/value pair global status values.
These can include graph state, Falcon version, ...
See: <https://falcon-core.readthedocs.io/en/latest/ui/interaction.html?highlight=info>
-}
status : Cmd msg
status =
    sendMessage <|
        message (Request [ "info" ])


{-| Ask Falcon to start processing a built graph.
-}
start : Cmd msg
start =
    sendMessage <|
        message (Request [ "graph", "start" ])


{-| Ask Falcon to stop processing a graph.

See: <https://falcon-core.readthedocs.io/en/latest/ui/interaction.html?highlight=graph%20stop>

-}
stop : Cmd msg
stop =
    sendMessage <|
        message (Request [ "graph", "stop" ])


{-| Build a graph from YAML text.

See: <https://falcon-core.readthedocs.io/en/latest/ui/interaction.html?highlight=graph%20build>

-}
build : String -> Cmd msg
build graph =
    sendMessage <|
        message (Request [ "graph", "build", graph ])


{-| Destroy (opposite of build) a graph.
-}
destroy : Cmd msg
destroy =
    sendMessage <|
        message (Request [ "graph", "destroy" ])


{-| Retrieve the YAML text definition of a graph after it has been built.

See: <https://falcon-core.readthedocs.io/en/latest/ui/interaction.html?highlight=graph%20yaml>

-}
graphDefinition : Cmd msg
graphDefinition =
    sendMessage <|
        message (Request [ "graph", "yaml" ])


{-| Change the state of a node by providing a new definition.

See: <https://falcon-core.readthedocs.io/en/latest/ui/interaction.html?highlight=graph%20update>

-}
update : String -> Cmd msg
update node =
    sendMessage <|
        message (Request [ "graph", "update", node ])



-- Bridge commands


{-| Connect to Falcon at the given address.

The address should not have protocol or port definitions.

-}
connect : String -> Cmd msg
connect addrs =
    sendMessage <|
        message (Control (Connect addrs))


{-| Disconnect from Falcon.
-}
disconnect : String -> Cmd msg
disconnect addrs =
    sendMessage <|
        message (Control (Disconnect addrs))


{-| Get the Bridge version.
-}
bridgeVersion : Cmd msg
bridgeVersion =
    sendMessage <|
        message (Control Version)



-- Plot commands


{-| Ask any displayed plots to adjust their layout.

This should be used when adjusting internal layout.

-}
resizePlots : Cmd msg
resizePlots =
    sendMessage <|
        Encode.list Encode.string [ "plot", "resize" ]


{-| Remove all data from the plot display and show a blank plot.
-}
clearPlot : String -> Cmd msg
clearPlot plot =
    sendMessage <|
        Encode.list Encode.string [ "plot", "clear", plot ]


{-| Reset the real-time scrolling position.

e.g. go back to real-time scrolling after panning of zooming the plot.

-}
resetPlot : String -> Cmd msg
resetPlot plot =
    sendMessage <|
        Encode.list Encode.string [ "plot", "reset", plot ]



-- PORTS


port sendMessage : Encode.Value -> Cmd msg


port receiveMessage : (Decode.Value -> msg) -> Sub msg

/**
 * A [Custom](https://developer.mozilla.org/en-US/docs/Web/Web_Components)
 * [Element](https://html.spec.whatwg.org/multipage/custom-elements.html)
 * wrapping a [TimeChart](https://huww98.github.io/TimeChart/) HTMLCanvas chart.
 *
 * Plots `FalconSignal` data where `timestamp` is the x-axis, `signal` and
 * `threshold` channels are displayed on the y-axis.
 */

// @ts-ignore because we're waiting got parcel support - issue to track: https://github.com/parcel-bundler/parcel/issues/4155
import TimeChart from 'timechart/dist/lib/core';
import { lineChart } from 'timechart/dist/lib/plugins/lineChart';
import { d3Axis } from 'timechart/dist/lib/plugins/d3Axis';
import { legend } from 'timechart/dist/lib/plugins/legend';
import { crosshair } from 'timechart/dist/lib/plugins/crosshair';
import { TimeChartZoomPlugin } from 'timechart/dist/lib/plugins/chartZoom';
import { EventsPlugin } from 'timechart/dist/lib/plugins_extra/events';

import { scaleUtc } from 'd3-scale';

import { FalconSignal, FalconEvent } from './client';

/** Mapping of chart names to plots. */
export const chartdata: { [key: string]: EventPlot } = {};

/**
 * The time-series chart wrapper.
 * See: https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements
 */
class EventPlot extends HTMLElement {
    chart: TimeChart | undefined = undefined;
    name = '';
    legends: string[] = [];
    plot_event = false;

    readonly shadow: ShadowRoot;
    readonly container: HTMLDivElement;

    constructor() {
        super();

        this.shadow = this.attachShadow({ mode: 'open' });
        this.container = document.createElement('div');
        this.container.style.width = '100%';
        this.container.style.height = '100%';
        this.shadow.appendChild(this.container);
    }

    /**
     * Prompt the canvas to be resized.
     *
     * @remarks
     * This happens automatically if the browser window is resized.
     * This should be manually called in response to internal layout changes.
     * The delay is added because Elm updated the virtual DOM before the change
     * is reflected in the browser DOM which results in the resize not happening
     * (TimeChart can only see the "old" layout).
     * With the timeout delay Elm can adjust the virtual DOM and request a resize
     * here immediately.
     */
    resize(): void {
        setTimeout(() => {
            this.chart?.onResize();
        }, 500);
    }

    /** Remove all data in the chart. Clear the display. */
    clear(): void {
        if (this.chart) {
            this.chart.dispose();
            this.chart = this.createChart();
        }
    }

    /** Follow incoming data in real time. */
    reset(): void {
        if (this.chart) {
            this.chart.options.realTime = true;
            this.chart.update();
        }
    }

    /** Add data to the plot. */
    pushData(toadd: FalconSignal[]): void {
        if (this.chart) {
            const stats = toadd.map((event: FalconSignal) => {
                return {
                    x: event.signal.timestamp / 1000,
                    y: event.signal.signal[0],
                };
            });
            const thresholds = toadd.map((event: FalconSignal) => {
                return {
                    x: event.signal.timestamp / 1000,
                    y: event.signal.signal[1],
                };
            });

            if (this.chart.options.series[0].data.length > 500_000) {
                this.chart.dispose();
                this.chart = this.createChart(
                    this.chart.options.series[0].data.slice(300_000),
                    this.chart.options.series[1].data.slice(300_000),
                );
            }

            Array.prototype.push.apply(this.chart.options.series[0].data, stats);
            Array.prototype.push.apply(this.chart.options.series[1].data, thresholds);
            console.log(`Now plotting ${this.chart.options.series[0].data.length} data points in ${this.name}.`);

            this.chart.update();
        }
    }

    pushEventData(toadd: FalconEvent[]): void {
        if (this.chart && this.plot_event) {
            toadd.map((event: FalconEvent) => {
                console.log(`Now plotting ${event.signal.name} in ${event.signal.timestamp}.`);
                Array.prototype.push.apply(this.chart.options.plugins.events.data, [
                    { x: event.signal.timestamp / 1000, name: event.signal.name },
                ]);
            });
        }
    }

    // Called when this custom element is inserted into the DOM.
    connectedCallback() {
        this.name = this.getAttribute('name') || '';
        this.legends = (this.getAttribute('legends') || '').split(',');
        this.plot_event = this.getAttribute('plot_event') === 'true' || false;
        this.chart = this.createChart();

        chartdata[this.name] = this;
        console.log(`Registered plot: ${this.name}`);
    }

    createChart(stats?: { x: number; y: number }[], thresholds?: { x: number; y: number }[]): TimeChart {
        return new TimeChart(this.container, {
            realTime: true,
            series: [
                { name: this.legends[0] || '', color: 'darkGreen', data: stats || [] },
                { name: this.legends[1] || '', color: 'red', data: thresholds || [] },
            ],
            xRange: {
                min: 0,
                max: 10000,
            },
            xScaleType: scaleUtc,
            plugins: {
                lineChart,
                d3Axis,
                legend,
                crosshair,
                events: new EventsPlugin([]),
                zoom: new TimeChartZoomPlugin({
                    x: { autoRange: true, minDomainExtent: 10 },
                    y: { autoRange: true },
                }),
            },
        });
    }

    disconnectedCallback() {
        delete chartdata[this.name];
    }

    attributeChangedCallback(attr: string, oldVal: string, newVal: string) {
        if (attr === 'name') {
            if (oldVal in chartdata) {
                chartdata[newVal] = chartdata[oldVal];
                delete chartdata[oldVal];
            }
            this.name = newVal;
        } else {
            console.log('attr', attr, newVal);
        }
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'event-plot': EventPlot;
    }
}

customElements.define('event-plot', EventPlot);

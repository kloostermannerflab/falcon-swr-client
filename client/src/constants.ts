// Why did I choose port 51966 you ask? Excellent question. 51966_10 == CAFE_16 :)
export const FALCON_BRIDGE_URL = 'ws://localhost:51966/bridge/';

export const FALCON_BRIDGE_HEARTBEAT_TIMEOUT = 2000; // wait up to 2 seconds for a ping response
export const FALCON_BRIDGE_HEARTBEAT = 5000; // ping every 5 seconds
export const FALCON_BRIDGE_CONNECTION_TIMEOUT = 2000; // 2 seconds

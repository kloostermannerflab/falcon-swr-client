module FalconProcessor exposing
    ( FalconProcessor, FalconPort, FalconConnector, parseProcessor
    , parseConnector, saveConnector
    , State, StatePermission(..), StateValue(..), saveState, updateState, decodeState
    , OptionValue(..), encodeOption, updateOptions, decodeOption
    )

{-| Represent vertices and edges in a Falcon processing graph.

  - FalconProcessor as vertices, and
  - pairs of FalconConnection as edges

Unfortunately processor classes cannot be typed properly because
classes aren't constrained. Therefore they're represented as a
String for now.

This module also implements parsing of special-purpose notation
in Falcon graphs including the connections section.

@docs FalconProcessor, FalconPort, FalconConnector, parseProcessor


# Special notation parsing

@docs parseConnector, saveConnector


# Dynamic processor state

@docs State, StatePermission, StateValue, encodeState, saveState, updateState, decodeState


# Static processor options

@docs OptionValue, encodeOption, updateOptions, decodeOption

-}

import Basics.Extra exposing (flip)
import Dict exposing (Dict)
import List.Extra as ListE
import Maybe.Extra as MaybeE
import Parser
    exposing
        ( (|.)
        , (|=)
        , Parser
        , Step(..)
        , andThen
        , chompWhile
        , end
        , getChompedString
        , int
        , loop
        , oneOf
        , problem
        , run
        , spaces
        , succeed
        , symbol
        )
import Set
import Yaml.Decode as Decode
import Yaml.Encode as Encode


{-| A node in a Falcon processing graph.
-}
type alias FalconProcessor =
    { name : String -- unique name of a processor
    , class : String -- class name of a processor
    , ports : List FalconPort -- edges connect to these
    , options : Dict String OptionValue -- options attached to each processor
    , state : Dict String State -- state attached to each processor
    }


{-| A place for edges to connect to in a Falcon processing graph.
-}
type alias FalconPort =
    { name : String -- name of a port
    , slot : Maybe String -- a slot cannot be named unless a port is named
    }


{-| One end of an edge in a Falcon processing graph.
-}
type alias FalconConnector =
    { processor : String
    , prt : Maybe FalconPort -- a port does not need to be named
    }


type OptionValue
    = OptionBool Bool
    | OptionNum Float
    | OptionStr String
    | OptionList (List OptionValue)
    | OptionMap (Dict String OptionValue)


type StateValue
    = StateBool Bool
    | StateNum Float


type StatePermission
    = Read
    | Write
    | None


type alias State =
    { value : StateValue
    , permission : StatePermission
    }



-- SPECIAL NOTATION PARSING


{-| A helper type for parsing the special notation
-}
type ConnectorPart
    = Processor (List (Maybe String))
    | Port (List (Maybe String))
    | Slot (List (Maybe String))


{-| Parse an element of the "connections" section in a Falcon
processing graph description.

An example of a connection might be:

"processor1.port1.slot1=processor2.port2.slot2"
You can parse this with:

    parseConnector "processor1.port1.slot1"
    --> [{ processor = "processor1", prt = Just { name = "port1", slot = Just "slot1" } }]

A list is returned because a single connection endpoint written in
the special notation can "expand" out to multiple connections in the
graph. See: <https://falcon-core.readthedocs.io/en/latest/manual/graphs.html#data-stream-connections-between-processors>

-}
parseConnector : String -> List FalconConnector
parseConnector input =
    case run connectorParser input of
        Ok connector ->
            connector

        Err _ ->
            []


{-| Write the special notation for "connections"
-}
saveConnector : FalconConnector -> String
saveConnector connector =
    let
        processor : String
        processor =
            "f:" ++ connector.processor

        savePort : FalconPort -> String
        savePort prt =
            "p:" ++ prt.name
    in
    case connector.prt of
        Nothing ->
            processor

        Just prt ->
            case prt.slot of
                Nothing ->
                    String.join "." [ savePort prt, processor ]

                Just slotName ->
                    String.join "." [ "s:" ++ slotName, savePort prt, processor ]


parseProcessor : String -> List String
parseProcessor name =
    case run (extractPart |> andThen rangeNotation) name of
        Ok names ->
            List.filterMap identity names

        Err _ ->
            []


saveState : FalconProcessor -> String -> String
saveState processor stateName =
    Encode.toString 2 (encodeStateChange processor stateName)



-- HELPERS


updateState : String -> StateValue -> FalconProcessor -> FalconProcessor
updateState name value proc =
    { proc
        | state =
            Dict.update name
                (flip MaybeE.andMap <|
                    Just (\state -> { state | value = value })
                )
                proc.state
    }


updateOptions : String -> OptionValue -> FalconProcessor -> FalconProcessor
updateOptions name value proc =
    { proc
        | options =
            Dict.update name
                (flip MaybeE.andMap <|
                    Just (always value)
                )
                proc.options
    }


{-| Parser for connection endpoints.

There are 2 alternative syntaxes and a "range" syntax that
we need to be able to handle. Also, the "parts" of a connector
can occur in any order. Finally, multiple port parts and
multiple slot parts can be specified in a single connection
description using a embedded special notation. So multiple
connectors can be emitted from a single description.

-}
connectorParser : Parser (List FalconConnector)
connectorParser =
    let
        procParser : Parser ConnectorPart
        procParser =
            succeed Processor
                |. symbol ".f:"
                |= (extractPart |> andThen rangeNotation)

        portParser : Parser ConnectorPart
        portParser =
            succeed Port
                |. symbol ".p:"
                |= (extractPart |> andThen rangeNotation)

        slotParser : Parser ConnectorPart
        slotParser =
            succeed Slot
                |. symbol ".s:"
                |= (extractPart |> andThen rangeNotation)
    in
    succeed makeConnectors
        |= oneOf
            [ succeed Processor
                |. symbol "f:"
                |= (extractPart |> andThen rangeNotation)
            , succeed Port
                |. symbol "p:"
                |= (extractPart |> andThen rangeNotation)
            , succeed Slot
                |. symbol "s:"
                |= (extractPart |> andThen rangeNotation)
            , succeed Processor
                |= (extractPart |> andThen rangeNotation)
            ]
        |= oneOf
            [ procParser
            , portParser
            , slotParser
            , succeed Port
                |. symbol "."
                |= (extractPart |> andThen rangeNotation)
            , succeed (Port [ Nothing ])
            ]
        |= oneOf
            [ procParser
            , portParser
            , slotParser
            , succeed Slot
                |. symbol "."
                |= (extractPart |> andThen rangeNotation)
            , succeed (Slot [ Nothing ])
            ]
        |. end


makeConnectors : ConnectorPart -> ConnectorPart -> ConnectorPart -> List FalconConnector
makeConnectors a b c =
    let
        comparator : ConnectorPart -> Int
        comparator cp =
            case cp of
                Processor _ ->
                    0

                Port _ ->
                    1

                Slot _ ->
                    2

        triple : a -> b -> c -> ( a, b, c )
        triple x y z =
            ( x, y, z )

        sortConnectorParts : List ConnectorPart -> List ConnectorPart
        sortConnectorParts =
            List.sortBy comparator
    in
    case sortConnectorParts [ a, b, c ] of
        (Processor procs) :: (Port ports) :: (Slot slots) :: [] ->
            ListE.lift3 triple procs ports slots
                |> List.map makeConnector
                |> List.filterMap identity

        _ ->
            []


makeConnector : ( Maybe String, Maybe String, Maybe String ) -> Maybe FalconConnector
makeConnector ( proc, prt, slt ) =
    let
        makeConnectorIntrnl : String -> FalconConnector
        makeConnectorIntrnl procname =
            case prt of
                Nothing ->
                    FalconConnector procname Nothing

                Just name ->
                    FalconConnector procname (Just { name = name, slot = slt })
    in
    Maybe.map makeConnectorIntrnl proc


{-| Extract a "part" from the special notation.

A "part" is delimited by ".", as in e.g. "processor.port.slot".
The "parts" on this string are:

  - "processor"
  - "port"
  - "slot"

Valid characters in a "part" are strictly alphanumeric.

-}
extractPart : Parser String
extractPart =
    let
        nonempty : String -> Parser String
        nonempty s =
            if String.isEmpty s then
                problem "Name cannot be empty"

            else
                succeed s
    in
    chompWhile (\c -> Char.isAlphaNum c || (c == '_') || (c == '-'))
        |> getChompedString
        |> andThen nonempty


{-| Parser for the embedded "range" notation.

The special notation has an embedded "range" notation. Ranges are
delimited by parentheses. Valid values in a range are integers,
",", and "-".

See: <https://falcon-core.readthedocs.io/en/latest/manual/graphs.html#data-stream-connections-between-processors>

-}
rangeNotation : String -> Parser (List (Maybe String))
rangeNotation name =
    oneOf
        [ succeed (List.map (\n -> Just (name ++ String.fromInt n)))
            |. spaces
            |. symbol "("
            |. spaces
            |= loop [] rangeHelp
        , succeed [ Just name ]
        ]


rangeHelp : List Int -> Parser (Step (List Int) (List Int))
rangeHelp chunks =
    let
        contRange to =
            let
                from =
                    Maybe.withDefault to (List.head chunks)
            in
            Loop (List.append (List.reverse <| List.range (from + 1) to) chunks)
    in
    oneOf
        [ succeed (Done <| Set.toList <| Set.fromList chunks)
            |. symbol ")"
        , succeed (Loop chunks)
            |. symbol ","
        , succeed contRange
            |. symbol "-"
            |. spaces
            |= int
        , succeed (\val -> Loop (val :: chunks))
            |= int
        , succeed (Loop chunks)
            |. spaces
        ]


encodeStateChange : FalconProcessor -> String -> Encode.Encoder
encodeStateChange processor stateName =
    let
        state : Maybe State
        state =
            Dict.get stateName processor.state

        encodeStateRec : Encode.Encoder
        encodeStateRec =
            Encode.record
                [ ( stateName
                  , case state of
                        Just s ->
                            encodeState s.value

                        Nothing ->
                            Encode.null
                  )
                ]
    in
    Encode.record
        [ ( processor.name, encodeStateRec ) ]



-- ENCODING


encodeState : StateValue -> Encode.Encoder
encodeState value =
    case value of
        StateBool b ->
            Encode.bool b

        StateNum n ->
            Encode.float n


encodeOption : OptionValue -> Encode.Encoder
encodeOption value =
    case value of
        OptionBool b ->
            Encode.bool b

        OptionNum n ->
            Encode.float n

        OptionStr s ->
            Encode.string s

        OptionList l ->
            Encode.list encodeOption l

        OptionMap m ->
            Encode.dict identity encodeOption m



-- DECODING


{-| Decode a YAML state description into a `State`
-}
decodeState : Decode.Decoder State
decodeState =
    let
        permission : String -> StatePermission
        permission p =
            case p of
                "W" ->
                    Write

                "R" ->
                    Read

                _ ->
                    None
    in
    Decode.map2 State
        (Decode.field "value" <|
            Decode.oneOf
                [ Decode.map StateNum Decode.float
                , Decode.map StateBool Decode.bool
                ]
        )
        (Decode.field "permission" Decode.string
            |> Decode.map permission
        )


{-| Decode a YAML option description into a `OptionValue`
-}
decodeOption : Decode.Decoder OptionValue
decodeOption =
    Decode.oneOf
        [ Decode.float |> Decode.map OptionNum
        , Decode.bool |> Decode.map OptionBool
        , Decode.list (Decode.lazy (\_ -> decodeOption))
            |> Decode.map OptionList
        , Decode.dict (Decode.lazy (\_ -> decodeOption))
            |> Decode.map OptionMap
        , Decode.string |> Decode.map OptionStr
        ]

module LogView exposing (Log, LogLevel(..), LogMessage, Msg(..), decode, init, update, view)

{-| Display a table of log messages from Falcon
-}

import AssocList as Dict exposing (Dict)
import Colors.Opaque as C
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Json.Decode as Json
import Time exposing (toHour, toMinute, toSecond)


{-| Falcon defined log levels
-}
type LogLevel
    = DEBUG
    | INFO
    | STATE
    | EVENT
    | UPDATE
    | WARNING
    | ERROR
    | FATAL


{-| Structure of a log message from Falcon
-}
type alias LogMessage =
    { level : LogLevel
    , time : Time.Posix
    , message : String
    }


{-| Application models type to store messages.
Filtering is done dynamically in the view.
-}
type alias Log =
    { filter : List LogLevel
    , timezone : Time.Zone
    , messages : List LogMessage
    , maxMessages : Int
    }


{-| Log level display colours
-}
type alias LogColour =
    { foreground : Color
    , background : Color
    }


{-| TEA message type for this module
-}
type Msg
    = NewLogMessage LogMessage
    | LogFilterToggle LogLevel


{-| Decode a JSON serialised log message into a `LogMessage`.
-}
decode : Json.Decoder LogMessage
decode =
    let
        toLogLevel : String -> LogLevel
        toLogLevel ll =
            case ll of
                "DEBUG" ->
                    DEBUG

                "INFO" ->
                    INFO

                "STATE" ->
                    STATE

                "EVENT" ->
                    EVENT

                "UPDATE" ->
                    UPDATE

                "WARNING" ->
                    WARNING

                "ERROR" ->
                    ERROR

                "FATAL" ->
                    FATAL

                _ ->
                    ERROR
    in
    Json.map3 LogMessage
        (Json.field "level" Json.string |> Json.map toLogLevel)
        (Json.field "timestamp" Json.int |> Json.map Time.millisToPosix)
        (Json.field "message" Json.string)


{-| TEA: Initialise the model type

Initialise with the timezone of log messages and the maximum number to display

-}
init : Time.Zone -> Int -> Log
init tz max =
    { filter = [ DEBUG, INFO, STATE, EVENT, UPDATE, WARNING, ERROR, FATAL ]
    , timezone = tz
    , messages = []
    , maxMessages = max
    }


{-| TEA: update the model type
-}
update : Msg -> Log -> Log
update msg log =
    case msg of
        NewLogMessage message ->
            let
                messages =
                    if List.length log.messages >= log.maxMessages then
                        message :: List.take (log.maxMessages - 1) log.messages

                    else
                        message :: log.messages
            in
            { log | messages = messages }

        LogFilterToggle filter ->
            if List.member filter log.filter then
                { log | filter = List.filter (\v -> v /= filter) log.filter }

            else
                { log | filter = filter :: log.filter }


{-| Display checkboxes for log levels to filter messages
-}
logfilter : List LogLevel -> Element Msg
logfilter filters =
    let
        allFilters : List LogLevel
        allFilters =
            [ DEBUG, INFO, STATE, EVENT, UPDATE, WARNING, ERROR, FATAL ]
    in
    wrappedRow
        [ width fill ]
        (List.map
            (\l ->
                Input.checkbox
                    []
                    { onChange = always (LogFilterToggle l)
                    , icon = Input.defaultCheckbox
                    , checked = List.member l filters
                    , label = Input.labelRight [] (text (showLogLevel l))
                    }
            )
            allFilters
        )


{-| TEA: View the log messages table
-}
view : Log -> Element Msg
view log =
    column
        [ height fill
        , width fill
        ]
        [ logfilter log.filter
        , table
            [ alignTop
            , height fill
            , width fill
            , scrollbars
            , spacingXY 15 3
            ]
            { data = log.messages |> List.filter (\m -> List.member m.level log.filter)
            , columns =
                [ { header = el [ Font.bold ] (text "Level")
                  , width = fill
                  , view = labelWidget
                  }
                , { header = el [ Font.bold ] (text "Time")
                  , width = fill
                  , view = timeWidget log.timezone
                  }
                , { header = el [ Font.bold ] (text "Message")
                  , width = fill
                  , view =
                        \msg -> text msg.message
                  }
                ]
            }
        ]


{-| Mapping of log level to label colours
-}
colours : Dict LogLevel LogColour
colours =
    Dict.fromList
        [ ( DEBUG, LogColour C.grey C.white )
        , ( INFO, LogColour C.black C.white )
        , ( STATE, LogColour C.blue C.white )
        , ( EVENT, LogColour C.maroon C.white )
        , ( UPDATE, LogColour C.green C.white )
        , ( WARNING, LogColour C.orange C.white )
        , ( ERROR, LogColour C.red C.white )
        , ( FATAL, LogColour C.red C.yellow )
        ]


{-| Convert a log level into a string
-}
showLogLevel : LogLevel -> String
showLogLevel ll =
    case ll of
        DEBUG ->
            "DEBUG"

        INFO ->
            "INFO"

        STATE ->
            "STATE"

        EVENT ->
            "EVENT"

        UPDATE ->
            "UPDATE"

        WARNING ->
            "WARNING"

        ERROR ->
            "ERROR"

        FATAL ->
            "FATAL"


{-| Display a label for the log level.
Prototype at: <https://jsfiddle.net/q31mb08L/2/>
-}
labelWidget : LogMessage -> Element msg
labelWidget msg =
    let
        bg : Color
        bg =
            Dict.get msg.level colours
                |> Maybe.map .background
                |> Maybe.withDefault C.white

        fg : Color
        fg =
            Dict.get msg.level colours
                |> Maybe.map .foreground
                |> Maybe.withDefault C.black
    in
    el
        [ Background.color bg
        , Border.color fg
        , Border.width 3
        , Border.solid
        , Border.rounded 5
        , Font.color fg
        , Font.family [ Font.sansSerif ]
        ]
        (el [ centerX, centerY ] (text <| showLogLevel msg.level))


{-| Display the time that a log message was sent
-}
timeWidget : Time.Zone -> LogMessage -> Element msg
timeWidget tz msg =
    text
        (String.fromInt (toHour tz msg.time)
            ++ ":"
            ++ (String.padLeft 2 '0' <| String.fromInt (toMinute tz msg.time))
            ++ ":"
            ++ (String.padLeft 2 '0' <| String.fromInt (toSecond tz msg.time))
        )

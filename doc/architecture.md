# Architecture

This document describes the high-level architecture of falcon-swr-client.
If you want to familiarize yourself with the code base, you are in the right place!

## Bird's Eye View

![Birds eye view of the architecture](images/birds_eye.png)

On the highest level, falcon-swr-client is a thing that allows you to visually interact with the Falcon server.

Input data consists of YAML descriptions of Falcon graphs (see `client/src/FalconGraph.elm`) which are sent to Falcon.
Falcon also exposes streaming data as ripple events (see `client/src/client.ts::FalconSignal` and
`falcon-bridge/src/model.rs::FalconSignal`) and log messages
(see `client/src/LogView.elm::LogMessage` and `falcon-bridge/src/model.rs::FalconLog`). In this model, Falcon is the
single source of truth about the world.

The runtime dependency graph _always_ flows in a single direction. The Web GUI depends on the bridge. The Bridge
depends on Falcon. This is equally true internally to each of these sub-systems as you will see later on.

## Code map
This section talks briefly about various important directories and data structures.
Pay attention to the **Architecture Invariant** sections.
They often talk about things which are deliberately absent in the source code.

Note also which modules are **API Boundaries**.
Remember, [rules at the boundary are different](https://www.tedinski.com/2018/02/06/system-boundaries.html).

![High-level overview of the client architecture](images/high-level_software_design.png)

### `falcon-bridge/`

This is a "bridge" or "proxy" (from now on I will just call this sub-system the _Bridge_) between the
[zeromq](https://zeromq.org/) world of Falcon and the JSON world of the web GUI client.
It is written in [Rust](https://www.rust-lang.org/) and compiled using [Cargo](https://doc.rust-lang.org/cargo/). The implementation
of the bridge is inspired by and loosely based on [this blog post](https://blog.logrocket.com/how-to-build-a-websocket-server-with-rust/).

The Bridge runs the [Warp](https://lib.rs/crates/warp) webserver. All requests respond with the web client code _except_ for
the special `/bridge/` path which is used to connect a [WebSocket](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API).

There are 3 (actually more but that is out of scope for this document) threads that the Bridge runs:
1. The proxy thread that handles web requests and passes message back-and-forth between the Falcon connection and the web client.
   This thread performs I/O asynchronously with [Tokio](https://tokio.rs/).
1. A thread for sending requests to Falcon and waiting for responses. This handles timeouts and re-trying requests that fail.
1. A thread listening for streaming messages from Falcon.

**Architecture Invariant**: The bridge has a _build dependency_ on `client/` but at _runtime_ the only dependency is on Falcon.

### Server: `falcon-bridge/src/server.rs`

This module sets up the web-server and routes. It handles connections on the websocket route and uses `Client` to (de)serialise
messages. It passes all messages to `Proxy` over [Tokio channels](https://tokio.rs/tokio/tutorial/channels). It also waits on
a receive channel for messages from the `Proxy` and forwards them all directly to the WebSocket.

From the [Tokio documentation](https://docs.rs/tokio/1.5.0/tokio/sync/index.html):
> The most common form of synchronization in a Tokio program is message passing. Two tasks operate independently and send
> messages to each other to synchronize. Doing so has the advantage of avoiding shared state.
>
> Message passing is implemented using channels. A channel supports sending a message from one producer task to one or more
> consumer tasks.

Reading and writing to each websocket (1 websocket for each client) is modeled using a [`Stream`](https://rust-lang.github.io/async-book/05_streams/01_chapter.html).

**Architecture Invariant**: `Server` (actually `main`) is at the top of the internal dependency hierarchy within the Bridge. `Server`
is solely responsible for performing network I/O with clients.


### Proxy: `falcon-bridge/src/proxy.rs`

This module is the central point of communication between clients and Falcon. It tracks connected clients (subscribers to
a [broadcast channel](https://docs.rs/tokio/1.5.0/tokio/sync/broadcast/index.html)) and spawns the threads to communicate with
Falcon.

The `Proxy` shares a data structure (`falcon-bridge/src/falcon.rs::FalconClient`) with the Falcon communication threads for out-of band communication such as connection
address and termination flag.


### `client/`
This is the client GUI that runs in a web browser. It is logically seperated into 3 major
parts:

  1. Application initialisation and raw WebSocket handling.
  1. Plotting.
  1. GUI application.
    
Everything that can be is written in [Elm](https://elm-lang.org/), a statically typed functional programming language that compiles to JavaScript. Application initialisation (`client/src/index.ts`), raw WebSocket connection handling (`client/src/client.ts`), and plotting (`client/src/plto.ts`; using the [TimeChart](https://huww98.github.io/TimeChart/) library) is written in [Typescript](https://www.typescriptlang.org/), a "typed superset of JavaScript". The _TimeChart_ plot itself is wrapped in a custom component and used from Elm.

**Architecture Invariant**: As much static checking as possible is used to ensure the application is as reliable as possible. If a task can be achieved in Elm then it should be done there, only then should the task be completed in Typescript.

**Architecture Invariant**: The Elm/Typescript boundary is an API boundary. There is no static checking at this boundary. Changing only one side or making incorrect (non-matching) changes will result in runtime issues (decoding error notifications in Elm or runtime errors in Typescript).

### WebSocket connection handling: `client/src/client.ts`
This module opens, monitors connection health/status, reads incoming messages, and buffers
outgoing messages to a WebSocket connection to the Bridge.

  * If the connection fails, attempts to re-establish connection.
  * Sends heartbeat (`ping`/`pong`) messages to the Bridge to monitor connection health.
  * Relays incoming messages to the proper destination (GUI or plot).
  * Forwards messages from the GUI to the WebSocket. If the WebSocket is not ready to send
  messages then they're buffered until the WebSocket is ready.
  
### `Main: client/src/Main.elm`
This module is responsible for displaying the overall UI and decoding messages from
Falcon.

**Architecture Invariant**: Almost everything is written in the `Main` module. Unless
manipulating data becomes difficult or names begin to clash. See the talk
[Life of a File](https://youtu.be/XpDsk374LDE) by
[Evan Czaplicki](https://github.com/evancz).

### Graph: `client/src/FalconGraph.elm`
This module is entirely responsible for dealing with YAML graphs:
  * Encoding
  * Decoding
  * Display
  * Interactivity
  * Adjusting options and states

## Cross-Cutting Concerns
This sections talks about the things which are everywhere and nowhere in particular.

### Testing
Falcon-swr-client has three interesting [system boundaries](https://www.tedinski.com/2018/04/10/making-tests-a-positive-influence-on-design.html) to concentrate tests on.

The outermost boundary is the `FalconGraph.elm` module.

The middle boundary is the interface between Elm and Typescript in the browser.

The innermost boundary is the Bridge where it processes messages from the GUI.

### Observability
Falcon-swr-client is a long-running process, so its important to understand what's going on inside.
We have 2 instruments for that.
  * The browser console for the GUI, and
  * log messages from the Bridge printed to `stdout`.

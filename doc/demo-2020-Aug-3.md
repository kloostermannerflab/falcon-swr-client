# Demo of the Falcon Client progress

## Agenda
Presenting  prototype.

1. Start the bridge before video call and refresh page.
2. Beginning of the demo, share screen to the browser (Bridge should show connected).
3. Load the ripple detection graph using the file selector
4. Demonstrate re-organising the graph and talk about controls for options
5. Show the terminal and start `falcon` and `nlxtestbench`
6. Show leg messages and what could be done with those (filtering, colouring etc as in the existing
   client)
7. Press the "PLAY" button in the client and watch in awe as data starts pouring in.

## Challenges
1. Data volume. SVG cannot handle the required volume. Displaying on canvas allows easy saving
   in bitmap form. Can also trivially zoom and pan the displayed area (potentially in a seperate
   plot).
2. Can use browser local storage mechanism to store session data offline. Whole app does not need
   an internet connection. (if hosted somewhere an internet connection will be needed to
   download the app one-time only).
3. Graph display is limited and messy. Trivial to clean up and add parameter adjustment controls.
4. Downsampling for display (currently min/max downsampling to 10 points per second). Can do more but
   have to balance with display performance.
   
## Review notes
- Amount of times into the past plotted should be user settable to a fixed number of seconds.
- Fred wants a copy of Jane's display settings (whats parameters can be modified on each node).
  * Share a config file
  * Ad-hoc networking
  * Central repository of configurations (app config repo)

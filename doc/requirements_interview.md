# Falcon UI Requirements interview

## Project outline

* Of the 2 parts to this project (ripple detection GUI, and graph creation GUI), which is more important?
> Ripple detection first (focus on this).
> Also render the graph based on the YAML destription.
>
> Having an e.g. Python library being able to specify
> and e.g. N number of inputs specific to an experimental setup.
> Could also be a temple directly in the YAML. Python would
> be another mechanism for templating (automatic templating).
> 
> Maybe 2-3 templates and user just takes parameters that then
> creates the YAML to send to Falcon. Marine will create some
> templates and list parameters that are important (struture, 
> node parameters for initialisation and for changes at run-time).

* Would you like to be involved in code review?
> Marine is interested. And Fabian (will look). Happy to review PRs.
* Should this be open source? Select copyright license?
> GPLv3+
* Should the software be published in a package index?
> Yes.

## User info

* What level of knowledge do you expect a user to have?
    * Programming knowledge
    * ability to read and understand log messages
    * Would they need to know the Falcon server IP address?
> Python is often fine.
* How does a user currently, "manually load a graph into Falcon"?
> Notes:
* How to you envision the user starting the GUI? e.g. Double-click an icon on the desktop, visit a web page,...
> At the moment, users start server from the command line
> (a graph can be accepted as a parameter or a config file)
> 
> Experiment might run with several animals. Each animal
> has a YAML graph. You load a graph run the experiment then
> load the next graph and run the experiment in the next
> animal.
* There are 3 main sources of visual information:
    * plot of data and overlay of (ripple) events against time
    * log messages
    * execution graph (rendered from YAML input)
    * ...?
> During the experiment youre not looking at the log messages
> (unless something is wrong).
> Better way to see how many ripples:
> * discarded
> * missed
> * accepted
> Might be useful to mark in the plot ones that are discarded,
> accepted, etc also timestamps.
* Is it useful for a user to be able to see any combination of these in a single window? Which combinations? Should this be user configurable?
> Notes:
* Graphs are changed how?
> By copy paste. Tweaking parameters. Copy paste e.g. input
> nodes.

## Features

* What's the most important feature? How do other features rank in importance?
> * Lucasz GUI design was good.
> 1. Display plot + threshold (see example screenshot)
> 1. Send a graph to Falcon + start/stop processing
> 1. Modify static parameters (based on templates)
> 1. Modify run-time parameters
* Should a user doing ripple detection be able to adjust parameters?
> Yes only runtime parameters. "Green ones" in the screenshot.
> * threshold for cortex and hippocampus
> * smoothing
> * options tree -> ???
> * lockout (shared state); rarely changed
> * detection lockout; rarely changed
> Some of these should be changed togehter
* Might it be useful to store parameter states so that they can be re-used later?
> Need to think about this...
* The "detection threshold" parameter has a range of valid values? Are these discoverable? What are they?
> No validation in Falcon itself. Fabian needs to think about whether
> this should be in Falcon.
> Threshold: Non-negative values should not be allowed. no upper-bound.
> Smoothing: positive (unit is milliseconds).
> Lockout: always positive (time unit milliseconds) no upper-bound (maybe warn when outside recommended values).
> Could be colour coded.
> 
> Can ask Falcon for the "active" YAML
* Are the togglable "output triggers" discoverable? Is there a list somewhere?
> Notes:
* What types of plot do you expect to need/want?
> * Plot of events and thresholds
> * Plot of raw detected events within ~100ms
> * Lucasz design has red (raw signal; not from the green)
>   for a period around a detection.
> * Experimental interaction (disruption) want to see the 
> effect in 2 streams of data (raw stream and processed stream)
> within a time range (~ hundreds milliseconds around disruption)
> don't necessarily want to see the initial ripple event.
> Also averaging of data around several disruption events as
> defined by maybe around a minute.
* Plots against time should be able to "look back" through the time axis? e.g. scroll or zoom
> Scrolling and zooming would be useful.
* Do you expect a permanent record of data (events, ...?) to be useful?
> No. This client is not involved in recording (that's done in the
> aquisition system).
> Maybe it's useful to save some event statistics.
> Falcon saves everything in a general way. It might be useful
> to access and filter some data from the client.
* How real-time does sensor data (and events) have to be?
> Display as quickly as possible (even seconds). This is feedback
> from setting e.g. a threshold. Or a physical movement of the animal.
> Or if there's something wrong in the signal.
* Are there any nice-to-have features that haven't been mentioned yet?
> * Set time window around ripple detection (maybe also displayed in a zoomed in version). This should be automatic (maybe last N detections)
> * Saves threshold parameters for use later. When you come back > tomorrow you don't have to type them in again. This is also
> useful to distribute or publish.
> * Filter display of markers {invalid detections, accepted, etc}


## Existing software and docs

* Are the Falcon server 0MQ API endpoints documented? Where?
> Exists. Not yet online (from Marine); Marine will generate a PDF for us.
* Is the processing graph format documented?
> Mostly from the paper. Mostly examples.
* Are there some test examples of valid YAML graphs?
> Notes:
* There is an existing Python app for ripple detection:
    * What are it's limitations?
    * Is it a useful starting point?
> Python client has 2 connections: 1 cmd/reply, 2 logs are
> posted without a receiver.
* Is the Falcon easy to set up in a test environment? Is there one running that I can use?
* Is there test data available that I can replay over 0MQ?
> Notes:
 
## Possible solutions

* Do you like the GUI design that Lucasz made? Comments?
> Yes
* Are the "START"/"STOP" buttons in Lucasz' design useful?
> Notes:
* Do you have any suggestions?
    * UI design
    * software architecture
    * ...?
> Marine is fine with thoughts so far.
* Are you aware of any potential pain points for implementation?
> This is for Fabian.

## Final result

* Timeline or deadline?
> July 2020 for a prototype.
* How would you like to evaluate:
    * requirements document
    * software design
    * prototype
    * final solution
> Everything.
* Do you expect a publication from this work?
> Will be mentioned in future publications (not necessarily a stand along pub; maybe a methods paper).
* Maintenance
> Question for Fabian. VIB biocore helps in maintenance?
> Marine probably will help in the future.

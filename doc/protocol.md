Hi @marine chaput ,

I will try to write out my understanding and thoughts from the discussion.

The problem:
It is sometimes necessary to interpret or structure/shape the data sent by Falcon differently. For example, timestamps (that have the same shape: uint64_t) may be interpreted as microseconds since the beginning or the experiment, as seconds since the epoch (unix time). Or the data may be structured differently as in the case of filtered ripple data:

struct ripple_event {
  uint64_t timestamp;
  float signal;
  float threshold;
}
In contrast to e.g. the output of an EventFilter used for debugging which would look like this:

struct event_data {
  char event[128];
};
‌

Prior art:
Adding type information to real time network streams is not a new problem.

RTMP or Real-Time Messaging Protocol defines a packet structure with a header containing a tag called Message Type ID which is a single byte which, among other things, defines whether the packet contains audio or video data.

Flatbuffers and other popular (de)serialisation methods often define a schema and allow a receiving party to accurately decode individual messages.

Solutions:
As I understood there are 4 ideas:

1. Bind unique data formats and interpretations to sockets. 1 socket for every unique format and interpretation.
This is truly inflexible because it requires the client to know in advance to open a fixed set of sockets. If a data format is added the client needs to open yet another socket. The overhead of adding sockets is potentially very high. If they’re listen on separately that’s a new thread to manage. If they’re added to an existing polling group then the streams are combined anyway. I do not see any advantages of this method.

2. Bind unique data formats and interpretations to processor slots.
I don’t understand this method. Is it meant as a means of implementing 4?

3. Mark changes in format or interpretation in the data stream. Every time the format or interpretation should change a marker is sent first.
This is efficient and can be implemented in at least 2 ways: 1) protocol switches can be inserted into the data stream as necessary. For example, if we have 3 different ways of interpreting or structuring the data: A, B, and C. And a marker M to switch between them, the data stream might look like this:

... M A A A A A M C C M A A A A A A A M B M A A A A A ...
--- time --->
or 2) data can be spooled (up to some maximum) and sent together with a protocol marker + amount of data. Here the data stream might look like this:

... [(M, 5) A A A A A] [(M, 2) C C] [(M, 7) A A A A A A A] [(M, 1) B] [(M, 5) A A A A A] ...
--- time --->
4. Mark each datum sent with its format and how it should be interpreted (this may be the same as 2 I don’t really understand 2). This would mean that each datum contains a header which could just be a number that uniquely marks how the following bytes should be interpreted. e.g.

template <typename Payload>
struct output_packet {
  uint8_t header;
  Payload payload;
};
Then each possible interpretation or data shape can be statically assigned a constant number identifier. e.g. https://replit.com/talk/share/A-working-example-of-option-4/136128

5. As pointed out by @marine chaput . Use an existing well supported serialisation format such as flatbuffers, or one of the many alternatives. Falcon can define a schema and clients can use that schema to generate message decoders. This option is equivalent to option 4 above but using a standard/popular serialisation format. Message size is also likely to be larger due to message element type tagging etc. It is worth looking at some benchmarks:
* https://github.com/thekvs/cpp-serializers
* https://blog.logrocket.com/rust-serialization-whats-ready-for-production-today/
* https://aloiskraus.wordpress.com/2017/04/23/the-definitive-serialization-performance-guide/
* https://github.com/chronoxor/CppSerialization

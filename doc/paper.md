---
title: 'Falcon-Ripple-Client: A Graphical Ripple-detection Client for Falcon'
tags:
  - Falcon
  - closed-loop neuroscience
  - graphics
  - low-latency
  - Elm
  - Rust
  - WebSocket
  - ZeroMQ
  - WebGL
authors:
  - name: James H. Collier
    orcid: 0000-0002-0020-421X
    affiliation: 1
  - name: Marine Chaput
    orcid: 0000-0002-6046-7863
    affiliation: "2, 5"
  - name: Alexander Botzki
    orcid: 0000-0001-6691-4233
    affiliation: 1
  - name: Fabian Kloosterman^[corresponding author]
    orcid: 0000-0001-6680-9660
    affiliation: "2, 3, 4, 5"
affiliations:
 - name: VIB Bioinformatics Core, Ghent, Belgium.
   index: 1
 - name: Neuroelectronics Research Flanders, Leuven, Belgium.
   index: 2
 - name: VIB, Leuven, Belgium.
   index: 3
 - name: Brain and Cognition, KU Leuven, Leuven, Belgium.
   index: 4
 - name: IMEC, Leuven, Belgium.
   index: 5
date: 28 Oct 2021
bibliography: paper.bib
---

# Summary
This application, `falcon-ripple-client`, is a graphical client for Falcon [@falcon]. Falcon provides real-time processing of neural signals to enable low-latency closed-loop feedback in experiments that aim to causally link neural activity to behaviour. Falcon was built as a generic tool, on top of which specific client applications can be built for the real-time analysis and detection of signals. 

`falcon-ripple-client` is used to detect and investigate the role of sharp-wave ripple oscillations and their links to other neural activity in the brain [@ripple]. It provides a web-browser based graphical interface to load and edit the real-time processing workflow, execute that workflow with Falcon, and display experiment results on interactive time-series plots.

![A screen recording of the running ripple detection client. The workflow being executed by Falcon is displayed in the left panel. Signal outputs from Falcon are displayed in the center panel. Log messages from Falcon are displayed in the right panel.\label{fig:screenshot}](https://bitbucket.org/kloostermannerflab/falcon-swr-client/raw/65c7939a98c433a3605651baa6353ea1f78bed14/doc/images/demo.gif)

A screen recording of the running client can be found in \autoref{fig:screenshot}. The left pane displays the real-time processing workflow represented as a graph of nodes. While the workflow is being executed by Falcon, the user is able to adjust node parameters through this interface. The graph layout is computed automatically and also may be adjusted by the user to their liking at any time. The workflow and log panes may be hidden by the user so they can focus on the plot of the experiment output. The plot is displayed in WebGL using the Timechart [@timechart] library.

# Statement of Need
Natively, Falcon provides only limited interactivity. However, among the extension capabilities of Falcon is the provision of a Remote Procedure Call (RPC) interface exposed over ZeroMQ [@zeromq]. This interface can be used to build rich graphical clients such as this one.

This client provides a convenient graphical interface to run experiments with Falcon, edit experimental parameters, and to view and interact with the experiment output. While a very simple client [currently exists](https://bitbucket.org/kloostermannerflab/falcon-client) it provides minimal features (only a non-interactive plot) and does not perform as well as the `falcon-ripple-client` described here.

# Acknowledgements
We acknowledge contributions of Lies Deceuninck who gave very useful contributions to this manuscript and excellent feedback as an end-user. We are also grateful to Frank Vernaillen who provided useful advice about the software architecture and design.

# References

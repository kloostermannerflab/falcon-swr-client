# Falcon UI Design Document
***

## Context
The Falcon server software ([source](),
[publication](http://iopscience.iop.org/article/10.1088/1741-2552/aa7526)) is connected
to sensor hardware as part of an experimental setup.
The server is expected to run on the same local network (if not the same machine) as the
GUI. The GUI has a [ZeroMQ](https://zeromq.org/) TCP pub/sub connection with the server. It is concievable that
multiple different GUI implementations tailored to different purposes could exist in the future.

![Falcon GUI client context](images/context.png)

The Falcon server executes a "graph" with outputs including files and ZeroMQ.
Above is a diagram displaying
the experimental setup where the falcon server receives sensor data from a single animal, it is
publishing statistics data to ZeroMQ to be displayed on the experimenters laptop.

## Goals
* Display real-time sensor data (and overlay ripple evnts)
* Load existing graphs into the Falcon server.
* Display messages from Falcon
* Adjust ripple detection parameters based on the execution graph

## Non-Goals
* Will not provide a mechanism to create Falcon execution graphs.

## Milestones
* 2020-Aug-1: 1st GUI prototype for ripple detection

## Stakeholders
1. Experimenter: a user of this visualisation tool, the person running an experiment.
1. Developer: the person improving and maintaining the software.
1. PI: The Principal Investigator.
1. Manager: Person responsible for overseeing all experiments and software installations.

## Functional Requirements

### User story 1:
As an experimenter, I want to be able to see a real-time plot of ripple events that
   breach a threshold that I set. This is so that I can monitor the progress of the experiment,
   notice if there are any problems, and investigate points of interest.

| F-1-1 | Plot events from Falcon |
|:------|:------------------------|
| Description | A plot of real-time events  is central to the utility of the application. It should be prominent and easy to return to if the user navigates away. Data displayed in this plot should include individual events and the threshold. x-axis should be time, y-axis should be signal level (in microVolts). |
| References | None |


| F-1-2 | Ripple event markers |
|:------|:---------------------|
| Description | Events that breach the threshold should be _marked_ and "clickable". When the experimenter clicks on a marker the statistics around the clicked event should be displayed nearby. |
| References | None |


| F-1-3 | Zooming and panning |
|:------|:--------------------|
| Description | The time axis (x-axis) should be zoomable by selecting a region of time that the experimenter wishes to examine. The time axis should also be "draggable" such that data outside the visible region can be examined (panning). |
| References | None |


| F-1-5 | Filter events displayed in the plot |
|:------|:------------------------------------|
| Description | The experimenter may wish to dispay markers on all events, only accepted events, only invalid detections, or no markers. A filtering option should be available close to the main statistics plot. |
| References | None |


| F-1-6 | The plot should be no longer than 1 second behind real-time |
|:------|:------------------------------------------------------------|
| Description | It is important that the experimenter be able to observe event in the plot _as they happen_ in real-time. Therefore the delay between event and display should be no longer than 1 second. |
| References | None |

### User story 2:
As an experimenter, I want to be able to verify that my experiment is set up correctly and software and connections are functioning as expected. This is so that I can identify and correct any mistakes as early as possible.

| F-2-1 | Display log messages |
|:------|:---------------------|
| Description | Log messages should be indirectly accessible (a single mouse or button press; hidden by default) through a resizable window or panel. Log messages should be highlighted and filterable to draw user attention to what they're looking for.
| References | None |

### User story 3:
As an experimenter, I want to be able to "load" a YAML format description of an experiment form the filesystem so that it can be submitted to the Falcon server and so that I can adjust parameters.

| F-3-1       | Load a YAML file from the filesystem                                    |
|:------------|:------------------------------------------------------------------------|
| Description | The user should be able to select a YAML file from the local filesystem |
| References  | None                                                                    |
|             |                                                                         |


| F-3-2       | Extract parameters from loaded YAML files                           |
|:------------|:--------------------------------------------------------------------|
| Description | Loaded YAML files should be parsed to extract adjustable parameters |
| References  | None                                                                |


| F-3-3 | Submit YAML file to the Falcon server |
|:------|:--------------------------------------|
| Description | "Loading" and "submission" should be two events controlled by the experimenter. Once parameters have been set by the user, they should be able to submit the YAML description to the Falcon server |
| References | None |

### User story 4:
As an experimenter, I want to be able to run an experiment with identical parameter values on consecutive animals.

| F-4-1       | Store parameter values for later re-use                                    |
|:------------|:---------------------------------------------------------------------------|
| Description | Store user adjusted parameters so that they can be loaded on user request. |
| References  | None                                                                       |


## Non-functional Requirements

### Availability

| NF-1        | Copyright license                                      |
|:------------|:-------------------------------------------------------|
| Description | [GPLv3+](https://www.gnu.org/licenses/gpl-3.0.en.html) |
| References  | None                                                   |


| NF-2        | Publish on software package indexes                                 |
|:------------|:--------------------------------------------------------------------|
| Description | This software will be published on relevent software ackage indexes |
| References  | None                                                                |

### Performance

| NF-3        | Event data received from the Falcon server should be displayable immediately |
|:------------|:-----------------------------------------------------------------------------|
| Description | Event display should be as fast as possible (within 1 second).               |
| References  | [F-1-6](#markdown-header-user-story-1)                                       |


| NF-4 | Should run on a low spec tablet computer. |
|:-----|:------------------------------------------|
| Description | Computing resources required to plot real-time data are minimal. This should translate to the ability to run the software on low performance devices such as tablets |
| References | None |

### Quality

| NF-5 | Documentation |
|:-----|:--------------|
| Description | User documentation should be extensive. Reference material and tutorials should be made available. Programmers documentation should be available for every module and function, this should be supplemented by doctests where applicable. |
| References | None |


| NF-6        | Unit test coverage                                            |
|:------------|:--------------------------------------------------------------|
| Description | No less than 90% unit test coverage. This should be enforced. |
| References  | None                                                          |


| NF-7 | Code formatting |
|:-----|:----------------|
| Description | An automatic code formatting tool should be used as applicable. This should be enforced. |
| References | None |


| NF-8 | Linting |
|:-----|:--------|
| Description | Strict static analysis should pass with no errors or warnings on _every_ commit to master. This should be enforced. |
| References | None |


| NF-9 | Continuous integration |
|:-----|:-----------------------|
| Description | A continuous integration tool should be used to enforce NF-6 - NF-8. This should also be used to perform end-to-end and integration tests. |
| References | None |

## Constraints

1. Must run on a reasonable laptop computer running one of these environments:
    * Windows 10
    * GNU/Linux
    * OSX

1. Must be able to interface with these technologies:
    * ZeroMQ
    * YAML

## Existing solution
![Current Falcon ripple-detection client](images/current_falcon_client_main.png)

![Current Falcon client plot](images/current_falcon_client_plot.png)

This is a Python app in PyQt that researchers are using daily.

## Proposed Design

## Alternative Designs

1. Native binary with GUI in a cross-platform toolkit. ZeroMQ communication handled directly.
   Potential GUI toolkits:
       * [Qt](https://qt.io) Major release [due end-of-year](https://www.qt.io/blog/qt-roadmap-for-2020). [Plotting](https://doc.qt.io/qt-5/qtcharts-index.html)
       * [GTK+](https://gtk.org) Major release due ["soon"](https://wiki.gnome.org/Projects/GTK/Roadmap/GTK4). [Plotting](https://developer.gnome.org/gtk3/stable/GtkDrawingArea.html).
       * ~~[WxWidgets](https://www.wxwidgets.org/)~~. No active plotting library. Closest is [wxCharts](https://github.com/wxIshiko/wxCharts).
       * [Avalonia](https://avaloniaui.net/). .NET platform. [Plotting](https://oxyplot.github.io/).
       * ~~[Flutter](https://flutter.dev)~~. Developed by Google. [Desktop](https://flutter.dev/desktop) is experimental and excludes Windows.
       * [Azul](https://azul.rs/). Rust. [Immediate mode](https://caseymuratori.com/blog_0001).
       * [Dear ImGui](https://github.com/ocornut/imgui). Bindings to many languages. [Immediate mode](https://caseymuratori.com/blog_0001).
         [Intro](https://blog.conan.io/2019/06/26/An-introduction-to-the-Dear-ImGui-library.html).

1. Native binary with GUI in a cross-platform toolkit. ZeroMQ communication handled directly. Plotting in a webview.

1. ~~Native binary with platform specific GUIs.~~ Maintenance nightmare.

1. ~~ZeroMQ handling in WebAssembly with libzmq~~. Communicates with the GUI running in the same browser. This
   isn't currently possible because libzmq doesn't compile to WASM (See [these](https://github.com/emscripten-core/emscripten/issues/5726)
   [bugs](https://github.com/zeromq/libzmq/issues/3437)).
   
1. ZeroMQ handled with a native binary proxying over a websocket with a custom protocol.
   The native binary could launch the GUI running in the browser.

1. ZeroMQ handled in a native binary launched by the browser using a WebExtension. Communication
   between the GUI and the ZeroMQ handler done through the [`sendNativeMessage`](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/runtime/sendNativeMessage)
   [WebExtension](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Native_messaging) [API](https://developer.chrome.com/apps/nativeMessaging).

1. ~~ZeroMQ communication directly over a WebSocket from a Browser GUI~~. Not possible, [ZWSSock](https://github.com/zeromq/zwssock) project abandoned.

1. Electron. Proxy ZeroMQ in [Node](https://github.com/zeromq/zeromq.js/). WebSocket connection to GUI for real-time plotting.
   Advantage: can be distributed as a bundled binary. Appears like a native application. Disadvantage: resource hog, runs an instance of Node + Chromium, also Thomas noted this caused significant problems.

## Open Questions
1. What types of plot do you expect to need/want?
1. What limitations does the current plotting application have?
1. How can the design/prototype/final solution be evaluated? When is it complete?

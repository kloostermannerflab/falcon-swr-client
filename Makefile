# This Makefile orchestrates the building of the web client, the bridge, and
# finally, the combination.
#

.PHONY: clean all debug

all: falcon-swr-client bridge-docs.txz

debug: client/dist/index.html
	cp client/dist/index.html falcon-bridge/src/index.html
	cd falcon-bridge && cargo build
	cp falcon-bridge/target/debug/falcon-bridge falcon-swr-client

client/dist/index.html: client/src/client.ts client/src/constants.ts client/src/FalconControl.elm client/src/FalconGraph.elm client/src/FalconProcessor.elm client/src/index.ts client/src/Main.elm client/src/plot.ts client/src/Notifications.elm
	cd client && npm ci && npm run build

falcon-swr-client: falcon-bridge/src/client.rs falcon-bridge/src/error.rs falcon-bridge/src/falcon.rs falcon-bridge/src/main.rs falcon-bridge/src/model.rs falcon-bridge/src/proxy.rs falcon-bridge/src/server.rs client/dist/index.html
	cp client/dist/index.html falcon-bridge/src/index.html
	cd falcon-bridge && cargo build --release && strip target/release/falcon-bridge
	cp falcon-bridge/target/release/falcon-bridge falcon-swr-client

bridge-docs.txz:
	cd falcon-bridge && cargo doc
	tar -C falcon-bridge/target/ -cJf bridge-docs.txz doc/

clean:
	rm -rf client/dist/* falcon-bridge/target/ falcon-swr-client bridge-docs.txz

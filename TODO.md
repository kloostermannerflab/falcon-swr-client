# TODO Items

## Proof of concept [Demo 3-Aug-2020]

- [x] Display ripple plot data
- [x] Basic web UI

## Alpha 1 [Demo 15-Apr-2021]

- [x] launch client by running a single program (./falcon-client; or double-clicking an icon)
- [x] Interactive connection/disconnection/reconnection to/from a running Falcon server instance
    - [x] Bridge should not automatically try to connect to Falcon
- [x] Load a YAML graph from Falcon and editing options
    - [x] Correctly parse the YAML
    - [x] Send the Graph to the server (["graph", "build", <YAML>])
    - [x] Parse options from the server
    - [x] Display options UI
- [x] Start and stop graph processing using the loaded graph
- [x] Scrolling and zooming the main plot
- [x] Add "save" button to the graph display
- [x] Add "reset view" button to the graph display
- [x] Make text in graph nodes invisible to mouse events
- [x] Change graph node background colour to something less alarming

## Alpha 2 (22-Apr)
- [x] Release build does not display the client
- [x] Graph view resizing is unreliable
- [x] Better error handling in the bridge (lock up when falcon crashes, no abort(),)
- [x] Make the log messages more pretty and filterable
- [x] Refresh view on plot deletes all data. This should be another button.
- [x] Real-time scroll on the plot breaks when user pans plot.
- [x] MINOR: Make the resources list prettier (keep only graph:// is fine)
- [x] Developer documentation
  - [x] How to build with and without docker
  - [x] Module programmers documentation
  - [x] architecture.md
- [x] Remove bridge downscaling and improve plot performance

## Beta 1 (6-May)
- [x] Modify static options before building the graph (rebuild when user clicks START)
- [x] Track graph state
- [x] Graph nodes should have a "collapse" button that displays only node name/type and expands
      to a scrollable list of all options.
- [x] Display Falcon server version and client version
- [x] Show hints for key combinations to interact with the plot
- [x] Better falcon connection failure feedback
- [x] Fix hang in bridge when we cannot send a message to a zmq socket
- [x] Initial size for the graph display is hardcoded
- [x] Fix Bridge connection status display

## Beta 2 (1-Jun)
- [ ] Add in interactive markers for (filterd | dropped | stimulation) events that can be clicked and zoomed. Maybe in a
      seperate plot. These markers should be filterable.
- [ ] Display counters for filterd | dropped | stimulation events. Can these be saved (as a file).
- [x] JOSS draft paper.
- [ ] Design the streaming data format to be more flexible and adapt client.
  - (time since exp start, signal, thresh)
  - (abs timestamp, ...)
  - (debug info, ...)
  - (timestamp, ripple detection ignored | filtered | ... )
- [ ] Tests
- [ ] Fix immediate validation on options entires for array and object type data
- [ ] Move "Load local YAML" button to the top of the pane
- [ ] stimulation-trigger/detection-only-mode checkbox has no effect
- [ ] ttl-output has no options, should have "port address"
- [ ] Mark the "States" tab if there are states to change
- [ ] Purge data from plot after ~1 million data points
- [ ] Pretty SVG icons instead of unicode icons

## Nice to have
- [-] Windows build of the client [WONTFIX; too difficult to cross compile a windows binary with libzmq]
- [ ] Display favicon








![Falcon GUI Client Screenshot](doc/images/screenshot.png)

# Falcon GUI Client

A GUI for _Ripple Detection_ using the [Falcon](https://bitbucket.org/kloostermannerflab/falcon-core) real-time, closed-loop neuroscience software.

## Features
* Display ripple events and plot data from Falcon.
* Load and save graphs.
* Configure settings and runtime options for a graph.
* Interact with real-time data from Falcon (pan and zoom the plot).
* Display log messages from Falcon.

## Why
The Falcon server does not provide a GUi interface. This client provides such a GUI tailored for ripple detection. The technologies used
are [Rust](https://www.rust-lang.org/) for its safety, integration with [zmq](https://zeromq.org/), and ability to build a statically linked binary on multiple platform; and [Elm](https://elm-lang.org/) for its safety and ergonomics for building reliable web-based user interfaces.

## Installation
Place the `falcon-swr-client` binary anywhere you have access to and ensure that it is executable.

## Building

### With Docker
This method is useful for building a release binary without installing and setting up all of the necessary dependencies. It is not useful for development.

A docker configuration exists in the supplied `docker-compose.yml` so you only need to install:

* [Docker](https://docs.docker.com/get-docker/)
* [Compose](https://docs.docker.com/compose/install/)

On Debian based GNU/Linux distributions you can install the `docker.io` package to get Docker.

Once you have installed Docker you can simply run `docker-compose up --build` to build the `falcon-swr-client` binary in the release configuration. To build in the debug configuration run `TARGET=debug docker-compose up --build`.

### Without Docker
This method is useful for development or building a release binary.

Prerequisites are:

* [Nodejs](https://nodejs.org/) ([install](https://nodejs.org/en/download/); **Warning**: the `nodejs` **and** `npm` packages from Debian are **too old** and will not work correctly. By default `npm` will install to (`npm root -g`) `/usr/local/` on Debian. This can (should; otherwise you will need to build as root) be adjusted to a user controlled directory (such as `~/.npm`). This can be done with `npm config set prefix '~/.npm'`. Then add `~/.npm/bin` to your `$PATH`.)
* [Elm](https://elm-lang.org/) and `elm-format`, `elm-test`, `elm-verify-examples` (`npm i -g elm elm-format elm-test elm-verify-examples`).
* [Rust](https://www.rust-lang.org/) which can be installed using [Rustup](https://rustup.rs/).
* [Zemo MQ](https://zeromq.org/) which can be installed on Debian using the `libzmq3-dev` package.
* [pkg-config](https://www.freedesktop.org/wiki/Software/pkg-config/) is used to link with `libzmq`. Can be installed on Debian using the `pkg-config` package.
* Make (Optional). Is used to guide a release build, this can be done manually if `make` is not available. `make` is not used for development.

There are 2 components to the client. The _GUI_ (in the `client/` directory) and the _Bridge_ to Falcon (in the `falcon-bridge/` directory). Each compondent can be built and worked on independently.

#### GUI
Run the following commands as a first time initial setup:

* `cd client/`
* `npm install`

To begin a development session:

* `cd client/`
* `npm start`

This will start a development server that automatically reloads when you make changes to the source code. You can point your browser to `localhost:1234` to begin.

To build a release GUI:

* `cd client`
* `npm run build`

This places `index.html` into the `client/dist/` directory. This file is used for the release build of the _Bridge_.

#### Bridge
Logging can be configured using the `$RUST_LOG` environment variable. For instance, setting `RUST_LOG=debug` will print debug level log messages to the console.

To begin a development session:

* `cd falcon-bridge/`
* `cargo run`

To build a release:

* `cp client/dist/index.html falcon-bridge/src/`
* `cd falcon-bridge/`
* `cargo build --release`

You can find the final binary (called `falcon-bridge`) in the `falcon-bridge/target/release` directory.

#### Release
If you have make installed, run `make` to build a release binary, or `make debug` to build a binary with debugging symbols.

## End-to-End Testing
You will need to have `falcon` and `nlxtestbench` executables from Falcon.
**Quick falcon installation guide**
```
conda env create KloostermanLab/falcon
conda activate falcon
fklab-build --gui false --path https://bitbucket.org/kloostermannerflab/falcon-core.git \
            --version 1.3.0-alpha  \
            --build_options DCMAKE_INSTALL_PREFIX="$HOME/opt/falcon-core"
```
**Run a simulated data source**: `nlxtestbench -c test/nlxtestbench-ripple-source.yml`
**Start Falcon**: `falcon`
**Run the Bridge with debug logging**: `RUST_LOG=debug cargo run`
**Start the GUI**: `npm run start`

You can load a test graph from `test/ripple_detection_graph.yml`. Running this graph
will display data in the plot.

## Contributing

This project is licensed under the [GPLv3 or later](https://www.gnu.org/licenses/gpl-3.0.en.html), see [LICENSE](./LICENSE).

You might find exploring the [architecture](doc/architecture.md) document useful to get started.

When you start the GUI in a browser, messages received can be printed to the console if you change the address to include a `debug` flag.
For example, `http://localhost:51966/?debug`. Now you can open the browser developer tools console and view messages received.

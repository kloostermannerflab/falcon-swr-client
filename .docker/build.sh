#!/bin/sh

echo "Building..."
make ${TARGET}
if [ -f falcon-swr-client ]; then
    sudo cp falcon-swr-client /client-source
    sudo chown node:node /client-source/falcon-swr-client
else
    echo "Build failed"
fi

if [ -f bridge-docs.txz ]; then
    sudo cp bridge-docs.txz /client-source
    sudo chown node:node /client-source/bridge-docs.txz
fi
